<?php

/**
 * DiscuzX Convert
 *
 * $Id: home_event.php 15720 2010-08-25 23:56:08Z monkey $
 */

// 获取当前程序名称
$curprg = basename(__FILE__);

// 转换的table
$table_source = $db_source->tablepre.'event';

// 取得本次转换的起始id
$start = getgpc('start');
$start = empty($start) ? 0 : intval($start);
// 每次转换多少数据
$limit = $setting['limit']['event'] ? $setting['limit']['event'] : 100;
// 下次跳转id
$nextid = 0;
$home = load_process('home');
$eventfid = intval($home['forum']['event']);

// 如果指定了，验证是否存在
if(!empty($eventfid)) {
	$value = $db_target->fetch_first('SELECT * FROM '.$db_target->table_name('forum_forum')." WHERE fid='$eventfid' AND status!='3'");
	if(empty($value)) {
		$eventfid = 0;
	}
}
// 如果未指定 eventfid，使用程序自动创建的活动版块
if(empty($eventfid)) {
	$board_name = 'UCHome数据';
	$forum_name = 'UCHome活动';
	$value = $db_target->fetch_first('SELECT fid FROM '.$db_target->table_name('forum_forum')." WHERE type='forum' AND status='1' AND `name`='$forum_name'");
	if(!empty($value)) {// 使用
		$eventfid = intval($value['fid']);
	} else {// 创建
		//分区
		$value = $db_target->fetch_first('SELECT fid FROM '.$db_target->table_name('forum_forum')." WHERE type='group' AND status='1' AND `name`='$board_name'");
		if($value) {
			$fup = intval($value['fid']);
		} else {
			$board = array(
				'name' => $board_name,
				'type' => 'group',
				'status' => '1',
			);
			$fup = $db_target->insert('forum_forum', $board, true);
		}
		// 版块
		$forum = array(
			'name' => $forum_name,
			'fup' => $fup,
			'type' => 'forum',
			'allowsmilies' => 1,
			'allowbbcode' => 1,
			'allowimgcode' => 1,
			'status' => '1',
		);
		$eventfid = $db_target->insert('forum_forum', $forum, true);
		$forumfield = array(
			'fid' => $eventfid,
			'description' => '从 UCenter Home 转移过来的活动内容'
		);
		$db_target->insert('forum_forumfield', $forumfield);
	}
}

// 活动分类
$eventclass = array();
$query = $db_source->query('SELECT classid, classname FROM '.$db_source->table_name('eventclass'));
while($value=$db_source->fetch_array($query)) {
	$eventclass[$value['classid']] = $value['classname'];
}

include_once DISCUZ_ROOT.'./include/editor.func.php';

// 取得数据，并存储
$event_query = $db_source->query("SELECT e.*, ef.detail, ef.limitnum FROM $table_source e LEFT JOIN ".$db_source->table_name('eventfield')." ef ON e.eventid = ef.eventid WHERE e.eventid > $start ORDER BY e.eventid LIMIT $limit");
while ($event = $db_source->fetch_array($event_query)) {

	$nextid = intval($event['eventid']);

	//活动留言数和最后一条活动留言
	$commentnum = $db_source->result_first('SELECT count(*) FROM '.$db_source->table_name('comment')." WHERE id='$event[eventid]' AND idtype='eventid'");
	$lastcomment = array();
	if($commentnum) {
		$lastcomment = $db_source->fetch_first('SELECT author, dateline FROM '.$db_source->table_name('comment')." WHERE id='$event[eventid]' AND idtype='eventid' ORDER BY cid DESC LIMIT 1");
	}
	// 生成帖子
	$threadarr = array(
		'fid' => $eventfid,
		'author' => $event['username'],
		'authorid' => $event['uid'],
		'subject' => $event['title'],
		'dateline' => $event['dateline'],
		'lastpost' => !empty($lastcomment) ? $lastcomment['dateline'] : $event['updatetime'],
		'lastposter' => !empty($lastcomment) ? $lastcomment['author'] : $event['username'],
		'views' => $event['viewnum'],
		'replies' => $commentnum,
		'special' => 4
	);
	$tid = $db_target->insert('forum_thread', daddslashes($threadarr), true);
	//帖子信息（post）
	$event['detail'] = html2bbcode($event['detail']);
	$postarr = array(
		'fid' => $eventfid,
		'tid' => $tid,
		'first' => '1',
		'author' => $event['username'],
		'authorid' => $event['uid'],
		'subject' => $event['subject'],
		'dateline' => $event['dateline'],
		'message' => $event['detail']
	);
	$pid = $db_target->insert('forum_post', daddslashes($postarr), true);
	// 活动信息（activity）
	// 封面图片
	$aid = 0;
	$activityarr = array(
		'tid' => $tid,
		'uid' => $event['uid'],
		'aid' => $aid,
		'cost' => '',
		'starttimefrom' => $event['starttime'],
		'starttimeto' => $event['endtime'],
		'place' => '['.$event['province'].$event['city'].'] '.$event['location'],
		'class' => $eventclass[$event['classid']],
		'number' => $event['limitnum'],
		'applynumber' => $event['membernum'] - 1,// Home 里的活动成员包括创建者
		'expiration' => $event['deadline']
	);
	$db_target->insert('forum_activity', daddslashes($activityarr));

	//活动成员
	$inserts = array();
	$query = $db_source->query('SELECT * FROM '.$db_source->table_name('userevent')." WHERE eventid = '$event[eventid]' AND status IN ('1', '2')");
	while($value=$db_source->fetch_array($query)) {
		$value['verified'] = $value['status'] == 1 ? 0 : 1;
		$value['username'] = addslashes($value['username']);
		$inserts[] = "('$tid', '$value[username]', '$value[uid]', '$value[verified]', '$value[dateline]', '-1')";
	}
	if($inserts) {
		$db_target->query('INSERT INTO '.$db_target->table_name('forum_activityapply').'(tid, username ,uid, verified, dateline, payment) VALUES '.implode(', ', $inserts));
	}

	//活动留言
	if($commentnum) {
		$inserts = array();
		$query = $db_source->query('SELECT * FROM '.$db_source->table_name('comment')." WHERE id='$event[eventid]' AND idtype='eventid' ORDER BY cid");
		while($value=$db_source->fetch_array($query)) {
			$value['message'] = addslashes(html2bbcode($value['message']));
			$value['author'] = addslashes($value['author']);
			$inserts[] = "('$fid', '$tid', '$value[author]', '$value[authorid]', '$value[dateline]', '$value[message]')";
		}
		$db_target->query('INSERT INTO '.$db_target->table_name('forum_post')."(fid, tid, author, authorid, dateline, message) VALUES ".implode(', ',$inserts));
	}

	//更新版块帖子数
	$posts = $commentnum + 1;
	$db_target->query("UPDATE ".$db_target->table_name('common_member_count')." SET threads=threads+1 WHERE uid='$event[uid]'");
	$db_target->query("UPDATE ".$db_target->table_name('forum_forum')." SET threads=threads+1, posts=posts+$posts WHERE fid='$eventfid'");
}

//判断是否需要跳转
if($nextid) {
	//跳转到新的id
	showmessage("继续转换数据表 ".$table_source." eventid> $nextid", "index.php?a=$action&source=$source&prg=$curprg&start=$nextid");
}

//更新forum_post_tableid
$maxpid = $db_target->result_first("SELECT MAX(pid) FROM ".$db_target->table('forum_post'));
$maxpid = intval($maxpid) + 1;
$db_target->query("ALTER TABLE ".$db_target->table('forum_post_tableid')." AUTO_INCREMENT=$maxpid");
$db_target->query("UPDATE ".$db_target->table_name('forum_forum')." SET status=1 WHERE status=2");
?>
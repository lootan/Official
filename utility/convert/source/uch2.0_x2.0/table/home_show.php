<?php

/**
 * DiscuzX Convert
 *
 * $Id: home_show.php 15720 2010-08-25 23:56:08Z monkey $
 */

// 获取当前程序名称
$curprg = basename(__FILE__);

// 转换的table
$table_source = $db_source->tablepre.'show';
$table_target = $db_target->tablepre.'home_show';

// 每次转换多少数据
$limit = 2000;
// 下次跳转id
$nextid = 0;

// 取得本次转换的起始id
$start = getgpc('start');
// 首次执行，清空目标数据表，避免重复转换
if($start == 0) {
	$db_target->query("TRUNCATE $table_target");
}

// 取得数据，并存储
$query = $db_source->query("SELECT * FROM $table_source WHERE uid>'$start' ORDER BY uid LIMIT $limit");
while ($show = $db_source->fetch_array($query)) {

	//下次执行id
	$nextid = intval($show['showid']);

	//数据引号处理
	$show  = daddslashes($show, 1);

	//将数组整理成sql数据格式
	$data = implode_field_value($show, ',', db_table_fields($db_target, $table_target));

	//插入数据表
	$db_target->query("INSERT INTO $table_target SET $data");
}

//判断是否需要跳转
if($nextid) {
	//跳转到新的id
	showmessage("继续转换数据表 ".$table_source." uid> $nextid", "index.php?a=$action&source=$source&prg=$curprg&start=$nextid");
}

?>
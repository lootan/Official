<?php

/**
 * DiscuzX Convert
 *
 * $Id: taskvars.php 15475 2010-08-24 07:34:47Z monkey $
 */

// 获取当前程序名称
$curprg = basename(__FILE__);

// 转换的table
$table_source = $db_source->tablepre.'taskvars';
$table_target = $db_target->tablepre.'common_taskvar';

// 每次转换多少数据
$limit = 100;
// 下次跳转id
$nextid = 0;

// 取得本次转换的起始id
$start = intval(getgpc('start'));
// 首次执行，清空目标数据表，避免重复转换
if($start == 0) {
	$db_target->query("TRUNCATE $table_target");
}

$taskids = 0;
$query = $db_source->query("SELECT * FROM ".$db_source->tablepre.'tasks');
while($row = $db_source->fetch_array($query)) {
	$taskids .= ", $row[taskid]";
}

// 取得数据，并存储
$query = $db_source->query("SELECT * FROM $table_source WHERE taskid IN ($taskids) AND taskvarid>$start ORDER BY taskvarid LIMIT $limit");
while ($row = $db_source->fetch_array($query)) {

	//下次执行id
	$nextid = $row['taskvarid'];

	unset($row['extra']);
	//数据引号处理
	$row  = daddslashes($row, 1);

	//将数组整理成sql数据格式
	$data = implode_field_value($row, ',', db_table_fields($db_target, $table_target));

	//插入数据表
	$db_target->query("INSERT INTO $table_target SET $data");
}

//判断是否需要跳转
if($nextid) {
	//跳转到新的id
	showmessage("继续转换数据表 ".$table_source."  taskvarid > $nextid ", "index.php?a=$action&source=$source&prg=$curprg&start=$nextid");
}

?>
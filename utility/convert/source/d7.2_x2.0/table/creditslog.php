<?php

/**
 * DiscuzX Convert
 *
 * $Id: creditslog.php 15815 2010-08-27 02:56:14Z monkey $
 */

// 获取当前程序名称
$curprg = basename(__FILE__);

// 转换的table
$table_source = $db_source->tablepre.'creditslog';
$table_target = $db_target->tablepre.'common_credit_log';

// 每次转换多少数据
$limit = 2000;
// 下次跳转id
$nextid = 0;

// 取得本次转换的起始id
$start = intval(getgpc('start'));
// 首次执行，清空目标数据表，避免重复转换
if(empty($start) && !$process['truncate_credit_log']) {
	$start = 0;
	$process['truncate_credit_log'] = 1;
	save_process('main', $process);
	$db_target->query("TRUNCATE $table_target");
}

$rowlist = $userarr = array();
$query = $db_source->query("SELECT * FROM $table_source LIMIT $start, $limit");
while ($row = $db_source->fetch_array($query)) {
	$nextid = 1;
	$rowlist[] = $row;
	$userarr[$row['fromto']] = $row['fromto'];
}

if($nextid) {
	$userarr = daddslashes($userarr, 1);
	$usernames = implode("', '", $userarr);
	$query = $db_source->query("SELECT * FROM ".$db_source->tablepre."members WHERE username IN('$usernames')");
	while($row = $db_source->fetch_array($query)) {
		$userarr[$row['username']] = $row['uid'];
	}

	foreach($rowlist as $row) {
		$rownew = array();
		if(in_array($row['operation'], array('AFD', 'TFR', 'RCV'))) {//note 积分转账
			$rownew['uid'] = $row['uid'];
			if($row['operation'] == 'RCV' && $row['fromto'] == 'TASK REWARD') {
				$rownew['operation'] = 'TRC';
				$rownew['relatedid'] = 0;//note discuzx之前版本未记录此值，这里暂用0代替
			} else {
				$rownew['operation'] = $row['operation'];
				$rownew['relatedid'] = $userarr[$row['fromto']];
			}
			
			$rownew['dateline'] = $row['dateline'];
			if($row['receive']) {
				$rownew['extcredits'.$row['receivecredits']] = $row['receive'];
			}
			if($row['send']) {
				$rownew['extcredits'.$row['sendcredits']] = -$row['send'];
			}
		} elseif($row['operation'] == 'UGP') {//note 购买用户组
			$rownew['uid'] = $row['uid'];
			$rownew['operation'] = $row['operation'];
			$rownew['relatedid'] = 0;//note discuzx之前版本未记录此值，这里暂用0代替
			$rownew['dateline'] = $row['dateline'];
			if($row['receive']) {
				$rownew['extcredits'.$row['receivecredits']] = $row['receive'];
			}
			if($row['send']) {
				$rownew['extcredits'.$row['sendcredits']] = -$row['send'];
			}
		} elseif($row['operation'] == 'EXC') {//note 通过uc积分兑换
			$rownew['uid'] = $row['uid'];
			$rownew['operation'] = 'ECU';
			$rownew['relatedid'] = $row['uid'];
			$rownew['dateline'] = $row['dateline'];
			if($row['receive']) {
				$rownew['extcredits'.$row['receivecredits']] = $row['receive'];
			}
			if($row['send']) {
				$rownew['extcredits'.$row['sendcredits']] = -$row['send'];
			}
		}
		if($rownew) {
			//数据引号处理
			$rownew  = daddslashes($rownew, 1);
		
			//将数组整理成sql数据格式
			$data = implode_field_value($rownew, ',', db_table_fields($db_target, $table_target));
		
			//插入数据表
			$db_target->query("INSERT INTO $table_target SET $data");
		}
	}
}

//判断是否需要跳转
if($nextid) {
	//跳转到新的id
	showmessage("继续转换数据表 ".$table_source." $start 至 ".($start+$limit)." 行", "index.php?a=$action&source=$source&prg=$curprg&start=".($start+$limit));
}

?>
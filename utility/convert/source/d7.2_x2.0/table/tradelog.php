<?php

/**
 * DiscuzX Convert
 *
 * $Id: tradelog.php 15719 2010-08-25 23:51:36Z monkey $
 */

// 获取当前程序名称
$curprg = basename(__FILE__);

// 转换的table
$table_source = $db_source->tablepre.'tradelog';
$table_target = $db_target->tablepre.'forum_tradelog';

// 每次转换多少数据
$limit = 100;
// 下次跳转id
$next = FALSE;

// 取得本次转换的起始id
$start = $_GET['start'] ? getgpc('start') : 0;
// 首次执行，清空目标数据表，避免重复转换
if($start == 0) {
	$db_target->query("TRUNCATE $table_target");
}

// 取得数据，并存储
$query = $db_source->query("SELECT * FROM $table_source ORDER BY tid LIMIT $start, $limit");
while ($data = $db_source->fetch_array($query)) {

	//下次执行id
	$next = TRUE;

	//数据引号处理
	$data  = daddslashes($data, 1);

	//将数组整理成sql数据格式
	$datalist = implode_field_value($data, ',', db_table_fields($db_target, $table_target));

	//插入数据表
	$db_target->query("INSERT INTO $table_target SET $datalist");
}

//判断是否需要跳转
if($next) {
	//跳转到新的id
	showmessage("继续转换数据表 ".$table_source." $start 至 ".($start+$limit)." 行", "index.php?a=$action&source=$source&prg=$curprg&start=".($start + $limit));
}

?>
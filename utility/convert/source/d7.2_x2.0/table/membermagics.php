<?php

/**
 * DiscuzX Convert
 *
 * $Id: membermagics.php 15719 2010-08-25 23:51:36Z monkey $
 */

// 获取当前程序名称
$curprg = basename(__FILE__);

// 转换的table
$table_source = $db_source->tablepre.'membermagics';
$table_sourcemarket = $db_source->tablepre.'magicmarket';
$table_target = $db_target->tablepre.'common_member_magic';

// 每次转换多少数据
$limit = 1000;
// 下次跳转id
$nextid = 0;

// 取得本次转换的起始id
$start = getgpc('start');
// 首次执行，清空目标数据表，避免重复转换
if(empty($start)) {
	$start = 0;
	$db_target->query("TRUNCATE $table_target");
}

// 取得数据，并存储
$query = $db_source->query("SELECT * FROM $table_source LIMIT $start, $limit");
while ($row = $db_source->fetch_array($query)) {

	//下次执行id
	$nextid = 1;

	//数据引号处理
	$row  = daddslashes($row, 1);

	//将数组整理成sql数据格式
	$data = implode_field_value($row, ',', db_table_fields($db_target, $table_target));

	//插入数据表
	$db_target->query("INSERT INTO $table_target SET $data");
}

//判断是否需要跳转
if($nextid) {
	$query = $db_source->query("SELECT * FROM $table_sourcemarket");
	while ($row = $db_source->fetch_array($query)) {
		$row = daddslashes($row, 1);
		$mm = $db_target->fetch_first("SELECT * FROM $table_target WHERE uid='$row[uid]' AND magicid='$row[magicid]'");
		if($mm) {
			$db_target->query("UPDATE $table_target SET num=num+'$row[num]' WHERE uid='$row[uid]' AND magicid='$row[magicid]'");
		} else {
			$db_target->query("INSERT INTO $table_target SET uid='$row[uid]', magicid='$row[magicid]', num='$row[num]'");
		}
	}
	//跳转到新的id
	showmessage("继续转换数据表 ".$table_source." $start 至 ".($start+$limit)." 行", "index.php?a=$action&source=$source&prg=$curprg&start=".($start+$limit));
}

?>
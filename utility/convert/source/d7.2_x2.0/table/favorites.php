<?php

/**
 * DiscuzX Convert
 *
 * $Id: favorites.php 15719 2010-08-25 23:51:36Z monkey $
 */

// 获取当前程序名称
$curprg = basename(__FILE__);

// 转换的table
$table_source = $db_source->tablepre.'favorites';
$table_target = $db_target->tablepre.'home_favorite';

// 每次转换多少数据
$limit = 100;
// 下次跳转id
$nextid = 0;

// 取得本次转换的起始id
$start = getgpc('start');
$nextstep = intval(getgpc('nextstep'));
// 首次执行，清空目标数据表，避免重复转换
if(empty($start)) {
	$start = 0;
	$db_target->query("TRUNCATE $table_target");
}

// 取得数据，并存储
$query = $db_source->query("SELECT * FROM $table_source LIMIT $start, $limit");
while ($row = $db_source->fetch_array($query)) {

	//下次执行id
	$nextid = 1;

	$row['id'] = 0;
	$row['idtype'] = '';

	if($row['tid']) {
		$row['title'] = $db_source->result_first("SELECT subject FROM ".$db_source->tablepre."threads WHERE tid='".$row['tid']."'");
		$row['id'] = $row['tid'];
		$row['idtype'] = 'tid';
	} elseif($row['fid']) {
		$row['title'] = $db_source->result_first("SELECT name FROM ".$db_source->tablepre."forums WHERE fid='".$row['fid']."'");
		$row['id'] = $row['fid'];
		$row['idtype'] = 'fid';
	}
	$row  = daddslashes($row, 1);

	if($row['id']) {
		unset($row['tid'], $row['fid']);
		$row['dateline'] = time();
		$data = implode_field_value($row, ',', db_table_fields($db_target, $table_target));
		//插入数据表
		$db_target->query("INSERT INTO $table_target SET $data");
	}

}

//判断是否需要跳转
if($nextid) {
	//跳转到新的id
	showmessage("继续转换数据表 ".$table_source." $start 至 ".($start+$limit)." 行", "index.php?a=$action&source=$source&prg=$curprg&start=".($start+$limit));
}

?>
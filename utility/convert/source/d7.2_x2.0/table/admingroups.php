<?php

/**
 * DiscuzX Convert
 *
 * $Id: admingroups.php 15808 2010-08-27 02:34:26Z monkey $
 */

// 获取当前程序名称
$curprg = basename(__FILE__);

// 转换的table
$table_source = $db_source->tablepre.'admingroups';
$table_target = $db_target->tablepre.'common_admingroup';

// 每次转换多少数据
$limit = 2000;
// 下次跳转id
$nextid = 0;

// 取得本次转换的起始id
$start = getgpc('start');
// 首次执行，清空目标数据表，避免重复转换
if(empty($start)) {
	$start = 0;
	$db_target->query("DELETE FROM $table_target WHERE admingid>'3'");
}

// 取得数据，并存储
$query = $db_source->query("SELECT * FROM $table_source WHERE admingid>'$start' ORDER BY admingid LIMIT $limit");
while ($row = $db_source->fetch_array($query)) {

	//下次执行id
	$nextid = $row['admingid'];

	//数据引号处理
	$row  = daddslashes($row, 1);

	//将数组整理成sql数据格式
	$data = implode_field_value($row, ',', db_table_fields($db_target, $table_target));
	//判断是否存在管理组
	$gidexist = 0;
	if($row['admingid'] < 4) {
		$gidexist = $db_target->result_first("SELECT admingid FROM $table_target WHERE admingid='".$row['admingid']."'");
	}
	if(!empty($gidexist)) {
		$db_target->query("UPDATE $table_target SET $data WHERE admingid='".$row['admingid']."'");
	} else {
		//插入数据表
		$db_target->query("INSERT INTO $table_target SET $data");
	}
}

//判断是否需要跳转
if($nextid) {
	//跳转到新的id
	showmessage("继续转换数据表 ".$table_source." admingid > $nextid", "index.php?a=$action&source=$source&prg=$curprg&start=$nextid");
} else {
	$db_target->query("UPDATE $table_target SET allowbanvisituser='1' WHERE admingid='1' OR admingid='2'");
}

?>
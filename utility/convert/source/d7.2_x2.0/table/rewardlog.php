<?php

/**
 * DiscuzX Convert
 *
 * $Id: rewardlog.php 15815 2010-08-27 02:56:14Z monkey $
 */

// 获取当前程序名称
$curprg = basename(__FILE__);

// 转换的table
$table_source = $db_source->tablepre.'rewardlog';
$table_target = $db_target->tablepre.'common_credit_log';

// 每次转换多少数据
$limit = 1000;
// 下次跳转id
$nextid = 0;

// 取得本次转换的起始id
$start = intval(getgpc('start'));
// 首次执行，清空目标数据表，避免重复转换
if(empty($start) && !$process['truncate_credit_log']) {
	$start = 0;
	$process['truncate_credit_log'] = 1;
	save_process('main', $process);
	$db_target->query("TRUNCATE $table_target");
}

$query = $db_source->query("SELECT * FROM ".$db_source->tablepre."settings WHERE variable IN ('creditstax', 'creditstrans')");
while($setting = $db_source->fetch_array($query)) {
	if($setting['variable'] == 'creditstrans') {
		$creditstrans = explode(',', $setting['value']);
		$ext = $creditstrans[2] ? $creditstrans[2] : $creditstrans[0];
		if(!$ext) {
			$ext = 1;
		}
	}
	if($setting['variable'] == 'creditstax') {
		$creditstax = $setting['value'];
	}
}

// 取得数据，并存储
$query = $db_source->query("SELECT  * FROM $table_source WHERE netamount>'0' ORDER BY dateline LIMIT $start, $limit");
while ($row = $db_source->fetch_array($query)) {

	//下次执行id
	$nextid = 1;

	if($row['answererid'] > 0) {
		$rownew = array();
		$rownew['uid'] = $row['answererid'];
		$rownew['operation'] = 'RAC';
		$rownew['relatedid'] = $row['tid'];
		$rownew['dateline'] = $row['dateline'];
		$rownew['extcredits'.$ext] = $row['netamount'];
		//数据引号处理
		$rownew  = daddslashes($rownew, 1);

		//将数组整理成sql数据格式
		$data = implode_field_value($rownew, ',', db_table_fields($db_target, $table_target));

		//插入数据表
		$db_target->query("INSERT INTO $table_target SET $data");
	} else {
		$rownew = array();
		$rownew['uid'] = $row['authorid'];
		$rownew['operation'] = 'RTC';
		$rownew['relatedid'] = $row['tid'];
		$rownew['dateline'] = $row['dateline'];
		$rownew['extcredits'.$ext] = -ceil($row['netamount'] / (1 - $creditstax));
		//数据引号处理
		$rownew  = daddslashes($rownew, 1);

		//将数组整理成sql数据格式
		$data = implode_field_value($rownew, ',', db_table_fields($db_target, $table_target));

		//插入数据表
		$db_target->query("INSERT INTO $table_target SET $data");
	}

}

//判断是否需要跳转
if($nextid) {
	//跳转到新的id
	$next = $start + $limit;
	showmessage("继续转换数据表 ".$table_source." $start 至 ".($start+$limit)." 行", "index.php?a=$action&source=$source&prg=$curprg&start=$next");
}

?>
/*
	[Discuz!] (C)2001-2099 Comsenz Inc.
	This is NOT a freeware, use is subject to license terms

	$Id: imgcropper.js 30998 2012-07-06 07:22:08Z zhangguosheng $
*/
(function(){

	ImgCropper = function() {
		this.options = {
				//note 默认值
				opacity:	50,//note 透明度(0到100)
				color:		"",//note 背景色
				width:		0,//note 图片高度
				height:		0,//note 图片高度
				//note 缩放触发对象
				resize:		false,//note 是否设置缩放
				right:		"",//note 右边缩放对象
				left:		"",//note 左边缩放对象
				up:			"",//note 上边缩放对象
				down:		"",//note 下边缩放对象
				rightDown:	"",//note 右下缩放对象
				leftDown:	"",//note 左下缩放对象
				rightUp:	"",//note 右上缩放对象
				leftUp:		"",//note 左上缩放对象
				min:		false,//note 是否最小宽高限制(为true时下面min参数有用)
				minWidth:	50,//note 最小宽度
				minHeight:	50,//note 最小高度
				scale:		false,//note 是否按比例缩放
				ratio:		0,//note 缩放比例(宽/高)
				//note 预览对象设置
				Preview:	"",//note 预览对象
				viewWidth:	0,//note 预览宽度
				viewHeight:	0//note 预览高度
			};
		this.setParameter.apply(this, arguments);
	};
	ImgCropper.prototype = {
		//note 容器对象,控制层,图片地址
		setParameter: function(container, handle, url, options) {
			this._container = $(container);//note 容器对象
			this._layHandle = $(handle);//note 控制层
			this.url = url;//note 图片地址

			this._layBase = this._container.appendChild(document.createElement("img"));//note 底层
			this._layCropper = this._container.appendChild(document.createElement("img"));//note 切割层
			this._layCropper.onload = Util.bindApply(this, this.setPos);
			//note 用来设置大小
			this._tempImg = document.createElement("img");
			this._tempImg.onload = Util.bindApply(this, this.setSize);

			this.options = Util.setOptions(this.options, options || {});

			this.opacity = Math.round(this.options.opacity);
			this.color = this.options.color;
			this.scale = !!this.options.scale;
			this.ratio = Math.max(this.options.ratio, 0);
			this.width = Math.round(this.options.width);
			this.height = Math.round(this.options.height);
			this.setLayHandle = true;

			//note 设置预览对象
			var oPreview = $(this.options.Preview);//note 预览对象
			if(oPreview){
				oPreview.style.position = "relative";
				oPreview.style.overflow = "hidden";
				this.viewWidth = Math.round(this.options.viewWidth);
				this.viewHeight = Math.round(this.options.viewHeight);
				//note 预览图片对象
				this._view = oPreview.appendChild(document.createElement("img"));
				this._view.style.position = "absolute";
				this._view.onload = Util.bindApply(this, this.SetPreview);
			}
			//note 设置拖放
			this._drag = new dzDrag(handle, {limit:true, container:container, onDragMove: Util.bindApply(this, this.setPos)});
			//note 设置缩放
			this.resize = !!this.options.resize;
			if(this.resize){
				var op = this.options;
				var _resize = new ImgCropperResize(container, {max: false, scale:true, min:true, minWidth:options.minWidth, minHeight:options.minHeight, onResize: Util.bindApply(this, this.scaleImg)});
				//note 设置缩放触发对象
				op.rightDown && (_resize.set(op.rightDown, "right-down"));
				op.leftDown && (_resize.set(op.leftDown, "left-down"));
				op.rightUp && (_resize.set(op.rightUp, "right-up"));
				op.leftUp && (_resize.set(op.leftUp, "left-up"));
				op.right && (_resize.set(op.right, "right"));
				op.left && (_resize.set(op.left, "left"));
				op.down && (_resize.set(op.down, "down"));
				op.up && (_resize.set(op.up, "up"));
				//note 最小范围限制
				this.min = !!this.options.min;
				this.minWidth = Math.round(this.options.minWidth);
				this.minHeight = Math.round(this.options.minHeight);
				//note 设置缩放对象
				this._resize = _resize;
			}
			//note 设置样式
			this._container.style.position = "relative";
			this._container.style.overflow = "hidden";
			this._layHandle.style.zIndex = 200;
			this._layCropper.style.zIndex = 100;
			this._layBase.style.position = this._layCropper.style.position = "absolute";
			this._layBase.style.top = this._layBase.style.left = this._layCropper.style.top = this._layCropper.style.left = 0;//note 对齐
			//note 初始化设置
			this.initialize();
		},

		//note 初始化对象
		initialize: function() {
			//note 设置背景色
			this.color && (this._container.style.backgroundColor = this.color);
			//note 设置图片
			this._tempImg.src = this._layBase.src = this._layCropper.src = this.url;
			//note 设置透明
			if(BROWSER.ie){
				this._layBase.style.filter = "alpha(opacity:" + this.opacity + ")";
				this._layHandle.style.filter = "alpha(opacity:0)";
				this._layHandle.style.backgroundColor = "#FFF";
			} else {
				this._layBase.style.opacity = this.opacity / 100;
			}
			//note 设置预览对象
			this._view && (this._view.src = this.url);
			//note 设置缩放
			if(this.resize){
				with(this._resize){
					Scale = this.scale; Ratio = this.ratio; Min = this.min; minWidth = this.minWidth; minHeight = this.minHeight;
				}
			}
		},
		//note 设置切割样式
		setPos: function() {
			//note ie6渲染bug
			if(BROWSER.ie == 6.0){ with(this._layHandle.style){ zoom = .9; zoom = 1; }; };
			//note 获取位置参数
			var p = this.getPos();
			//note 按拖放对象的参数进行切割
			this._layCropper.style.clip = "rect(" + p.Top + "px " + (p.Left + p.Width) + "px " + (p.Top + p.Height) + "px " + p.Left + "px)";
			//note 设置预览
			this.SetPreview();
			parent.resetHeight(this._container, this.getPos(), this._layBase);
		},
		scaleImg:function() {
			this.height = this._resize._styleHeight;
			this.width = this._resize._styleWidth;
			this.initialize();
			this.setSize();
			this.setPos();
			//note 判断裁切框是不是越界
			var maxRight = (parseInt(this._layHandle.style.left) || 0) + (parseInt(this._layHandle.offsetWidth) || 0);
			var maxBottom = (parseInt(this._layHandle.style.top) || 0) + (parseInt(this._layHandle.offsetHeight) || 0);
			//note 设置范围参数
			//note this._layHandle.innerHTML = 'mr:'+maxRight+'<br/>cw:' + drag._container.clientWidth + '<br/>mb:'+maxBottom+'<br/>ch:'+drag._container.clientHeight;
			if(this._container != null) {
				if(maxRight > this._container.clientWidth) {
					var nowLeft = this._container.clientWidth - this._layHandle.offsetWidth;
					this._layHandle.style.left = (nowLeft < 0 ? 0 : nowLeft) + "px";
				}
				if(maxBottom > this._container.clientHeight) {
					var nowTop = this._container.clientHeight - this._layHandle.offsetHeight;
					this._layHandle.style.top = (nowTop < 0 ? 0 : nowTop) + "px";
				}
			}
			//note 判断是否越界
			parent.resetHeight(this._container, this.getPos(), this._layBase);
		},
		//note 设置预览效果
		SetPreview: function() {
			if(this._view){
				//note 预览显示的宽和高
				var p = this.getPos(), s = this.getSize(p.Width, p.Height, this.viewWidth, this.viewHeight), scale = s.Height / p.Height;
				//note 按比例设置参数
				var pHeight = this._layBase.height * scale, pWidth = this._layBase.width * scale, pTop = p.Top * scale, pLeft = p.Left * scale;
				//note 设置预览对象
				with(this._view.style){
					//note 设置样式
					width = pWidth + "px"; height = pHeight + "px"; top = - pTop + "px "; left = - pLeft + "px";
					//note 切割预览图
					clip = "rect(" + pTop + "px " + (pLeft + s.Width) + "px " + (pTop + s.Height) + "px " + pLeft + "px)";
				}
			}
		},
		//note 设置图片大小
		setSize: function() {
			if(this.width > this._tempImg.width) {
				this.width = this._tempImg.width;
			}
			if(this.height > this._tempImg.height) {
				this.height = this._tempImg.height;
			}
			var s = this.getSize(this._tempImg.width, this._tempImg.height, this.width, this.height);
			if(this.options.min && (s.Width <= this.options.minWidth || s.Height <= this.options.minHeight)) {
				return false;
			}
			//note 设置底图和切割图
			this._layBase.style.width = this._layCropper.style.width = s.Width + "px";
			this._layBase.style.height = this._layCropper.style.height = s.Height + "px";
			//note 设置拖放范围
			this._drag.maxRight = s.Width; this._drag.maxBottom = s.Height;
			//note 设置缩放范围
			if(this.resize) {
				//note this._resize.maxRight = s.Width; this._resize.maxBottom = s.Height;
				this._container.style.width = this._layBase.style.width; this._container.style.height = this._layBase.style.height;
				//note 重新计算裁切框坐标
				if(this.setLayHandle) {
					this._layHandle.style.left = ((s.Width - this._layHandle.offsetWidth)/2)+"px";
					this._layHandle.style.top = ((s.Height - this._layHandle.offsetHeight)/2)+"px";
					this.setPos();
					this.setLayHandle = false;
				}
			}
		},
		//note 获取当前样式
		getPos: function() {
			with(this._layHandle){
				return { Top: offsetTop, Left: offsetLeft, Width: offsetWidth, Height: offsetHeight };
			}
		},
		//note 获取尺寸
		getSize: function(nowWidth, nowHeight, fixWidth, fixHeight) {
			var iWidth = nowWidth, iHeight = nowHeight, scale = iWidth / iHeight;
			//note 按比例设置
			if(fixHeight){ iWidth = (iHeight = fixHeight) * scale; }
			if(fixWidth && (!fixHeight || iWidth > fixWidth)){ iHeight = (iWidth = fixWidth) / scale; }
			//note 返回尺寸对象
			return { Width: iWidth, Height: iHeight }
		}
	};

	ImgCropperResize = function() {
		this.options = {
			max:		false,	//note 是否设置范围限制(为true时下面mx参数有用)
			container:	"",		//note 指定限制在容器内
			maxLeft:	0,		//note 左边限制
			maxRight:	9999,	//note 右边限制
			maxTop:		0,		//note 上边限制
			maxBottom:	9999,	//note 下边限制
			min:		false,	//note 是否最小宽高限制(为true时下面min参数有用)
			minWidth:	50,		//note 最小宽度
			minHeight:	50,		//note 最小高度
			scale:		false,	//note 是否按比例缩放
			ratio:		0,		//note 缩放比例(宽/高)
			onResize:	function(){}	//note 缩放时执行
		};
		this.initialize.apply(this, arguments);
	};

	ImgCropperResize.prototype = {
		initialize: function(resizeObjId, options) {
			this.options = Util.setOptions(this.options, options || {});
			this._resizeObj = $(resizeObjId);	//note 缩放对象

			this._styleWidth = this._styleHeight = this._styleLeft = this._styleTop = 0;	//note 样式参数
			this._sideRight = this._sideDown = this._sideLeft = this._sideUp = 0;	//note 坐标参数
			this._fixLeft = this._fixTop = 0;	//note 定位参数
			this._scaleLeft = this._scaleTop = 0;	//note 定位坐标

			this._maxSet = function(){};//note 范围设置程序
			this._maxRightWidth = this._maxDownHeight = this._maxUpHeight = this._maxLeftWidth = 0;//note 范围参数
			this._maxScaleWidth = this._maxScaleHeight = 0;//note 比例范围参数

			this._fun = function(){};//note 缩放执行程序

			//note 获取边框宽度
			var _style = Util.currentStyle(this._resizeObj);
			this._borderX = (parseInt(_style.borderLeftWidth) || 0) + (parseInt(_style.borderRightWidth) || 0);
			this._borderY = (parseInt(_style.borderTopWidth) || 0) + (parseInt(_style.borderBottomWidth) || 0);
			//note 事件对象(用于绑定移除事件)
			this._resizeTranscript = Util.bindApply(this, this.resize);
			this._stopTranscript = Util.bindApply(this, this.stop);

			//note 范围限制
			this.max = !!this.options.max;
			this._container = $(this.options.container) || null;
			this.maxLeft = Math.round(this.options.maxLeft);
			this.maxRight = Math.round(this.options.maxRight);
			this.maxTop = Math.round(this.options.maxTop);
			this.maxBottom = Math.round(this.options.maxBottom);
			//note 宽高限制
			this.min = !!this.options.min;
			this.minWidth = Math.round(this.options.minWidth);
			this.minHeight = Math.round(this.options.minHeight);
			//note 按比例缩放
			this.scale = !!this.options.scale;
			this.ratio = Math.max(this.options.ratio, 0);

			this.onResize = this.options.onResize;

			this._resizeObj.style.position = "absolute";
			!this._container || Util.currentStyle(this._container).position == "relative" || (this._container.style.position = "relative");
		},
		//note 设置触发对象
		set: function(resize, side) {
			var resize = $(resize), fun;
			if(!resize) return;
			//note 根据方向设置
			switch(side.toLowerCase()) {
				case "up":
					fun = this.up;
					break;
				case "down":
					fun = this.down;
					break;
				case "left":
					fun = this.left;
					break;
				case "right":
					fun = this.right;
					break;
				case "left-up":
					fun = this.leftUp;
					break;
				case "right-up":
					fun = this.rightUp;
					break;
				case "left-down":
					fun = this.leftDown;
					break;
				case "right-down" :
				default:
					fun = this.rightDown;
					break;
			};
			//note 设置触发对象
			Util.addEventHandler(resize, "mousedown", Util.bindApply(this, this.start, fun));
		},
		//note 准备缩放
		start: function(oEvent, fun, touch) {
			//note 防止冒泡(跟拖放配合时设置)
			oEvent.stopPropagation ? oEvent.stopPropagation() : (oEvent.cancelBubble = true);
			//note 设置执行程序
			this._fun = fun;

			//note 样式参数值
			this._styleWidth = this._resizeObj.clientWidth;
			this._styleHeight = this._resizeObj.clientHeight;
			this._styleLeft = this._resizeObj.offsetLeft;
			this._styleTop = this._resizeObj.offsetTop;
			//note 四条边定位坐标
			this._sideLeft = oEvent.clientX - this._styleWidth;
			this._sideRight = oEvent.clientX + this._styleWidth;
			this._sideUp = oEvent.clientY - this._styleHeight;
			this._sideDown = oEvent.clientY + this._styleHeight;
			//note top和left定位参数
			this._fixLeft = this._styleLeft + this._styleWidth;
			this._fixTop = this._styleTop + this._styleHeight;

			//note 缩放比例
			if(this.scale) {
				//note 设置比例
				this.ratio = Math.max(this.ratio, 0) || this._styleWidth / this._styleHeight;
				//note left和top的定位坐标
				this._scaleLeft = this._styleLeft + this._styleWidth / 2;
				this._scaleTop = this._styleTop + this._styleHeight / 2;
			};
			//note 范围限制
			if(this.max) {
				//note 设置范围参数
				var maxLeft = this.maxLeft, maxRight = this.maxRight, maxTop = this.maxTop, maxBottom = this.maxBottom;
				//note 如果设置了容器，再修正范围参数
				if(!!this._container){
					maxLeft = Math.max(maxLeft, 0);
					maxTop = Math.max(maxTop, 0);
					maxRight = Math.min(maxRight, this._container.clientWidth);
					maxBottom = Math.min(maxBottom, this._container.clientHeight);
				};
				//note 根据最小值再修正
				maxRight = Math.max(maxRight, maxLeft + (this.min ? this.minWidth : 0) + this._borderX);
				maxBottom = Math.max(maxBottom, maxTop + (this.min ? this.minHeight : 0) + this._borderY);
				//note 由于转向时要重新设置所以写成function形式
				this._mxSet = function() {
					this._maxRightWidth = maxRight - this._styleLeft - this._borderX;
					this._maxDownHeight = maxBottom - this._styleTop - this._borderY;
					this._maxUpHeight = Math.max(this._fixTop - maxTop, this.min ? this.minHeight : 0);
					this._maxLeftWidth = Math.max(this._fixLeft - maxLeft, this.min ? this.minWidth : 0);
				};
				this._mxSet();
				//note 有缩放比例下的范围限制
				if(this.scale) {
					this._maxScaleWidth = Math.min(this._scaleLeft - maxLeft, maxRight - this._scaleLeft - this._borderX) * 2;
					this._maxScaleHeight = Math.min(this._scaleTop - maxTop, maxBottom - this._scaleTop - this._borderY) * 2;
				}
			}
			//note mousemove时缩放 mouseup时停止
			Util.addEventHandler(document, "mousemove", this._resizeTranscript);
			Util.addEventHandler(document, "mouseup", this._stopTranscript);
			if(BROWSER.ie){
				Util.addEventHandler(this._resizeObj, "losecapture", this._stopTranscript);
				this._resizeObj.setCapture();
			}else{
				Util.addEventHandler(window, "blur", this._stopTranscript);
				oEvent.preventDefault();
			};
		},
		//note 缩放
		resize: function(e) {
			//note 清除选择
			window.getSelection ? window.getSelection().removeAllRanges() : document.selection.empty();
			//note 执行缩放程序
			this._fun(e);
			if(this.options.min && (this._styleWidth <= this.options.minWidth || this._styleHeight <= this.options.minHeight)) {
				return false;
			}
			//note 设置样式，变量必须大于等于0否则ie出错
			with(this._resizeObj.style) {
				width = this._styleWidth + "px"; height = this._styleHeight + "px";
				top = this._styleTop + "px"; left = this._styleLeft + "px";
			}
			//note 附加程序
			this.onResize();
		},
		//note 缩放程序
		//note 上
		up: function(e) {
			this.repairY(this._sideDown - e.clientY, this._maxUpHeight);
			this.repairTop();
			this.turnDown(this.down);
		},
		//note 下
		down: function(e) {
			this.repairY(e.clientY - this._sideUp, this._maxDownHeight);
			this.turnUp(this.up);
		},
		//note 右
		right: function(e) {
			this.repairX(e.clientX - this._sideLeft, this._maxRightWidth);
			this.turnLeft(this.left);
		},
		//note 左
		left: function(e) {
			this.repairX(this._sideRight - e.clientX, this._maxLeftWidth);
			this.repairLeft();
			this.turnRight(this.right);
		},
		//note 右下
		rightDown: function(e) {
			this.repairAngle(
				e.clientX - this._sideLeft, this._maxRightWidth,
				e.clientY - this._sideUp, this._maxDownHeight
			);
			this.turnLeft(this.leftDown) || this.scale || this.turnUp(this.rightUp);
		},
		//note 右上
		rightUp: function(e) {
			this.repairAngle(
				e.clientX - this._sideLeft, this._maxRightWidth,
				this._sideDown - e.clientY, this._maxUpHeight
			);
			this.repairTop();
			this.turnLeft(this.leftUp) || this.scale || this.turnDown(this.rightDown);
		},
		//note 左下
		leftDown: function(e) {
			this.repairAngle(
				this._sideRight - e.clientX, this._maxLeftWidth,
				e.clientY - this._sideUp, this._maxDownHeight
			);
			this.repairLeft();
			this.turnRight(this.rightDown) || this.scale || this.turnUp(this.leftUp);
		},
		//note 左上
		leftUp: function(e) {
			this.repairAngle(
				this._sideRight - e.clientX, this._maxLeftWidth,
				this._sideDown - e.clientY, this._maxUpHeight
			);
			this.repairTop();
			this.repairLeft();
			this.turnRight(this.rightUp) || this.scale || this.turnDown(this.leftDown);
		},
		//note 修正程序
		//note 水平方向
		repairX: function(iWidth, maxWidth) {
			iWidth = this.repairWidth(iWidth, maxWidth);
			if(this.scale){
				var iHeight = this.repairScaleHeight(iWidth);
				if(this.max && iHeight > this._maxScaleHeight){
					iHeight = this._maxScaleHeight;
					iWidth = this.repairScaleWidth(iHeight);
				}else if(this.min && iHeight < this.minHeight){
					var tWidth = this.repairScaleWidth(this.minHeight);
					if(tWidth < maxWidth){ iHeight = this.minHeight; iWidth = tWidth; }
				}
				this._styleHeight = iHeight;
				this._styleTop = this._scaleTop - iHeight / 2;
			}
			this._styleWidth = iWidth;
		},
		//note 垂直方向
		repairY: function(iHeight, maxHeight) {
			iHeight = this.repairHeight(iHeight, maxHeight);
			if(this.scale){
				var iWidth = this.repairScaleWidth(iHeight);
				if(this.max && iWidth > this._maxScaleWidth){
					iWidth = this._maxScaleWidth;
					iHeight = this.repairScaleHeight(iWidth);
				}else if(this.min && iWidth < this.minWidth){
					var tHeight = this.repairScaleHeight(this.minWidth);
					if(tHeight < maxHeight){ iWidth = this.minWidth; iHeight = tHeight; }
				}
				this._styleWidth = iWidth;
				this._styleLeft = this._scaleLeft - iWidth / 2;
			}
			this._styleHeight = iHeight;
		},
		//note 对角方向
		repairAngle: function(iWidth, maxWidth, iHeight, maxHeight) {
			iWidth = this.repairWidth(iWidth, maxWidth);
			if(this.scale) {
				iHeight = this.repairScaleHeight(iWidth);
				if(this.max && iHeight > maxHeight){
					iHeight = maxHeight;
					iWidth = this.repairScaleWidth(iHeight);
				}else if(this.min && iHeight < this.minHeight){
					var tWidth = this.repairScaleWidth(this.minHeight);
					if(tWidth < maxWidth){ iHeight = this.minHeight; iWidth = tWidth; }
				}
			} else {
				iHeight = this.repairHeight(iHeight, maxHeight);
			}
			this._styleWidth = iWidth;
			this._styleHeight = iHeight;
		},
		//note top
		repairTop: function() {
			this._styleTop = this._fixTop - this._styleHeight;
		},
		//note left
		repairLeft: function() {
			this._styleLeft = this._fixLeft - this._styleWidth;
		},
		//note height
		repairHeight: function(iHeight, maxHeight) {
			iHeight = Math.min(this.max ? maxHeight : iHeight, iHeight);
			iHeight = Math.max(this.min ? this.minHeight : iHeight, iHeight, 0);
			return iHeight;
		},
		//note width
		repairWidth: function(iWidth, maxWidth) {
			iWidth = Math.min(this.max ? maxWidth : iWidth, iWidth);
			iWidth = Math.max(this.min ? this.minWidth : iWidth, iWidth, 0);
			return iWidth;
		},
		//note 比例高度
		repairScaleHeight: function(iWidth) {
			return Math.max(Math.round((iWidth + this._borderX) / this.ratio - this._borderY), 0);
		},
		//note 比例宽度
		repairScaleWidth: function(iHeight) {
			return Math.max(Math.round((iHeight + this._borderY) * this.ratio - this._borderX), 0);
		},
		//note 转向程序
		//note 转右
		turnRight: function(fun) {
			if(!(this.min || this._styleWidth)){
				this._fun = fun;
				this._sideLeft = this._sideRight;
				this.max && this._mxSet();
				return true;
			}
		},
		//note 转左
		turnLeft: function(fun) {
			if(!(this.min || this._styleWidth)){
				this._fun = fun;
				this._sideRight = this._sideLeft;
				this._fixLeft = this._styleLeft;
				this.max && this._mxSet();
				return true;
			}
		},
		//note 转上
		turnUp: function(fun) {
			if(!(this.min || this._styleHeight)){
				this._fun = fun;
				this._sideDown = this._sideUp;
				this._fixTop = this._styleTop;
				this.max && this._mxSet();
				return true;
			}
		},
		//note 转下
		turnDown: function(fun) {
			if(!(this.min || this._styleHeight)){
				this._fun = fun;
				this._sideUp = this._sideDown;
				this.max && this._mxSet();
				return true;
			}
		},
		//note 停止缩放
		stop: function() {
			Util.removeEventHandler(document, "mousemove", this._resizeTranscript);
			Util.removeEventHandler(document, "mouseup", this._stopTranscript);
			if(BROWSER.ie){
				Util.removeEventHandler(this._resizeObj, "losecapture", this._stopTranscript);
				this._resizeObj.releaseCapture();
			}else{
				Util.removeEventHandler(window, "blur", this._stopTranscript);
			}
		}
	};
	/**
	 * 初始化拖拽
	 * @param {string} dragId: 要拖拽的层ID
	 * @param {Object} options: 相关参数
	 */
	dzDrag = function() {
		this.options = {
			handle:			'',		//note 指定某一个拖动对象ID
			limit:			false,	//note 是否限制在某一个拖动区域内，设为true后maxLeft、maxRight、maxTop、maxBottom将会生效
			maxLeft:		0,
			maxRight:		9999,
			maxTop:			0,
			maxBottom:		9999,
			container:		'',		//note 指定限制在容器内
			lockX:			false,	//note 锁定X轴
			lockY:			false,	//note 锁定Y轴
			onDragStart:	function(){},
			onDragMove:		function(){},
			onDragEnd:		function(){}
		};
		this.initialize.apply(this, arguments);
	};
	dzDrag.prototype = {
		//note new dzDrag()时被执行
		initialize: function(dragId, options) {
			this.options = Util.setOptions(this.options, options || {});
			this._dragObj = $(dragId);
			//note 记录鼠标相对拖放对象的位置
			this._x = this._y = 0;
			//note 记录margin
			this._marginLeft = this._marginTop = 0;
			//note 当没有指定拖拽对象中的某一个元素为拖拽区域时把拖拽对象当整个拖拽操作区
			this._handle = $(this.options.handle) || this._dragObj;
			this._container = $(this.options.container) || null;
			this._dragObj.style.position = "absolute";
			//note 复制拖拽事件
			this._dragEndTranscript = Util.bindApply(this, this.dragEnd);
			this._dragMoveTranscript = Util.bindApply(this, this.dragMove);
			Util.addEventHandler(this._handle, "mousedown", Util.bindApply(this, this.dragStart));
		},
		dragStart: function(event) {

			this.setLimit();
			//note 记录鼠标相对拖放对象的位置
			this._x = event.clientX - this._dragObj.offsetLeft;
			this._y = event.clientY - this._dragObj.offsetTop;

			//note 记录margin
			var curStyle = Util.currentStyle(this._dragObj);
			this._marginLeft = parseInt(curStyle.marginLeft) || 0;
			this._marginTop = parseInt(curStyle.marginTop) || 0;
			//note 注册mousemove时移动事件， mouseup时停止事件
			Util.addEventHandler(document, "mousemove", this._dragMoveTranscript);
			Util.addEventHandler(document, "mouseup", this._dragEndTranscript);
			if(BROWSER.ie){
				//note 焦点丢失
				Util.addEventHandler(this._handle, "losecapture", this._dragEndTranscript);
				//note 设置鼠标捕获
				this._handle.setCapture();
			}else{
				//note 焦点丢失
				Util.addEventHandler(window, "blur", this._dragEndTranscript);
				//note 阻止默认动作
				event.preventDefault();
			};
			this.options.onDragStart();
		},
		dragMove: function(event) {
			//note 清除选择
			window.getSelection ? window.getSelection().removeAllRanges() : document.selection.empty();
			//note 设置移动参数
			var iLeft = event.clientX - this._x;
			var iTop = event.clientY - this._y;
			//note 设置范围限制
			if(this.options.limit) {
				//note 设置范围参数
				var maxLeft = this.options.maxLeft, maxRight = this.options.maxRight, maxTop = this.options.maxTop, maxBottom = this.options.maxBottom;
				//note 如果设置了容器，再修正范围参数
				if(this._container != null){
					maxLeft = Math.max(maxLeft, 0);
					maxTop = Math.max(maxTop, 0);
					maxRight = Math.min(maxRight, this._container.clientWidth);
					maxBottom = Math.min(maxBottom, this._container.clientHeight);
				};
				//note 修正移动参数
				iLeft = Math.max(Math.min(iLeft, maxRight - this._dragObj.offsetWidth), maxLeft);
				iTop = Math.max(Math.min(iTop, maxBottom - this._dragObj.offsetHeight), maxTop);
			}
			//note 设置位置，并修正margin
			if(!this.options.lockX) { this._dragObj.style.left = iLeft - this._marginLeft + "px"; }
			if(!this.options.lockY) { this._dragObj.style.top = iTop - this._marginTop + "px"; }
			this.options.onDragMove();
		},
		dragEnd: function(event) {
			//note 移除事件
			Util.removeEventHandler(document, "mousemove", this._dragMoveTranscript);
			Util.removeEventHandler(document, "mouseup", this._dragEndTranscript);
			if(BROWSER.ie) {
				Util.removeEventHandler(this._handle, "losecapture", this._dragEndTranscript);
				this._handle.releaseCapture();
			} else {
				Util.removeEventHandler(window, "blur", this._dragEndTranscript);
			}
			this.options.onDragEnd();
		},
		setLimit: function() {
			if(this.options.limit) {
				//note 修正错误范围参数
				this.options.maxRight = Math.max(this.options.maxRight, this.options.maxLeft + this._dragObj.offsetWidth);
				this.options.maxBottom = Math.max(this.options.maxBottom, this.options.maxTop + this._dragObj.offsetHeight);
				//note 如果有容器必须设置position为relative或absolute来相对或绝对定位，并在获取offset之前设置
				!this._container || Util.currentStyle(this._container).position == "relative" || Util.currentStyle(this._container).position == "absolute" || (this._container.style.position = "relative");
			}
		}
	};
	var Util = {
		/**
		 * 修改对象值
		 * @param {Object} object:目标对象
		 * @param {Object} source:数据源
		 */
		setOptions: function(object, source) {
			for(var property in source) {
				object[property] = source[property];
			}
			return object;
		},
		/**
		 * 往某一个对象中的事件添加操作
		 * @param {Object} targetObj: 目标对象
		 * @param {Object} eventType: 不含'on'的事件类型，例如"click"、"mouseover"等
		 * @param {Object} funHandler: 事件待执行的function
		 */
		addEventHandler: function(targetObj, eventType, funHandler) {
			if(targetObj.addEventListener) {
				targetObj.addEventListener(eventType, funHandler, false);
			} else if (targetObj.attachEvent) {
				targetObj.attachEvent("on" + eventType, funHandler);
			} else {
				targetObj["on" + eventType] = funHandler;
			}
		},
		/**
		 * 从某一个对象中的事件删除操作
		 * @param {Object} targetObj: 目标对象
		 * @param {Object} eventType: 不含'on'的事件类型，例如"click"、"mouseover"等
		 * @param {Object} funHandler: 事件待执行的function
		 */
		removeEventHandler: function(targetObj, eventType, funHandler) {
			if(targetObj.removeEventListener) {
				targetObj.removeEventListener(eventType, funHandler, false);
			} else if (targetObj.detachEvent) {
				targetObj.detachEvent("on" + eventType, funHandler);
			} else {
				targetObj["on" + eventType] = null;
			}
		},
		bindApply: function(object, fun) {
			var args = Array.prototype.slice.call(arguments).slice(2);
			return function(event) {
				return fun.apply(object, [event || window.event].concat(args));
			};
		},
		currentStyle: function(element){
			return element.currentStyle || document.defaultView.getComputedStyle(element, null);
		}
	};
})();
/*
	[Discuz!] (C)2001-2099 Comsenz Inc.
	This is NOT a freeware, use is subject to license terms

	$Id: common_diy.js 31093 2012-07-16 03:54:34Z zhangguosheng $
*/

String.prototype.property2js = function(){
	var t = this.replace(/-([a-z])/g, function($0, $1) {return $1.toUpperCase();});
	return t;
};
/**
 * 添加新的样式rule;
 * @param : n styleSheets 索引;
 * 代码 : var ss = new styleSheet(0);
 */
function styleCss(n) {
	if(typeof n == "number") {
		var _s = document.styleSheets[n];
	} else {
		return false;
	}
	this.sheet = _s;
	this.rules = _s.cssRules ? _s.cssRules : _s.rules;
};

/**
 * 查找样式 rule，成功返回 index,否则返回 -1;
 * @param : selector 为 rule 名称;
 * 代码 : var ss = new styleSheet(0); ss.indexOf("className");
 */
styleCss.prototype.indexOf = function(selector) {
	for(var i = 0; i < this.rules.length; i++) {
		if (typeof(this.rules[i].selectorText) == 'undefined') continue;
		if(this.rules[i].selectorText == selector) {
			return i;
		}
	}
	return -1;
};

/**
 * 描述 : 删除样式 rule;
 * @param : n 为 rule 索引或者样式名称;
 * 代码 : var ss = new styleSheet(0); ss.removeRule(0);
 */
styleCss.prototype.removeRule = function(n) {
	if(typeof n == "number") {
		if(n < this.rules.length) {
			this.sheet.removeRule ? this.sheet.removeRule(n) : this.sheet.deleteRule(n);
		}
	} else {
		var i = this.indexOf(n);
		if (i>0) this.sheet.removeRule ? this.sheet.removeRule(i) : this.sheet.deleteRule(i);
	}
};

/**
 * 描述 : 添加新的样式 rule;
 * @param : selector 样式 rule 名称;
 * @param : styles 样式 rule 的 style;
 * @param : n 位置;
 * @param : porperty 属性;
 * 代码 : var ss = new styleSheet(0); ss.addRule("className","color:red",0);
 */
styleCss.prototype.addRule = function(selector, styles, n, porperty) {
	var i = this.indexOf(selector);
	var s = '';
	var reg = '';
	if (i != -1) {
		reg = new RegExp('^'+porperty+'.*;','i');
		s = this.getRule(selector);
		if (s) {
			s = s.replace(selector,'').replace('{', '').replace('}', '').replace(/  /g, ' ').replace(/^ | $/g,'');
			s = (s != '' && s.substr(-1,1) != ';') ? s+ ';' : s;
			s = s.toLowerCase().replace(reg, '');
			if (s.length == 1) s = '';
		}
		this.removeRule(i);
	}
	s = s.indexOf('!important') > -1 || s.indexOf('! important') > -1 ? s : s.replace(/;/g,' !important;');
	s = s + styles;
	if (typeof n == 'undefined' || !isNaN(n)) {
		n = this.rules.length;
	}
	if (this.sheet.insertRule) {
		this.sheet.insertRule(selector+'{'+s+'}', n);
	} else {
		if (s) this.sheet.addRule(selector, s, n);
	}
};

/**
 * 描述 : 设置样式rule具体的属性;
 * @param : selector 样式rule名称;
 * @param : attribute 样式rule的属性;
 * @param : value 指定value值;
 * 代码 : ss.setrule("#logo", "color:", "green");
 */
styleCss.prototype.setRule = function(selector, attribute, value) {
	var i = this.indexOf(selector);
	if(-1 == i) return false;
	this.rules[i].style[attribute] = value;
	return true;
};

/**
 * 描述 : 获得样式rule具体的属性;
 * @param : selector 样式rule名称;
 * @param : attribute 样式rule的属性;
 * 代码 : ss.getrule("#logo", "color");
 */
styleCss.prototype.getRule = function(selector, attribute) {
	var i = this.indexOf(selector);
	if(-1 == i) return '';
	var value = '';
	if (typeof attribute == 'undefined') {
		value = typeof this.rules[i].cssText != 'undefined' ? this.rules[i].cssText : this.rules[i].style['cssText'];
	} else {
		value = this.rules[i].style[attribute];
	}
	return typeof value != 'undefined' ? value : '';
};
styleCss.prototype.removeAllRule = function(noSearch) {
	var num = this.rules.length;
	var j = 0;
	for(var i = 0; i < num; i ++) {
		var selector = this.rules[this.rules.length - 1 - j].selectorText;
		if(noSearch == 1) {
			this.sheet.removeRule ? this.sheet.removeRule(this.rules.length - 1 - j) : this.sheet.deleteRule(this.rules.length - 1 - j);
		} else {
			j++;
		}
	}
};
if (!Array.prototype.indexOf) {
	Array.prototype.indexOf = function (element, index) {
		var length = this.length;
		if (index == null) {
			index = 0;
		} else {
			index = (!isNaN(index) ? index : parseInt(index));
			if (index < 0) index = length + index;
			if (index < 0) index = 0;
		}
		for (var i = index; i < length; i++) {
			var current = this[i];
			if (!(typeof(current) === 'undefined') || i in this) {
				if (current === element) return i;
			}
		}
		return -1;
	};
}
if (!Array.prototype.filter){
	Array.prototype.filter = function(fun , thisp){
	var len = this.length;
	if (typeof fun != "function")
	throw new TypeError();
	var res = new Array();
	var thisp = arguments[1];
	for (var i = 0; i < len; i++){
		if (i in this){
			var val = this[i]; //note  in case fun mutates this
			if (fun.call(thisp, val, i, this)) res.push(val);
		}
	}
	return res;
	};
}
var Util = {
	event: function(event){
		//note 增加W3C标准事件方法
		Util.e = event || window.event;
		Util.e.aim = Util.e.target || Util.e.srcElement;
		if (!Util.e.preventDefault) {
			Util.e.preventDefault = function(){
				Util.e.returnValue = false;
			};
		}
		if (!Util.e.stopPropagation) {
			Util.e.stopPropagation = function(){
				Util.e.cancelBubble = true;
			};
		}
		if (typeof Util.e.layerX == "undefined") {
			Util.e.layerX = Util.e.offsetX;
		}
		if (typeof Util.e.layerY == "undefined") {
			Util.e.layerY = Util.e.offsetY;
		}
		if (typeof Util.e.which == "undefined") {
			Util.e.which = Util.e.button;
		}
		return Util.e;
	},
	url: function(s){
		var s2 = s.replace(/(\(|\)|\,|\s|\'|\"|\\)/g, '\\$1');
		if (/\\\\$/.test(s2)) {
			s2 += ' ';
		}
		return "url('" + s2 + "')";
	},
	trimUrl : function(s){
		var s2 = s.toLowerCase().replace(/url\(|\"|\'|\)/g,'');
		return s2;
	},
	swapDomNodes: function(a, b){
		var afterA = a.nextSibling;
		if (afterA == b) {
			swapDomNodes(b, a);
			return;
		}
		var aParent = a.parentNode;
		b.parentNode.replaceChild(a, b);
		aParent.insertBefore(b, afterA);
	},
	hasClass: function(el, name){
		return el && el.nodeType == 1 && el.className.split(/\s+/).indexOf(name) != -1;
	},
	addClass: function(el, name){
		el.className += this.hasClass(el, name) ? '' : ' ' + name;
	},
	removeClass: function(el, name){
		var names = el.className.split(/\s+/);
		el.className = names.filter(function(n){
			return name != n;
		}).join(' ');
	},
	getTarget: function(e, attributeName, value){
		var target = e.target || e.srcElement;
		while (target != null) {
			if (attributeName == 'className') {
				if (this.hasClass(target, value)) {
					return target;
				}
			}else if (target[attributeName] == value) {
					return target;
			}
			target = target.parentNode;
		}
		return false;
	},
	getOffset:function (el, isLeft) {
		var  retValue  = 0 ;
		while  (el != null ) {
			retValue  +=  el["offset" + (isLeft ? "Left" : "Top" )];
			el = el.offsetParent;
		}
		return  retValue;
	},
	insertBefore: function (newNode, targetNode) {
		var parentNode = targetNode.parentNode;
		var next = targetNode.nextSibling;
		if (targetNode.id && targetNode.id.indexOf('temp')>-1) {
			parentNode.insertBefore(newNode,targetNode);
		} else if (!next) {
			parentNode.appendChild(newNode);
		} else {
			parentNode.insertBefore(newNode,targetNode);
		}
	},
	insertAfter : function (newNode, targetNode) {
		var parentNode = targetNode.parentNode;
		var next = targetNode.nextSibling;
		if (next) {
			parentNode.insertBefore(newNode,next);
		} else {
			parentNode.appendChild(newNode);
		}
	},
	getScroll: function () {
		var t, l, w, h;
		if (document.documentElement && document.documentElement.scrollTop) {
			t = document.documentElement.scrollTop;
			l = document.documentElement.scrollLeft;
			w = document.documentElement.scrollWidth;
			h = document.documentElement.scrollHeight;
		} else if (document.body) {
			t = document.body.scrollTop;
			l = document.body.scrollLeft;
			w = document.body.scrollWidth;
			h = document.body.scrollHeight;
		}
		return {t: t, l: l, w: w, h: h};
	},
	hide:function (ele){
		if (typeof ele == 'string') {ele = $(ele);}
		if (ele){ele.style.display = 'none';ele.style.visibility = 'hidden';}
	},
	show:function (ele){
		if (typeof ele == 'string') {ele = $(ele);}
		if (ele) {
			this.removeClass(ele, 'hide');
			ele.style.display = '';
			ele.style.visibility = 'visible';
		}
	},
	//note 取消选择
	cancelSelect : function () {
		window.getSelection ? window.getSelection().removeAllRanges() : document.selection.empty();
	},
	//note 选择的文字
	getSelectText : function () {
		var t = '';
		 if (window.getSelection) {
			t = window.getSelection();
		} else if (document.getSelection) {
			t = document.getSelection();
		} else if (document.selection) {
			t = document.selection.createRange().text;
		} else {
			t = '';
		}
		return t;
	},
	//note 显示或隐藏元素
	toggleEle : function (ele) {
		ele = (typeof ele !='object') ? $(ele) : ele;
		if (!ele) return false;
		var value = this.getFinallyStyle(ele,'display');
		if (value =='none') {
			this.show(ele);
			this.hide($('uploadmsg_button'));
		} else {
			this.hide(ele);
			this.show($('uploadmsg_button'));
		}
	},
	//note 得到元素当前样式
	getFinallyStyle : function (ele,property) {
		ele = (typeof ele !='object') ? $(ele) : ele;
		var style = (typeof(ele['currentStyle']) == 'undefined') ? window.getComputedStyle(ele,null)[property] : ele['currentStyle'][property];
		if (typeof style == 'undefined' && property == 'backgroundPosition') {
			style = ele['currentStyle']['backgroundPositionX'] + ' ' +ele['currentStyle']['backgroundPositionY'];
		}
		return style;
	},
	//note IE6的渲染bug
	recolored:function (){
		var b = document.body;
		b.style.zoom = b.style.zoom=="1"?"100%":"1";
	},
	/**
	 *len 长度
	 *type 1为数字，2为数字+小写字母，3为数字+大小字母；默认为3
	 */
	getRandom : function (len,type) {
		len = len < 0 ? 0 : len;
		type = type && type<=3? type : 3;
		var str = '';
		for (var i = 0; i < len; i++) {
			var j = Math.ceil(Math.random()*type);
			if (j == 1) {
				str += Math.ceil(Math.random()*9);
			} else if (j == 2) {
				str += String.fromCharCode(Math.ceil(Math.random()*25+65));
			} else {
				str += String.fromCharCode(Math.ceil(Math.random()*25+97));
			}
		}
		return str;
	},
	fade : function(obj,timer,ftype,cur,fn) {
		if (this.stack == undefined) {this.stack = [];}
		obj = typeof obj == 'string' ? $(obj) : obj;
		if (!obj) return false;

		//note 去重
		for (var i=0;i<this.stack.length;i++) {
			if (this.stack[i] == obj && (cur == 0 || cur == 100)) return false;
		}
		if (cur == 0 || cur == 100) {this.stack.push(obj);}

		ftype = ftype != 'in' && ftype != 'out' ? 'out' : ftype;
		timer = timer || 400;
		var step = 100/(timer/20);
		obj.style.filter = 'Alpha(opacity=' + cur + ')';
		obj.style.opacity = cur / 100;
		cur = ftype == 'in' ? cur + step : cur - step ;
		var fadeTimer = (function(){
			return setTimeout(function () {
				Util.fade(obj, timer, ftype, cur, fn);
			}, 20);
			})();
		this[ftype == 'in' ? 'show' : 'hide'](obj);
		if(ftype == 'in' && cur >= 100 || ftype == 'out' && cur <= 0) {
			clearTimeout(fadeTimer);
			//note 删除执行完的
			for (i=0;i<this.stack.length;i++) {
				if (this.stack[i] == obj ) {
					this.stack.splice(i,1);break;
				}
			}

			fn = fn || function(){};
			fn(obj);
		}
		return obj;
	},
	fadeIn : function (obj,timer,fn) {
		return this.fade(obj, timer, 'in', 0, fn);
	},
	fadeOut : function (obj,timer,fn) {
		return this.fade(obj, timer, 'out', 100, fn);
	},
	//note 元素style属性的值，返回字符串
	getStyle : function (ele) {
		if (ele) {
			var s = ele.getAttribute('style') || '';
			return typeof s == 'object' ? s.cssText : s;
		}
		return false;
	},
	//note 设置元素style属性的值
	setStyle : function (ele,cssText) {
		if (ele) {
			var s = ele.getAttribute('style') || '';
			return typeof s == 'object' ? s.cssText = cssText : ele.setAttribute('style',cssText);
		}
		return false;
	},
	getText : function (ele) {
		var t = ele.innerText ? ele.innerText : ele.textContent;
		return !t ? '' : t;
	},
	rgb2hex : function (color) {
		if (!color) return '';
		var reg = new RegExp('(\\d+)[, ]+(\\d+)[, ]+(\\d+)','g');
		var rgb = reg.exec(color);
		if (rgb == null) rgb = [0,0,0,0];
		var red = rgb[1], green = rgb[2], blue = rgb[3];
		var decColor = 65536 * parseInt(red) + 256 * parseInt(green) + parseInt(blue);
		var hex = decColor.toString(16).toUpperCase();
		var pre = new Array(6 - hex.length + 1).join('0');
		hex = pre + hex;
		return hex;
	},
	//note 格式化颜色值
	formatColor : function (color) {
		return color == '' || color.indexOf('#')>-1 || color.toLowerCase() == 'transparent' ? color : '#'+Util.rgb2hex(color);
	}
};

(function(){
	//note 框架
	Frame = function(name, className, top, left, moveable){
		this.name = name;
		this.top = top;
		this.left = left;
		this.moveable = moveable ? true : false;
		this.columns = [];
		this.className = className;
		this.titles = [];
		if (typeof Frame._init == 'undefined') {
			Frame.prototype.addColumn = function (column) {
				if (column instanceof Column) {
					this.columns[column.name] = column;
				}
			};
			Frame.prototype.addFrame = function(columnId, frame) {
				if (frame instanceof Frame || frame instanceof Tab){
					this.columns[columnId].children.push(frame);
				}
			};
			Frame.prototype.addBlock = function(columnId, block) {
				if (block instanceof Block){
					this.columns[columnId].children.push(block);
				}
			};
		}
		Frame._init = true;
	};

	Column = function (name, className) {
		this.name = name;
		this.className = className;
		this.children = [];
	};
	Tab = function (name, className, top, left, moveable) {
		Frame.apply(this, arguments);
	};
	Tab.prototype = new Frame();
	//note 模块
	Block = function(name, className, top, left) {
		this.name = name;
		this.top = top;
		this.left = left;
		this.className = className;
		this.titles = [];
	};

	//note 拖动
	Drag = function () {
		this.data = [];
		this.scroll = {};
		this.menu = [];
		this.data = [];
		this.allBlocks = []; //note 所有模块的BID
		this.overObj = '';
		this.dragObj = '';
		this.dragObjFrame = '';
		this.overObjFrame = '';
		this.isDragging = false;
		this.layout = 2;
		this.frameClass = 'frame';
		this.blockClass = 'block';
		this.areaClass = 'area';
		this.moveableArea = []; //note 可移动区域id， 多个以逗号分隔
		this.moveableColumn = 'column'; //note className
		this.moveableObject = 'move-span'; //note className
		this.titleClass = 'title'; //note 标题样式名
		this.hideClass = 'hide'; //note 隐藏样式名
		this.titleTextClass = 'titletext'; //note 标题文字样式名
		this.frameTitleClass = 'frame-title',//note 框架标题的样式
		this.tabClass = 'frame-tab'; //note tab的样式名
		this.tabActivityClass = 'tabactivity'; //note 激活标签的样式名
		this.tabTitleClass = 'tab-title'; //note tab标题的样式名
		this.tabContentClass = 'tb-c'; //note tab内容的样式名
		this.moving = 'moving'; //note className
		this.contentClass = 'dxb_bc'; //note 内容的样式名
		this.tmpBoxElement = null ; //note  移动时显示的占位虚线框
		this.dargRelative = {}; //note 拖动元素的原相邻元素
		this.scroll = {};
		this.menu = []; //note 菜单数组
		this.rein = []; //note 要停止执行的命令
		this.newFlag = false;
		this.isChange = false; //note 是否改变数据
		this.fn = '';
		this._replaceFlag = false; //note 正式版兼容问题替换标识，只替换一次
		this.sampleMode = false; //note 是否为简洁模式
		this.sampleBlocks = null; //note 简洁模式下的所有模块
		this.advancedStyleSheet = null; //note 高级模式下用的样式表
	};
	Drag.prototype = {
		//note  得到移动时显示的占位虚线框
		getTmpBoxElement : function () {
			if  (!this.tmpBoxElement) {
				this.tmpBoxElement = document.createElement("div");
				this.tmpBoxElement.id = 'tmpbox';
				this.tmpBoxElement.className = "tmpbox" ;
				this.tmpBoxElement.style.width = this.overObj.offsetWidth-4+"px";
				this.tmpBoxElement.style.height = this.overObj.offsetHeight-4+"px";
			} else if (this.overObj && this.overObj.offsetWidth > 0) {
				this.tmpBoxElement.style.width = this.overObj.offsetWidth-4+"px";
			}
			return this.tmpBoxElement;
		},
		//note  模块排放位置的字符串
		getPositionStr : function (){
			this.initPosition();
			var start = '<?xml version="1.0" encoding="ISO-8859-1"?><root>';
			var end ="</root>";
			var str = "";
			for (var i in this.data) {
				if (typeof this.data[i] == 'function') continue;
				str += '<item id="' + i + '">';
				for (var j in this.data[i]) {
					if (!(this.data[i][j] instanceof Frame || this.data[i][j] instanceof Tab)) continue;
					str += this._getFrameXML(this.data[i][j]);
				}
				str += '</item>';
			}
			return start + str + end;
		},
		//note  得到框架的XML
		_getFrameXML : function (frame) {
			if (!(frame instanceof Frame || frame instanceof Tab) || frame.name.indexOf('temp') > 0) return '';
			var itemId = frame instanceof Tab ? 'tab' : 'frame';
			var Cstr = "";
			//note 分析单个框架
			var name = frame.name;
			var frameAttr = this._getAttrXML(frame);
			var columns = frame['columns'];
			for (var j in columns) {
				if (columns[j] instanceof Column) {
					var Bstr = '';
					var colChildren = columns[j].children;
					for (var k in colChildren) {
						if (k == 'attr' || typeof colChildren[k] == 'function' || colChildren[k].name.indexOf('temp') > 0) continue;
						//note 分析单个模块
						if (colChildren[k] instanceof Block) {
							Bstr += '<item id="block`' + colChildren[k]['name'] + '">';
							Bstr += this._getAttrXML(colChildren[k]);
							Bstr += '</item>';
						} else if (colChildren[k] instanceof Frame || colChildren[k] instanceof Tab) {
							Bstr += this._getFrameXML(colChildren[k]);
						}
					}
					var columnAttr = this._getAttrXML(columns[j]);
					Cstr += '<item id="column`' + j + '">' + columnAttr + Bstr + '</item>';
				}
			}
			return '<item id="' + itemId + '`' + name + '">' + frameAttr + Cstr + '</item>';
		},
		_getAttrXML : function (obj) {
			var attrXml = '<item id="attr">';
			var trimAttr = ['left', 'top'];
			var xml = '';
			if (obj instanceof Frame || obj instanceof Tab || obj instanceof Block || obj instanceof Column) {
				for (var i in obj) {
					if (i == 'titles') {
						xml += this._getTitlesXML(obj[i]);
					}
					if (!(typeof obj[i] == 'object' || typeof obj[i] == 'function')) {
						if (trimAttr.indexOf(i) >= 0) continue;
						xml += '<item id="' + i + '"><![CDATA[' + obj[i] + ']]></item>';
					}
				}
			}else {
				xml += '';
			}
			return attrXml + xml + '</item>';
		},
		//note 标题xml化
		_getTitlesXML : function (titles) {
			var xml = '<item id="titles">';
			for (var i in titles) {
				if (typeof titles[i] == 'function') continue;
				xml += '<item id="'+i+'">';
				for (var j in titles[i]) {
					if (typeof titles[i][j] == 'function') continue;
					xml += '<item id="'+j+'"><![CDATA[' + titles[i][j] + ']]></item>';
				}
				xml += '</item>';
			}
			xml += '</item>';
			return xml;
		},
		//note  得到当前可插入的模块的ID
		getCurrentOverObj : function (e) {
			//note  鼠标坐标
			//note var _clientX = e.clientX;
			//note var _clientY = e.clientY;
			var _clientX = parseInt(this.dragObj.style.left);
			var _clientY = parseInt(this.dragObj.style.top);
			var max = 10000000;
			for (var i in this.data) {
				for (var j in this.data[i]) {
					if (!(this.data[i][j] instanceof Frame || this.data[i][j] instanceof Tab)) continue;
					var min = this._getMinDistance(this.data[i][j], max);
					if (min.distance < max) {
						var id = min.id;
						max = min.distance;
					}
				}
			}
			return $(id);
		},
		_getMinDistance : function (ele, max) {
			if(ele.name==this.dragObj.id) return {"id":ele.name, "distance":max};
			var _clientX = parseInt(this.dragObj.style.left);
			var _clientY = parseInt(this.dragObj.style.top);
			var id;
			//note 是否在Tab中直接拖入Tab框架(Tab能拖入Tab的子普通框架中，不能直接拖入Tab框架中)
			var isTabInTab = Util.hasClass(this.dragObj, this.tabClass) && Util.hasClass($(ele.name).parentNode.parentNode, this.tabClass);
			//note 可拖入Frame和Tab里
			if (ele instanceof Frame || ele instanceof Tab) {
				//note 如果当前遍历到的ele可移动,则可在其位置插入将当前ele，并且不是在Tab中直接拖入Tab框架
				if (ele.moveable && !isTabInTab) {
					//note 框架和Tab框架可以相互多层嵌套
					var isTab = Util.hasClass(this.dragObj, this.tabClass);
					//note 普通框架
					var isFrame = Util.hasClass(this.dragObj, this.frameClass);
					//note 模块只能拖入框架和Tab的列中
					var isBlock = Util.hasClass(this.dragObj, this.blockClass) && Util.hasClass($(ele.name).parentNode, this.moveableColumn);
					if ( isTab || isFrame || isBlock) {
						var _Y = ele['top'] - _clientY;
						var _X = ele['left'] - _clientX;
						var distance = Math.sqrt(Math.pow(_X, 2) + Math.pow(_Y, 2));
						if (distance < max) {
							max = distance;
							id = ele.name;
						}
					}
				}
				//note 遍历每列中的子ele
				for (var i in ele['columns']) {
					var column = ele['columns'][i];
					if (column instanceof Column) {
						for (var j in column['children']) {
							if ((column['children'][j] instanceof Tab || column['children'][j] instanceof Frame || column['children'][j] instanceof Block)) {
								var min = this._getMinDistance(column['children'][j], max);
								if (min.distance < max) {
									id = min.id;
									max = min.distance;
								}
							}
						}
					}
				}
				return {"id":id, "distance":max};
			} else {
				//note 是在Tab中直接拖入Tab框架则直接返回
				if (isTabInTab) return {'id': ele['name'], 'distance': max};
				var _Y = ele['top'] - _clientY;
				var _X = ele['left'] - _clientX;
				var distance = Math.sqrt(Math.pow(_X, 2) + Math.pow(_Y, 2));
				if (distance < max) {
					return {'id': ele['name'], 'distance': distance};
				} else {
					return {'id': ele['name'], 'distance': max};
				}
			}
		},
		//note 根据名称得到模块或框架
		getObjByName : function (name, data) {
			if (!name) return false;
			data = data || this.data;
			if ( data instanceof Frame) {
				if (data.name == name) {
					return data;
				} else {
					var d = this.getObjByName(name,data['columns']);
					if (name == d.name) return d;
				}
			} else if (data instanceof Block) {
				if (data.name == name) return data;
			} else if (typeof data == 'object') {
				for (var i in data) {
					var d = this.getObjByName(name, data[i]);
					if (name == d.name) return d;
				}
			}
			return false;
		},
		//note  初始化模块排放位置
		initPosition : function () {
			this.data = [],this.allBlocks = [];
			var blocks = $C(this.blockClass);
			for(var i = 0; i < blocks.length; i++) {
				if (blocks[i]['id'].indexOf('temp') < 0) {
					this.checkEdit(blocks[i]);
					this.allBlocks.push(blocks[i]['id'].replace('portal_block_',''));
				}
			}
			//note 可移动区域
			var areaLen = this.moveableArea.length;
			for (var j = 0; j < areaLen; j++ ) {
				var area = this.moveableArea[j];
				var areaData = [];
				if (typeof area == 'object') {
					//note 所有的框架
					this.checkTempDiv(area.id);
					var frames = area.childNodes;
					for (var i in frames) {
						if (typeof(frames[i]) != 'object') continue;
						if (Util.hasClass(frames[i], this.frameClass) || Util.hasClass(frames[i], this.blockClass)
							|| Util.hasClass(frames[i], this.tabClass) || Util.hasClass(frames[i], this.moveableObject)) {
							areaData.push(this.initFrame(frames[i]));
						}
					}
					this.data[area.id] = areaData;
				}
			}
			//note 处理frame列的样式,兼容正式版问题,替换标记
			this._replaceFlag = true;
		},
		removeBlockPointer : function(e) {this.removeBlock(e);},
		//note 显示\隐藏内容部分
		toggleContent : function (e) {
			if ( typeof e !== 'string') {
				e = Util.event(e);
				var id = e.aim.id.replace('_edit_toggle','');
			} else {
				id = e;
			}
			var obj = this.getObjByName(id);
			var display = '';
			if (obj instanceof Block || obj instanceof Tab) {
				display = $(id+'_content').style.display;
				Util.toggleEle(id+'_content');
			} else {
				var col = obj.columns;
				for (var i in col) {
					display = $(i).style.display;
					Util.toggleEle($(i));
				}
			}

			if(display != '') {
				e.aim.src=STATICURL+'/image/common/fl_collapsed_no.gif';
			} else {
				e.aim.src=STATICURL+'/image/common/fl_collapsed_yes.gif';
			}
		},
		//note check Edit
		checkEdit : function (ele) {
			if (!ele || Util.hasClass(ele, 'temp') || ele.getAttribute('noedit')) return false;
			var id = ele.id;
			var _method = this;
			if (!$(id+'_edit')) {
				var _method = this;
				var dom = document.createElement('div');
				dom.className = 'edit hide';
				dom.id = id+'_edit';
				//note dom.innerHTML = ' <img id="'+id+'_edit_menu" src="'+STATICURL+'/image/common/arw_d.gif" alt="编辑" title="编辑 "/> <img id="'+id+'_edit_toggle" src="'+STATICURL+'/image/common/fl_collapsed_no.gif" /> <img id="'+id+'_edit_remove" src="'+STATICURL+'/image/common/data_invalid.gif" alt="删除" title="删除" />';
				dom.innerHTML = '<span id="'+id+'_edit_menu">编辑</span>';
				ele.appendChild(dom);
				//note $(id+'_edit_toggle').onclick = function (e){Drag.prototype.toggleContent.call(_method, e, this)};
				//note $(id+'_edit_remove').onclick = function (e){Drag.prototype.removeBlockPointer.call(_method, e, this);};
				$(id+'_edit_menu').onclick = function (e){Drag.prototype.toggleMenu.call(_method, e, this);};
			}
			ele.onmouseover = function (e) {Drag.prototype.showEdit.call(_method,e);};
			ele.onmouseout = function (e) {Drag.prototype.hideEdit.call(_method,e);};
		},
		//note  初始化框架
		initFrame : function (frameEle) {
			if (typeof(frameEle) != 'object') return '';
			var frameId = frameEle.id;
			//note check edit div
			if(!this.sampleMode) {
				this.checkEdit(frameEle);
			}
			//note 绑定事件
			var moveable = Util.hasClass(frameEle, this.moveableObject);
			var frameObj = '';
			if (Util.hasClass(frameEle, this.tabClass)) {
				this._initTabActivity(frameEle);
				frameObj = new Tab(frameId, frameEle.className, Util.getOffset(frameEle,false), Util.getOffset(frameEle,true), moveable);
			} else if (Util.hasClass(frameEle, this.frameClass) || Util.hasClass(frameEle, this.moveableObject)) {
				//note 正式版兼容问题，只替换一次
				if (Util.hasClass(frameEle, this.frameClass) && !this._replaceFlag)	this._replaceFrameColumn(frameEle);
				frameObj = new Frame(frameId, frameEle.className, Util.getOffset(frameEle,false), Util.getOffset(frameEle,true), moveable);
			}
			//note  框架中的列处理
			this._initColumn(frameObj, frameEle);

			return frameObj;
		},
		//note  框架中的列处理
		_initColumn : function (frameObj,frameEle) {
			var columns = frameEle.children;
			if (Util.hasClass(frameEle.parentNode.parentNode,this.tabClass)) {
				var col2 = $(frameEle.id+'_content').children;
				var len = columns.length;
				for (var i in col2) {
					if (typeof(col2[i]) == 'object') columns[len+i] = col2[i];
				}
			}
			//note 框架的所有列
			for (var i in columns) {
				if (typeof(columns[i]) != 'object') continue;
				if (Util.hasClass(columns[i], this.titleClass)) {
					//note 处理数组对像的Title信息
					this._initTitle(frameObj, columns[i]);
				}
				//note 处理dom元素的Title信息,需要放if外处理,确保无论有无标题都显示
				this._initEleTitle(frameObj, frameEle);
				if (Util.hasClass(columns[i], this.moveableColumn)) {
					//note 某一列
					var columnId = columns[i].id;
					var column = new Column(columnId, columns[i].className);
					frameObj.addColumn(column);
					this.checkTempDiv(columnId);
					//note 某一列中所有可移动的元素
					var elements = columns[i].children;
					var eleLen = elements.length;
					for (var j = 0; j < eleLen; j++) {
						//note 某一可移动元素
						var ele = elements[j];
						if (Util.hasClass(ele, this.frameClass) || Util.hasClass(ele, this.tabClass)) {
							var frameObj2 = this.initFrame(ele);
							frameObj.addFrame(columnId, frameObj2);
						//note 添加block和可移动元素
						} else if (Util.hasClass(ele, this.blockClass) || Util.hasClass(ele, this.moveableObject)) {
							var block = new Block(ele.id, ele.className, Util.getOffset(ele, false), Util.getOffset(ele, true));
							for (var k in ele.children) {
								if (Util.hasClass(ele.children[k], this.titleClass)) this._initTitle(block, ele.children[k]);
							}
							//note 处理对象的title
							this._initEleTitle(block, ele);
							frameObj.addBlock(columnId, block);
						}
					}
				//note tab框架中内容部分的分析
				}
			}
		},
		//note 处理标题信息
		_initTitle : function (obj, ele) {
			if (Util.hasClass(ele, this.titleClass)) {
				obj.titles['className'] = [ele.className];
				obj.titles['style'] = {};
				if (ele.style.backgroundImage) obj.titles['style']['background-image'] = ele.style.backgroundImage;
				if (ele.style.backgroundRepeat) obj.titles['style']['background-repeat'] = ele.style.backgroundRepeat;
				if (ele.style.backgroundColor) obj.titles['style']['background-color'] = ele.style.backgroundColor;
				if (obj instanceof Tab) {
					obj.titles['switchType'] = [];
					obj.titles['switchType'][0] = ele.getAttribute('switchtype') ? ele.getAttribute('switchtype') : 'click';
				}
				var ch = ele.children;
				for (var k in ch) {
					//note 标题信息
					if (Util.hasClass(ch[k], this.titleTextClass)){
						this._getTitleData(obj, ch[k], 'first');
					//note 副标题信息，不包括tab框架中的Frame/Block
					} else if (typeof ch[k] == 'object' && !Util.hasClass(ch[k], this.moveableObject)) {
						this._getTitleData(obj, ch[k]);
					}
				}
			}
		},
		//note 标题的数据，包括文字、连接地址、大小、颜色
		_getTitleData : function (obj, ele, i) {
			var shref = '',ssize = '',sfloat = '',scolor = '',smargin = '',stext = '', src = '';
			//note 连接
			var collection = ele.getElementsByTagName('a');
			if (collection.length > 0) {
				 shref = collection[0]['href'];
				 scolor = collection[0].style['color'] + ' !important';
			}
			//note 图片
			collection = ele.getElementsByTagName('img');
			if (collection.length > 0) {
				 src = collection[0]['src'];
			}

			stext = Util.getText(ele);

			if (stext || src) {
				scolor = scolor ? scolor : ele.style['color'];
				sfloat = ele.style['styleFloat'] ? ele.style['styleFloat'] : ele.style['cssFloat'] ;
				sfloat = sfloat == undefined ? '' : sfloat;
				var margin_ = sfloat == '' ? 'left' : sfloat;
				smargin = parseInt(ele.style[('margin-'+margin_).property2js()]);
				smargin = smargin ? smargin : '';
				ssize = parseInt(ele.style['fontSize']);
				ssize = ssize ? ssize : '';
				var data = {'text':stext, 'href':shref,'color':scolor, 'float':sfloat, 'margin':smargin, 'font-size':ssize, 'className':ele.className, 'src':src};
				if (i) {
					obj.titles[i] = data;
				} else {
					obj.titles.push(data);
				}
			}
		},
		//note 设置模块或框架的title属性值
		_initEleTitle : function (obj,ele) {
			if (Util.hasClass(ele, this.moveableObject)) {
				if (obj.titles['first'] && obj.titles['first']['text']) {
					var title = obj.titles['first']['text'];
				} else {
					var title = obj.name;
				}
			}
		},
		showBlockName : function (ele) {
			var title = $C('block-name', ele, 'div');
			if(title.length) {
				Util.show(title[0]);
			}
		},
		hideBlockName : function (ele) {
			var title = $C('block-name', ele, 'div');
			if(title.length) {
				Util.hide(title[0]);
			}
		},
		//note 显示标头的编辑
		showEdit : function (e) {
			e = Util.event(e);
			var targetObject = Util.getTarget(e,'className',this.moveableObject);
			if (targetObject) {
				Util.show(targetObject.id + '_edit');
				targetObject.style.backgroundColor="#fffacd";
				this.showBlockName(targetObject);
			} else {
				var targetFrame = Util.getTarget(e,'className',this.frameClass);
				if (typeof targetFrame == 'object') {
					Util.show(targetFrame.id + '_edit');
					targetFrame.style.backgroundColor="#fffacd";
				}
			}
		},
		//note 隐藏标头的编辑
		hideEdit : function (e) {
			e = Util.event(e);
			var targetObject = Util.getTarget(e,'className',this.moveableObject);
			var targetFrame = Util.getTarget(e,'className',this.frameClass);
			if (typeof targetFrame == 'object') {
				Util.hide(targetFrame.id + '_edit');
				targetFrame.style.backgroundColor = '';
			}
			if (typeof targetObject == 'object') {
				Util.hide(targetObject.id + '_edit');
				targetObject.style.backgroundColor = '';
				this.hideBlockName(targetObject);
			}
		},
		//note 切换编辑菜单
		toggleMenu : function (e, obj) {
			e = Util.event(e);
			e.stopPropagation();
			//note 得到点击的元素的位置
			var objPara = {'top' : Util.getOffset( obj, false),'left' : Util.getOffset( obj, true),
							'width' : obj['offsetWidth'], 'height' : obj['offsetHeight']};

			//note 生成 菜单
			var dom = $('edit_menu');
			if (dom) {
				//note 相同的菜单已经显示则移除
				if (objPara.top + objPara.height == Util.getOffset(dom, false) && objPara.left == Util.getOffset(dom, true)) {
					dom.parentNode.removeChild(dom);
				} else {
					dom.style.top = objPara.top + objPara.height + 'px';
					dom.style.left = objPara.left + 'px';
					dom.innerHTML = this._getMenuHtml(e, obj);
				}
			} else {
				var html = this._getMenuHtml(e, obj);
				if (html != '') {
					dom = document.createElement('div');
					dom.id = 'edit_menu';
					dom.className = 'edit-menu';
					dom.style.top = objPara.top + objPara.height + 'px';
					dom.style.left = objPara.left + 'px';
					dom.innerHTML = html;
					document.body.appendChild(dom);
					var _method = this;
					document.body.onclick = function(e){Drag.prototype.removeMenu.call(_method, e);};
				}
			}
		},
		//note 生成菜单列表
		_getMenuHtml : function (e,obj) {
			var id = obj.id.replace('_edit_menu','');
			var html = '<ul>';
			if (typeof this.menu[id] == 'object') html += this._getMenuHtmlLi(id, this.menu[id]);
			if (Util.hasClass($(id),this.tabClass) && typeof this.menu['tab'] == 'object') html += this._getMenuHtmlLi(id, this.menu['tab']);
			if (Util.hasClass($(id),this.frameClass) && typeof this.menu['frame'] == 'object') html += this._getMenuHtmlLi(id, this.menu['frame']);
			if (Util.hasClass($(id),this.blockClass) && typeof this.menu['block'] == 'object') html += this._getMenuHtmlLi(id, this.menu['block']);
			if (typeof this.menu['default'] == 'object' && this.getObjByName(id)) html += this._getMenuHtmlLi(id, this.menu['default']); //note 框架内的模块显示default中的菜单
			html += '</ul>';
			return html == '<ul></ul>' ? '' : html;
		},
		_getMenuHtmlLi : function (id, cmds) {
			var li = '';
			var len = cmds.length;
			for (var i=0; i<len; i++) {
				li += '<li class="mitem" id="cmd_'+id+'" onclick='+"'"+cmds[i]['cmd']+"'"+'>'+cmds[i]['cmdName']+'</li>';
			}
			return li;
		},
		//note 隐藏菜单
		removeMenu : function (e) {
			var dom = $('edit_menu');
			if (dom) dom.parentNode.removeChild(dom);
			document.body.onclick = '';
		},
		//note 添加菜单
		addMenu : function (objId,cmdName,cmd) {
			if (typeof this.menu[objId] == 'undefined') this.menu[objId] = [];
			this.menu[objId].push({'cmdName':cmdName, 'cmd':cmd});
		},
		//note 添加默认菜单命令
		setDefalutMenu : function () {},
		//note 添加简洁模式下菜单命令
		setSampleMenu : function () {},

		//note  得到指定位置的模块ID
		getPositionKey : function (n) {
			this.initPosition();
			n = parseInt(n);
			var i = 0;
			for (var k in this.position) {
				if (i++ >= n) break;
			}
			return k;
		},

		//note  检查为拖动时insertBefore功能添加的DIV
		checkTempDiv : function (_id) {
			if(_id) {
				var id = _id+'_temp';
				var dom = $(id);
				if (dom == null || typeof dom == 'undefined') {
					dom  = document.createElement("div");
					dom.className = this.moveableObject+' temp';
					dom.id = id;
					$(_id).appendChild(dom);
				}
			}
		},
		//note 设置拖拽中的定位关系
		_setCssPosition : function (ele, value) {
			//note Tab框架中模块拖动时上级元素的定位
			while (ele && ele.parentNode && ele.id != 'ct') {
				if (Util.hasClass(ele,this.frameClass) || Util.hasClass(ele,this.tabClass)) ele.style.position = value;
				ele = ele.parentNode;
			}
		},
		//note 当前鼠标悬浮在的可移动的模块
		initDragObj : function (e) {
			e = Util.event(e);
			var target = Util.getTarget(e,'className',this.moveableObject);
			if (!target) {return false;}
			if (this.overObj != target && target.id !='tmpbox') {
				this.overObj = target;
				this.overObj.style.cursor = 'move';
			}
		},
		//note 开始拖动
		dragStart : function (e) {
			e = Util.event(e);
			if (e.aim['id'] && e.aim['id'].indexOf && e.aim['id'].indexOf('_edit') > 0) return false;
			if(e.which != 1 ) {return false;}//note  除了左键都不起作用
			this.overObj = this.dragObj = Util.getTarget(e,'className',this.moveableObject);
			if (!this.dragObj || Util.hasClass(this.dragObj,'temp')) {return false;}
			if (!this.getTmpBoxElement()) return false;
			//note 保存相邻元素
			this.getRelative();
			this._setCssPosition(this.dragObj.parentNode.parentNode, "static");
			var offLeft = Util.getOffset( this.dragObj, true );
			var offTop = Util.getOffset( this.dragObj, false );
			var offWidth = this.dragObj['offsetWidth'];
			this.dragObj.style.position = 'absolute';
			this.dragObj.style.left = offLeft + "px";
			this.dragObj.style.top = offTop - 3 + "px";
			this.dragObj.style.width = offWidth + 'px';
			this.dragObj.lastMouseX = e.clientX;
			this.dragObj.lastMouseY = e.clientY;
			Util.insertBefore(this.tmpBoxElement,this.overObj);
			Util.addClass(this.dragObj,this.moving);
			this.dragObj.style.zIndex = 500 ;
			this.scroll = Util.getScroll();
			var _method = this;
			document.onscroll = function(){Drag.prototype.resetObj.call(_method, e);};
			window.onscroll = function(){Drag.prototype.resetObj.call(_method, e);};
			document.onmousemove = function (e){Drag.prototype.drag.call(_method, e);};
		},
		getRelative : function () {
			this.dargRelative = {'up': this.dragObj.previousSibling, 'down': this.dragObj.nextSibling};
		},
		//note 窗口滚动条变动时，重置元素位置
		resetObj : function (e) {
			if (this.dragObj){
				e = Util.event(e);
				var p = Util.getScroll();
				var _t = p.t - this.scroll.t;
				var _l = p.l - this.scroll.l;
				var t = parseInt(this.dragObj.style.top);
				var l = parseInt(this.dragObj.style.left);
				t += _t;
				l += _l;
				this.dragObj.style.top =t+'px';
				this.dragObj.style.left =l+'px';
				this.scroll = Util.getScroll();
			}
		},
		//note 拖动
		drag : function (e) {
			e = Util.event(e);
			if(!this.isDragging) {
				//note  刚开始拖拽的时候将图层变透明，并标记为正在被拖拽
				this.dragObj.style.filter = "alpha(opacity=60)" ;
				this.dragObj.style.opacity = 0.6 ;
				this.isDragging = true ;
			}
			//note  鼠标坐标
			var _clientX = e.clientX;
			var _clientY = e.clientY;
			//note  如果鼠标没动就什么都不作
			if (this.dragObj.lastMouseX == _clientX && this.dragObj.lastMouseY == _clientY) return false ;
			//note 鼠标边缘操作
			//note 刚才Element的坐标
			var _lastY = parseInt(this.dragObj.style.top);
			var _lastX = parseInt(this.dragObj.style.left);
			_lastX = isNaN(_lastX) ? 0 :_lastX;
			_lastY = isNaN(_lastY) ? 0 :_lastY;
			//note  新的坐标
			var newX, newY;
			//note  计算新的坐标：原先的坐标+鼠标移动的值差
			newY = _lastY + _clientY - this.dragObj.lastMouseY;
			newX = _lastX + _clientX - this.dragObj.lastMouseX;

			//note  修改element的显示坐标
			this.dragObj.style.left = newX +"px ";
			this.dragObj.style.top = newY + "px ";
			//note  记录element现在的坐标供下一次移动使用
			this.dragObj.lastMouseX = _clientX;
			this.dragObj.lastMouseY = _clientY;
			var obj = this.getCurrentOverObj(e);
			if (obj && this.overObj != obj) {
				this.overObj = obj;
				this.getTmpBoxElement();
				Util.insertBefore(this.tmpBoxElement, this.overObj);
				this.dragObjFrame = this.dragObj.parentNode.parentNode;
				this.overObjFrame = this.overObj.parentNode.parentNode;
			}
			//note 取消选择
			Util.cancelSelect();
		},
		//note 把Tab title中的区块的内容部分添加到Tab框架的内容区域中
		_pushTabContent : function (tab, ele){
			//note 普通框架和Tab框架的多个列组装到一个容器中，然后移动到Tab的内容容器里
			if (Util.hasClass(ele, this.frameClass) || Util.hasClass(ele, this.tabClass)) {
				var dom = $(ele.id+'_content');
				//note 添加标示,初始化其中的farme/tab/block
				if (!dom) {
					dom = document.createElement('div');
					dom.id = ele.id+'_content';
					//note 保留框架原有的标识className
					dom.className = Util.hasClass(ele, this.frameClass) ? this.contentClass+' cl '+ele.className.substr(ele.className.lastIndexOf(' ')+1) : this.contentClass+' cl';
				}
				var frame = this.getObjByName(ele.id);
				if (frame) {
					for (var i in frame['columns']) {
						if (frame['columns'][i] instanceof Column) dom.appendChild($(i));
					}
				} else {
					//note 空frame/tab
					var children = ele.childNodes;
					var arrDom = [];
					for (var i in children) {
						if (typeof children[i] != 'object') continue;
						if (Util.hasClass(children[i],this.moveableColumn) || Util.hasClass(children[i],this.tabContentClass)) {
							arrDom.push(children[i]);
						}
					}
					var len = arrDom.length;
					for (var i = 0; i < len; i++) {
						dom.appendChild(arrDom[i]);
					}
				}
				$(tab.id+'_content').appendChild(dom);
			//note 处理模块：把模块的内容移动到Tab的内容容器里
			} else if (Util.hasClass(ele, this.blockClass)) {
				if ($(ele.id+'_content')) $(tab.id+'_content').appendChild($(ele.id+'_content'));
			}
		},
		//note Tab框架的内容区域移除内容
		_popTabContent : function (tab, ele){
			//note 普通框架和Tab框架的多个列组装到一个容器中，然后移动到Tab的内容容器里
			if (Util.hasClass(ele, this.frameClass) || Util.hasClass(ele, this.tabClass)) {
				Util.removeClass(ele, this.tabActivityClass);
				var eleContent = $(ele.id+'_content');
				if (!eleContent) return false;
				var children = eleContent.childNodes;
				var arrEle = [];
				for (var i in children) {
					if (typeof children[i] == 'object') arrEle.push(children[i]);
				}
				var len = arrEle.length;
				for (var i = 0; i < len; i++) {
					ele.appendChild(arrEle[i]);
				}
				children = '';
				$(tab.id+'_content').removeChild(eleContent);
			} else if (Util.hasClass(ele, this.blockClass)) {
				if ($(ele.id+'_content')) Util.show($(ele.id+'_content'));
				if ($(ele.id+'_content')) ele.appendChild($(ele.id+'_content'));
			}
		},
		/** Tab框架的显示处理，Tab的标题列中的第一个block/frame标签激活，其它为非激活;
		* Tab的content中对应标题列中第一个标签的content激活，其它隐藏
		* 处理Tab框架，把Tab title中区块的内容部分放到Tab content中
		* */
		_initTabActivity : function (ele) {
			if (!Util.hasClass(ele,this.tabClass)) return false;
			var tabs = $(ele.id+'_title').childNodes;
			var arrTab = [];
			for (var i in tabs) {
				if (typeof tabs[i] != 'object') continue;
				//note block或frame的ID
				var tabId = tabs[i].id;
				//note 标签为框架的,把框架的所有列组装成一个ID为 tabId+'_content'的元素，添加到tab框架的内容区域中
				if (Util.hasClass(tabs[i],this.frameClass) || Util.hasClass(tabs[i],this.tabClass)) {
					//note 正式版兼容问题，只替换一次
					if (!this._replaceFlag)	this._replaceFrameColumn(tabs[i]);
					if (!$(tabId + '_content')) {
						var arrColumn = [];
						for (var j in tabs[i].childNodes) {
							if (Util.hasClass(tabs[i].childNodes[j], this.moveableColumn)) arrColumn.push(tabs[i].childNodes[j]);
						}
						var frameContent = document.createElement('div');
						frameContent.id = tabId + '_content';
						//note 保留框架原有的标识className
						frameContent.className = Util.hasClass(tabs[i], this.frameClass) ? this.contentClass+' cl '+tabs[i].className.substr(tabs[i].className.lastIndexOf(' ')+1) : this.contentClass+' cl';
						var colLen = arrColumn.length;
						for (var k = 0; k < colLen; k++) {
							frameContent.appendChild(arrColumn[k]);
						}
					}
					arrTab.push(tabs[i]);
				//note 标签为模块的，直接把内容添加到tab框架的内容中
				} else if (Util.hasClass(tabs[i],this.blockClass)) {
					var frameContent = $(tabId+'_content');
					if (frameContent) {
						frameContent = Util.hasClass(frameContent.parentNode,this.blockClass) ? frameContent : '';
					} else {
						frameContent = document.createElement('div');
						frameContent.id = tabId+'_content';
					}
					arrTab.push(tabs[i]);
				}
				if (frameContent) $(ele.id + '_content').appendChild(frameContent);
			}
			var len = arrTab.length;
			for (var i = 0; i < len; i++) {
				Util[i > 0 ? 'hide' : 'show']($(arrTab[i].id+'_content'));
			}
		},
		//note 拖动结束
		dragEnd : function (e) {
			e = Util.event(e);
			if(!this.dragObj) {return false;}//note  除了左键都不起作用
			document.onscroll = function(){};
			window.onscroll = function(){};
			document.onmousemove = function(e){};
			document.onmouseup = '';
			if (this.tmpBoxElement.parentNode) {
				if (this.tmpBoxElement.parentNode == document.body) {
					document.body.removeChild(this.tmpBoxElement);
					document.body.removeChild(this.dragObj);
					this.fn = '';
				} else {
					Util.removeClass(this.dragObj,this.moving);
					this.dragObj.style.display = 'none';
					this.dragObj.style.width = '' ;
					this.dragObj.style.top = '';
					this.dragObj.style.left = '';
					this.dragObj.style.zIndex = '';
					this.dragObj.style.position = 'relative';
					this.dragObj.style.backgroundColor = '';
					this.isDragging = false ;
					this.tmpBoxElement.parentNode.replaceChild(this.dragObj, this.tmpBoxElement);
					Util.fadeIn(this.dragObj);
					this.tmpBoxElement='';
					this._setCssPosition(this.dragObjFrame, 'relative');
					//note 拖动结束后做相应的处理
					this.doEndDrag();
					this.initPosition();
					//note 如果不是原来的位置则设置退出警告
					if (!(this.dargRelative.up == this.dragObj.previousSibling && this.dargRelative.down == this.dragObj.nextSibling)) {
						this.setClose();
					}
					this.dragObjFrame = this.overObjFrame = null;
				}
			}
			this.newFlag = false;
			//note  执行回调函数
			if (typeof this.fn == 'function') {this.fn();}
		},
		//note 拖动结束后做相应的处理
		doEndDrag : function () {
			//note 处理Tab框架的拖动
			//note 拖入Tab框架中
			if (!Util.hasClass(this.dragObjFrame, this.tabClass) && Util.hasClass(this.overObjFrame, this.tabClass)) {
				this._pushTabContent(this.overObjFrame, this.dragObj);
			//note 从Tab框架中拖出标签
			}else if (Util.hasClass(this.dragObjFrame, this.tabClass) && !Util.hasClass(this.overObjFrame, this.tabClass)) {
				this._popTabContent(this.dragObjFrame, this.dragObj);
			//note 在Tab框架中拖动标签
			}else if (Util.hasClass(this.dragObjFrame, this.tabClass) && Util.hasClass(this.overObjFrame, this.tabClass)) {
				//note 在两个Tab框架之间拖动
				if (this.dragObjFrame != this.overObjFrame) {
					this._popTabContent(this.dragObjFrame, this.dragObj);
					this._pushTabContent(this.overObjFrame, this.dragObj);
				}
			//note 普通拖动：在框架和模块之间的拖动
			} else {
				//note no
			}

		},
		//note 处理frame列的样式,兼容正式版问题
		_replaceFrameColumn : function (ele,flag) {
			var children = ele.childNodes;
			var fcn = ele.className.match(/(frame-[\w-]*)/);
			if (!fcn) return false;
			var frameClassName = fcn[1];
			for (var i in children) {
				if (Util.hasClass(children[i], this.moveableColumn)) {
					var className = children[i].className;
					className = className.replace(' col-l', ' '+frameClassName+'-l');
					className = className.replace(' col-r', ' '+frameClassName+'-r');
					className = className.replace(' col-c', ' '+frameClassName+'-c');
					className = className.replace(' mn', ' '+frameClassName+'-l');
					className = className.replace(' sd', ' '+frameClassName+'-r');
					children[i].className = className;
				}
			}
		},
		//note 停止相关命令
		stopCmd : function () {
			this.rein.length > 0 ? this.rein.pop()() : '';
		},
		//note 设置close事件
		setClose : function () {
			if (!this.isChange) {
				window.onbeforeunload = function() {
					return '您的数据已经修改,退出将无法保存您的修改。';
				};
			}
			this.isChange = true;
		},
		//note 清除close事件
		clearClose : function () {
			this.isChange = false;
			window.onbeforeunload = function () {};
		},
		_getMoveableArea : function (ele) {
			ele = ele ? ele : document.body;
			this.moveableArea = $C(this.areaClass, ele, 'div');
		},
		//note 初始化可拖拽的区域
		initMoveableArea : function () {
			var _method = this;
			this._getMoveableArea();
			var len = this.moveableArea.length;
			for (var i = 0; i < len; i++) {
				var el = this.moveableArea[i]; //note 可拖动区域
				if (el == null || typeof el == 'undefined') return false;
				el.ondragstart = function (e) {return false;};
				el.onmouseover = function (e) {Drag.prototype.initDragObj.call(_method, e);};
				el.onmousedown = function (e) {Drag.prototype.dragStart.call(_method, e);};
				el.onmouseup = function (e) {Drag.prototype.dragEnd.call(_method, e);};
				el.onclick = function (e) {e = Util.event(e);e.preventDefault();};
			}
			//note 取消FF、chrome的默认拖动
			if ($('contentframe')) $('contentframe').ondragstart = function (e) {return false;};
		},
		//note 禁用指定ID的样式表
		disableAdvancedStyleSheet : function () {
			if(this.advancedStyleSheet) {
				this.advancedStyleSheet.disabled = true;
			}
		},
		//note 禁用指定ID的样式表
		enableAdvancedStyleSheet : function () {
			if(this.advancedStyleSheet) {
				this.advancedStyleSheet.disabled = false;
			}
		},
		//note 初始化
		init : function (sampleMode) {
			this.initCommon(); //note 公共部分
			this.setSampleMode(sampleMode);
			if(!this.sampleMode) {
				this.initAdvanced();
			} else {
				this.initSample();
			}
			return true;
		},
		//note 初始化高级模式
		initAdvanced : function () {
			this.initMoveableArea();
			this.initPosition();
			this.setDefalutMenu();
			this.enableAdvancedStyleSheet();
			this.showControlPanel();
			this.initTips();
			if(this.goonDIY) this.goonDIY();
			this.openfn();
		},

		//note 后继要执行的内容
		openfn : function () {
			var openfn = loadUserdata('openfn');
			if(openfn) {
				if(typeof openfn == 'function') {
					openfn();
				} else {
					eval(openfn);
				}
				saveUserdata('openfn', '');
			}
		},
		//note 初始化公共部分
		initCommon : function () {
			//note DIY模式下用的公共样式表
			this.advancedStyleSheet = $('diy_common');
			//note 菜单数据
			this.menu = [];
		},
		//note 初始化简洁模式
		initSample : function () {
			this._getMoveableArea();
			this.initPosition();
			this.sampleBlocks = $C(this.blockClass);
			this.initSampleBlocks();
			this.setSampleMenu();
			this.disableAdvancedStyleSheet();
			this.hideControlPanel();
		},
		//note 简洁模式下的模块
		initSampleBlocks : function () {
			if(this.sampleBlocks) {
				for(var i = 0; i < this.sampleBlocks.length; i++){
					this.checkEdit(this.sampleBlocks[i]);
				}
			}
		},
		setSampleMode : function (sampleMode) {
			if(loadUserdata('diy_advance_mode')) {
				this.sampleMode = '';
			} else {
				this.sampleMode = sampleMode;
				saveUserdata('diy_advance_mode', sampleMode ? '' : '1');
			}
			//note this.sampleMode = !loadUserdata('diy_advance_mode') && sampleMode;

		},

		//note 隐藏控制面板
		hideControlPanel : function() {
			Util.show('samplepanel');
			Util.hide('controlpanel');
			Util.hide('diy-tg');
		},

		//note 显示控制面板
		showControlPanel : function() {
			Util.hide('samplepanel');
			Util.show('controlpanel');
			Util.show('diy-tg');
		},
		checkHasFrame : function (obj) {
			obj = !obj ? this.data : obj;
			for (var i in obj) {
				if (obj[i] instanceof Frame && obj[i].className.indexOf('temp') < 0 ) {
					return true;
				} else if (typeof obj[i] == 'object') {
					if (this.checkHasFrame(obj[i])) return true;
				}
			}
			return false;
		},
		deleteFrame : function (name) {
			if (typeof name == 'string') {
				if (typeof window['c'+name+'_frame'] == 'object' && !BROWSER.ie) delete window['c'+name+'_frame'];
			} else {
				for(var i = 0,L = name.length;i < L;i++) {
					if (typeof window['c'+name[i]+'_frame'] == 'object' && !BROWSER.ie) delete window['c'+name[i]+'_frame'];
				}
 			}
		},
		//note 保存已经查看的Tips
		saveViewTip : function (tipname) {
			if(tipname) {
				saveUserdata(tipname, '1');
				Util.hide(tipname);
			}
			doane();
		},
		initTips : function () {
			var tips = ['diy_backup_tip'];
			for(var i = 0; i < tips.length; i++) {
				if(tips[i] && !loadUserdata(tips[i])) {
					Util.show(tips[i]);
				}
			}
		},
		//note  扩展
		extend : function (obj) {
			for (var i in obj) {
				this[i] = obj[i];
			}
		}
	};

	//note 自定义
	DIY = function() {
		this.frames = [];
		this.isChange = false; //note 是否改变数据
		this.spacecss = []; //note 空间css
		this.style = 't1'; //note 风格
		this.currentDiy = 'body'; //note 当前自定义的主题
		this.opSet = []; //note 操作记录集
		this.opPointer = 0; //note 当前操作指针
		this.backFlag = false; //note 撤消操作
		this.styleSheet = {} ; //note 自定的样式
		this.scrollHeight = 0 ; //note 正文高度
	};
	DIY.prototype = {

		//note 初始化
		init : function (mod) {
			drag.init(mod);
			this.style = document.diyform.style.value;
			if (this.style == '') {
				var reg = RegExp('topic\(.*)\/style\.css');
				var href = $('style_css') ? $('style_css').href : '';
				var arr = reg.exec(href);
				this.style = arr && arr.length > 1 ? arr[1] : '';
			}
			this.currentLayout = typeof document.diyform.currentlayout == 'undefined' ? '' : document.diyform.currentlayout.value;
			this.initStyleSheet();
			if (this.styleSheet.rules) this.initDiyStyle();
		},
		//note 得到diy_style样式表
		initStyleSheet : function () {
			var all = document.styleSheets;
			for (var i=0;i<all.length;i++) {
				var ownerNode = all[i].ownerNode || all[i].owningElement;
				if (ownerNode.id == 'diy_style') {
					this.styleSheet = new styleCss(i);
					return true;
				}
			}
		},
		//note 初始化用户自定义样式
		initDiyStyle : function (css) {
			var allCssText = css || $('diy_style').innerHTML;
			allCssText = allCssText ? allCssText.replace(/\n|\r|\t|  /g,'') : '';
			var random = Math.random(), rules = '';
			var reg = new RegExp('(.*?) ?\{(.*?)\}','g');
			while((rules = reg.exec(allCssText))) {
				var selector = this.checkSelector(rules[1]);
				var cssText = rules[2];
				var cssarr = cssText.split(';');
				var l = cssarr.length;
				for (var k = 0; k < l; k++) {
					var attribute = trim(cssarr[k].substr(0, cssarr[k].indexOf(':')).toLowerCase());
					var value = cssarr[k].substr(cssarr[k].indexOf(':')+1).toLowerCase();
					if (!attribute || !value) continue;
					if (!this.spacecss[selector]) this.spacecss[selector] = [];
					this.spacecss[selector][attribute] = value;
					//note 导入的样式
					if (css) this.setStyle(selector, attribute, value, random);
				}
			}
		},
		//note 检查选择器中的HTML标签
		checkSelector : function (selector) {
			var s  = selector.toLowerCase();
			if (s.toLowerCase().indexOf('body') > -1) {
				var body = BROWSER.ie ? 'BODY' : 'body';
				selector = selector.replace(/body/i,body);
			}
			if (s.indexOf(' a') > -1) {
				selector = BROWSER.ie ? selector.replace(/ [aA]/,' A') : selector.replace(/ [aA]/,' a');
			}
			return selector;
		},
		//note 初始化调色板
		initPalette : function (id) {
			var bgcolor = '',selector = '',bgimg = '',bgrepeat = '', bgposition = '', bgattachment = '', bgfontColor = '', bglinkColor = '', i = 0;
			var repeat = ['repeat','no-repeat','repeat-x','repeat-y'];
			var attachment = ['scroll','fixed'];
			var position = ['left top','center top','right top','left center','center center','right center','left bottom','center bottom','right bottom'];
			var position_ = ['0% 0%','50% 0%','100% 0%','0% 50%','50% 50%','100% 50%','0% 100%','50% 100%','100% 100%'];

			selector = this.getSelector(id);
			bgcolor = Util.formatColor(this.styleSheet.getRule(selector,'backgroundColor'));
			bgimg = this.styleSheet.getRule(selector,'backgroundImage');
			bgrepeat = this.styleSheet.getRule(selector,'backgroundRepeat');
			bgposition = this.styleSheet.getRule(selector,'backgroundPosition');
			bgattachment = this.styleSheet.getRule(selector,'backgroundAttachment');
			bgfontColor = Util.formatColor(this.styleSheet.getRule(selector,'color'));
			bglinkColor = Util.formatColor(this.styleSheet.getRule(this.getSelector(selector+' a'),'color'));

			var selectedIndex = 0;
			for (i=0;i<repeat.length;i++) {
				if (bgrepeat == repeat[i]) selectedIndex= i;
			}

			$('repeat_mode').selectedIndex = selectedIndex;
			for (i=0;i<attachment.length;i++) {
				$('rabga'+i).checked = (bgattachment == attachment[i] ? true : false);
			}
			var flag = '';
			for (i=0;i<position.length;i++) {
				var className = bgposition == position[i] ? 'red' : '';
				$('bgimgposition'+i).className = className;
				flag = flag ? flag : className;
			}
			//note chrome safari
			if (flag != 'red') {
				for (i=0;i<position_.length;i++) {
					className = bgposition == position_[i] ? 'red' : '';
					$('bgimgposition'+i).className = className;
				}
			}
			$('colorValue').value = bgcolor;
			if ($('cbpb')) $('cbpb').style.backgroundColor = bgcolor;
			$('textColorValue').value = bgfontColor;
			if ($('ctpb')) $('ctpb').style.backgroundColor = bgfontColor;
			$('linkColorValue').value = bglinkColor;
			if ($('clpb')) $('clpb').style.backgroundColor = bglinkColor;

			Util.show($('currentimgdiv'));
			Util.hide($('diyimages'));
			if ($('selectalbum')) $('selectalbum').disabled = 'disabled';
			//note 当前使用的图片
			bgimg = bgimg != '' && bgimg != 'none' ? bgimg.replace(/url\(['|"]{0,1}/,'').replace(/['|"]{0,1}\)/,'') : 'static/image/common/nophotosmall.gif';
			$('currentimg').src = bgimg;
		},
		//note 切换更换背景图片
		changeBgImgDiv : function () {
			Util.hide($('currentimgdiv'));
			Util.show($('diyimages'));
			if ($('selectalbum')) $('selectalbum').disabled = '';
		},
		getSpacecssStr : function() {
			var css = '';
			//note body 头部 内容区的样式
			var selectors = ['body', '#hd','#ct', 'BODY'];
			for (var i in this.spacecss) {
				var name = i.split(' ')[0];
				//note 删除已经不存在的dom样式
				if(selectors.indexOf(name) == -1 && !drag.getObjByName(name.substr(1))) {
					for(var k in this.spacecss) {
						//note 此样式相关的所有样式
						if (k.indexOf(i) > -1) {
							this.spacecss[k] = [];
						}
					}
					continue;
				}
				var rule = this.spacecss[i];
				if (typeof rule == "function") continue;
				var one = '';
				rule = this.formatCssRule(rule);
				for (var j in rule) {
					var content = this.spacecss[i][j];
					if (content && typeof content == "string" && content.length > 0) {
						content = this.trimCssImportant(content);
						content = content ? content + ' !important;' : ';';
						one += j + ":" + content;
					}
				}
				if (one == '') continue;
				css += i + " {" + one + "}";
			}
			return css;
		},
		//note 精简CSS代码
		formatCssRule : function (rule) {
			var arr = ['top', 'right', 'bottom', 'left'], i = 0;
			if (typeof rule['margin-top'] != 'undefined') {
				var margin = rule['margin-bottom'];
				if (margin && margin == rule['margin-top'] && margin == rule['margin-right'] && margin == rule['margin-left']) {
					rule['margin'] = margin;
					for(i=0;i<arr.length;i++) {
						delete rule['margin-'+arr[i]];
					}
				} else {
					delete rule['margin'];
				}
			}
			var border = '', borderb = '', borderr = '', borderl = '';
			if (typeof rule['border-top-color'] != 'undefined' || typeof rule['border-top-width'] != 'undefined' || typeof rule['border-top-style'] != 'undefined') {
				var format = function (css) {
					css = css.join(' ').replace(/!( ?)important/g,'').replace(/  /g,' ').replace(/^ | $/g,'');
					return css ? css + ' !important' : '';
				};
				border = format([rule['border-top-color'], rule['border-top-width'], rule['border-top-style']]);
				borderr = format([rule['border-right-color'], rule['border-right-width'], rule['border-right-style']]);
				borderb = format([rule['border-bottom-color'], rule['border-bottom-width'], rule['border-bottom-style']]);
				borderl = format([rule['border-left-color'], rule['border-left-width'], rule['border-left-style']]);
			} else if (typeof rule['border-top'] != 'undefined') {
				border = rule['border-top'];borderr = rule['border-right'];borderb = rule['border-bottom'];borderl = rule['border-left'];
			}
			if (border) {
				if (border == borderb && border == borderr && border == borderl) {
					rule['border'] = border;
					for(i=0;i<arr.length;i++) {
						delete rule['border-'+arr[i]];
					}
				} else {
					rule['border-top'] = border;rule['border-right'] = borderr;rule['border-bottom'] = borderb;rule['border-left'] = borderl;
					delete rule['border'];
				}
				for(i=0;i<arr.length;i++) {
					delete rule['border-'+arr[i]+'-color'];delete rule['border-'+arr[i]+'-width'];delete rule['border-'+arr[i]+'-style'];
				}
			}
			return rule;
		},
		//note 改变布局
		changeLayout : function (newLayout) {
			if (this.currentLayout == newLayout) return false;

			//note 宽度数据
			var data = $('layout'+newLayout).getAttribute('data');
			var dataArr = data.split(' ');

			//note 布局的列数
			var currentLayoutLength = this.currentLayout.length;
			var newLayoutLength = newLayout.length;

			//note 列数相同
			if (newLayoutLength == currentLayoutLength){
				$('frame1_left').style.width = dataArr[0]+'px';
				$('frame1_center').style.width = dataArr[1]+'px';
				if (typeof(dataArr[2]) != 'undefined') $('frame1_right').style.width = dataArr[2]+'px';
			//note 新列数>当前列数
			} else if (newLayoutLength > currentLayoutLength) {
				var block = this.getRandomBlockName();
				var dom = document.createElement('div');
				dom.id = 'frame1_right';
				dom.className = drag.moveableColumn + ' z';
				dom.appendChild($(block));
				$('frame1').appendChild(dom);
				$('frame1_left').style.width = dataArr[0]+'px';
				$('frame1_center').style.width = dataArr[1]+'px';
				dom.style.width = dataArr[2]+'px';
			//note 新列数<当前列数
			} else if (newLayoutLength < currentLayoutLength) {
				var _length = drag.data['diypage'][0]['columns']['frame1_right']['children'].length;
				var tobj = $('frame1_center_temp');
				for (var i = 0; i < _length; i++) {
					var name = drag.data['diypage'][0]['columns']['frame1_right']['children'][i].name;
					if (name.indexOf('temp') < 0) $('frame1_center').insertBefore($(name),tobj);
				}
				$('frame1').removeChild($('frame1_right'));
				$('frame1_left').style.width = dataArr[0]+'px';
				$('frame1_center').style.width = dataArr[1]+'px';
			}

			//note 交换
			var className = $('layout'+this.currentLayout).className;
			$('layout'+this.currentLayout).className = '';
			$('layout'+newLayout).className = className;
			this.currentLayout = newLayout;
			drag.initPosition();
			drag.setClose();
		},
		//note 随机获得模块名
		getRandomBlockName : function () {
			var left = drag.data['diypage'][0]['columns']['frame1_left']['children'];
			if (left.length > 2) {
				var block = left[0];
			} else {
				var block = drag.data['diypage'][0]['columns']['frame1_center']['children'][0];
			}
			return block.name;
		},
		//note 改变风格
		changeStyle : function (t) {
			if (t == '') return false;
			$('style_css').href=STATICURL+t+"/style.css";

			//note 添加操作记录
			if (!this.backFlag) {
				var oldData = [this.style];
				var newData = [t];
				var random = Math.random();
				this.addOpRecord ('this.changeStyle', newData, oldData, random);
			}
			var arr = t.split("/");
			this.style = arr[arr.length-1];
			drag.setClose();
		},
		//note 设置当前diy的主题
		setCurrentDiy : function (type) {
			if (type) {
				$('diy_tag_'+this.currentDiy).className = '';
				this.currentDiy = type;
				$('diy_tag_'+this.currentDiy).className = 'activity';
				this.initPalette(this.currentDiy);
			}
		},
		//note 使用透明背景
		hideBg : function () {
			var random = Math.random();
			//note 内置风格的背景图片不能取消掉
			this.setStyle(this.currentDiy, 'background-image', '', random);
			this.setStyle(this.currentDiy, 'background-color', '', random);
			if (this.currentDiy == 'hd') this.setStyle(this.currentDiy, 'height', '', random);
			this.initPalette(this.currentDiy);
		},
		//note 透明标题栏
		toggleHeader : function () {
			var random = Math.random();
			if ($('header_hide_cb').checked) {
				this.setStyle('#hd', 'height', '260px', random);
				this.setStyle('#hd', 'background-image', 'none', random);
				this.setStyle('#hd', 'border-width', '0px', random);
			} else {
				this.setStyle('#hd', 'height', '', random);
				this.setStyle('#hd', 'background-image', '', random);
				this.setStyle('#hd', 'border-width', '', random);
			}
		},
		//note 设置背景图片
		setBgImage : function (value) {
			var path = typeof value == "string" ? value : value.src;
			if (path.indexOf('.thumb') > 0) {
				path = path.substring(0,path.indexOf('.thumb'));
			}
			if (path == '' || path == 'undefined') return false;
			var random = Math.random();
			this.setStyle(this.currentDiy, 'background-image', Util.url(path), random);
			this.setStyle(this.currentDiy, 'background-repeat', 'repeat', random);
			if (this.currentDiy == 'hd') {
				var _method = this;
				var img = new Image();
				img.onload = function () {
					if (parseInt(img.height) < 140) {
						DIY.prototype.setStyle.call(_method, _method.currentDiy, 'background-repeat', 'repeat', random);
						DIY.prototype.setStyle.call(_method, _method.currentDiy, 'height', '', random);
					} else {
						DIY.prototype.setStyle.call(_method, _method.currentDiy, 'height', img.height+"px", random);
					}
				};
				img.src = path;
			}
		},
		//note 设置背景图片重复
		setBgRepeat : function (value) {
			var repeat = ['repeat','no-repeat','repeat-x','repeat-y'];
			if (typeof repeat[value] == 'undefined') {return false;}
			this.setStyle(this.currentDiy, 'background-repeat', repeat[value]);
		},
		//note 设置背景图片固定
		setBgAttachment : function (value) {
			var attachment = ['scroll','fixed'];
			if (typeof attachment[value] == 'undefined') {return false;}
			this.setStyle(this.currentDiy, 'background-attachment', attachment[value]);
		},
		//note 设置背景颜色
		setBgColor : function (color) {
			this.setStyle(this.currentDiy, 'background-color', color);
		},
		//note 设置文字颜色
		setTextColor : function (color) {
			this.setStyle(this.currentDiy, 'color', color);
		},
		//note 设置连接颜色
		setLinkColor : function (color) {
			this.setStyle(this.currentDiy + ' a', 'color', color);
		},
		//note 设置背景图片位置
		setBgPosition : function (id) {
			var td = $(id);
			var i = id.substr(id.length-1);
			var position = ['left top','center top','right top','left center','center center','right center','left bottom','center bottom','right bottom'];
			td.className = 'red';
			for (var j=0;j<9;j++) {
				if (i != j) {
					$('bgimgposition'+j).className = '';
				}
			}
			this.setStyle(this.currentDiy, 'background-position', position[i]);
		},
		//note 得到选择符
		getSelector : function (currentDiv) {
			//note alert(currentDiv);
			var arr = currentDiv.split(' ');
			currentDiv = arr[0];
			var link = '';
			if (arr.length > 1) {
				//note 如果最后一项是a
				link = (arr[arr.length-1].toLowerCase() == 'a') ? link = ' '+arr.pop() : '';
				currentDiv = arr.join(' ');
			}
			var selector = '';
			switch(currentDiv) {
				case 'blocktitle' :
					selector = '#ct .move-span .blocktitle';
					break;
				case 'body' :
				case 'BODY' :
					selector = BROWSER.ie ? 'BODY' : 'body';
					break;
				default :
					selector = currentDiv.indexOf("#")>-1 ? currentDiv : "#"+currentDiv;
			}
			var rega = BROWSER.ie ? ' A' : ' a';
			selector = (selector+link).replace(/ a/i,rega);
			return selector;
		},
		//note 设置样式
		setStyle : function (currentDiv, property, value, random, num){
			property = trim(property).toLowerCase();
			var propertyJs = property.property2js();
			if (typeof value == 'undefined') value = '';
			var selector = this.getSelector(currentDiv);

			//note 添加操作记录
			if (!this.backFlag) {
				var rule = this.styleSheet.getRule(selector,propertyJs);
				rule = rule.replace(/([\\"'])/g, "\\$1").replace(/\0/g, "\\0");
				var oldData = [currentDiv, property, rule];
				var newData = [currentDiv, property, value];
				if (typeof random == 'undefined') random = Math.random();
				this.addOpRecord ('this.setStyle', newData, oldData, random);
			}
			//note 改变样式表
			value = this.trimCssImportant(value);
			value = value ? value + ' !important' : '';
			var pvalue = value ? property+':'+value : '';
			this.styleSheet.addRule(selector,pvalue,num,property);
			Util.recolored();

			//note 保存数据
			if (typeof this.spacecss[selector] == 'undefined') {
				this.spacecss[selector] = [];
			}
			this.spacecss[selector][property] = value;
			drag.setClose();
		},
		//note 去除Css的important值
		trimCssImportant : function (value) {
			if (value instanceof Array) value = value.join(' ');
			return value ? value.replace(/!( ?)important/g,'').replace(/  /g,' ').replace(/^ | $/g,'') : '';
		},
		//note 删除样式
		removeCssSelector : function (selector) {
			for (var i in this.spacecss) {
				if (typeof this.spacecss[i] == "function") continue;
				if (i.indexOf(selector) > -1) {
					this.styleSheet.removeRule(i);
					this.spacecss[i] = [];
				}
			}
		},
		//note 撤消
		undo : function () {
			if (this.opSet.length == 0) return false;
			var oldData = '';
			if (this.opPointer <= 0) {return '';}
			var step = this.opSet[--this.opPointer];
			var random = step['random'];

			//note 多个命令以逗号分隔
			var cmd = step['cmd'].split(',');
			var cmdNum = cmd.length;

			//note 多条命令
			if (cmdNum >1) {
				for (var i=0; i<cmdNum; i++) {
					oldData = typeof step['oldData'][i] == 'undefined' || step['oldData'] == '' ? '' : '"' + step['oldData'][i].join('","') + '"';
					this.backFlag = true;
					eval(cmd[i]+'('+oldData+')');
					this.backFlag = false;
				}
			//note 单条命令
			} else {
				oldData = typeof step['oldData'] == 'undefined' || step['oldData'] == '' ? '' : '"' + step['oldData'].join('","') + '"';
				this.backFlag = true;
				eval(cmd+'('+oldData+')');
				this.backFlag = false;
			}
			$('button_redo').className = '';
			if (this.opPointer == 0) {
				$('button_undo').className = 'unusable';
				drag.isChange = false;
				drag.clearClose();
				return '';
			} else if (random == this.opSet[this.opPointer-1]['random']) {
				this.undo();
				return '';
			} else {
				return '';
			}
		},
		//note 重复操作
		redo : function () {
			if (this.opSet.length == 0) return false;
			var newData = '',random = '';
			if (this.opPointer >= this.opSet.length) return '';
			var step = this.opSet[this.opPointer++];
			random = step['random'];

			var cmd = step['cmd'].split(',');
			var cmdNum = cmd.length;

			//note 多条命令
			if (cmdNum >1) {
				for (var i=0; i<cmdNum; i++) {
					newData = typeof step['newData'][i] == 'undefined' || step['oldData'] == '' ? '' : '"' + step['newData'][i].join('","') + '"';
					this.backFlag = true;
					eval(cmd[i]+'('+newData+')');
					this.backFlag = false;
				}
			//note 单条命令
			}else {
				newData = typeof step['newData'] == 'undefined' || step['oldData'] == '' ? '' : '"' + step['newData'].join('","') + '"';
				this.backFlag = true;
				eval(cmd+'('+newData+')');
				this.backFlag = false;
			}
			$('button_undo').className = '';
			if (this.opPointer == this.opSet.length) {
				$('button_redo').className = 'unusable';
				return '';
			} else if(random == this.opSet[this.opPointer]['random']){
				this.redo();
			}
		},
		//note 添加操作记录
		addOpRecord : function (cmd, newData, oldData, random) {
			//note 初始化操作记录集
			if (this.opPointer == 0) this.opSet = [];
			//note 保存操作记录
			this.opSet[this.opPointer++] = {'cmd':cmd, 'newData':newData, 'oldData':oldData, 'random':random};

			$('button_undo').className = '';
			$('button_redo').className = 'unusable';
			Util.show('recover_button');
		},
		//note 恢复原装皮肤
		recoverStyle : function () {
			var random = Math.random();
			for (var selector in this.spacecss){
				var style = this.spacecss[selector];
				if (typeof style == "function") {continue;}
				for (var attribute in style) {
					if (typeof style[attribute] == "function") {continue;}
					this.setStyle(selector,attribute,'',random);
				}
			}
			Util.hide('recover_button');
			drag.setClose();
			this.initPalette(this.currentDiy);
		},
		//note 上传图片表单提交
		uploadSubmit : function (){
			if (document.uploadpic.attach.value.length<3) {
				alert('请选择您要上传的图片');
				return false;
			}
			if (document.uploadpic.albumid != null) document.uploadpic.albumid.value = $('selectalbum').value;
			return true;
		},
		//note 保存数据
		save : function () {
			return false;
		},
		//note 取消diy
		cancel : function () {
			var flag = false;
			if (this.isChange) {
				flag = confirm(this.cancelConfirm ? this.cancelConfirm : '退出将不会保存您刚才的设置。是否确认退出？');
			}
			if (!this.isChange || flag) {
				location.href = location.href.replace(/[\?|\&]diy\=yes/g,'');
			}
		},
		//note  扩展 DIY
		extend : function (obj) {
			for (var i in obj) {
				this[i] = obj[i];
			}
		}
	};
})();

<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: mod_index.php 30462 2012-05-30 03:27:10Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

/**
 * discuz remote service demo for default
 */
class mod_index extends remote_service {

	var $config;
	/**
	 * 构造函数, 本函数不是必须的
	 */
	function mod_index() {
		// 如果有构造函数, 请在最后,调用父类的构造函数
		parent::remote_service();
	}

	/**
	 * 默认调用的 method
	 */
	function run() {
		$this->success('Discuz! Remote Service API '.$this->version);
	}
}

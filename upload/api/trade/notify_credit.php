<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: notify_credit.php 34251 2013-11-25 03:10:11Z nemohou $
 */

define('IN_API', true);
define('CURSCRIPT', 'api');
define('DISABLEXSSCHECK', true);

require '../../source/class/class_core.php';
require '../../source/function/function_forum.php';

$discuz = C::app();
$discuz->init();

$apitype = empty($_GET['attach']) || !preg_match('/^[a-z0-9]+$/i', $_GET['attach']) ? 'alipay' : $_GET['attach'];
require_once DISCUZ_ROOT.'./api/trade/api_'.$apitype.'.php';
/** noteX
function stat_code_tenpay($bargainor_id, $price) {
	global $_G, $apitype;
	$stat_code = '';
	if(!defined('STAT_DISABLED') && !IS_ROBOT) {
		$statserver = 'http://stat.discuz.com/';
		if(!defined('CACHE_FILE') || $_G['uid']) {
			$url = $statserver.'stat.php?q='.rawurlencode(base64_encode(stat_query_tenpay($apitype, $bargainor_id, $price)));
			$stat_code .= !$imgcode ? '<script type="text/javascript" src="'.$url.'" reload="1"></script>' : '<img src="'.$url.'&amp;img=1" />';
			$statlogold = DISCUZ_ROOT.'./data/stat.log';
			if(file_exists($statlogold)) {
				$statlogfile = DISCUZ_ROOT.'./data/stat.log.'.random(3);
				@rename($statlogold, $statlogfile);
				if(($logs = @file($statlogfile)) !== FALSE && is_array($logs)) {
					foreach($logs as $log) {
						if($log) {
							$url = $statserver.'stat.php?q='.rawurlencode(base64_encode(trim($log)));
							$stat_code .= !$imgcode ? '<script type="text/javascript" src="'.$url.'" reload="1"></script>' : '<img src="'.$url.'&amp;img=1" />';
						}
					}
				}
				@unlink($statlogfile);
			}
		} else {
			$stat_code .= '<script type="text/javascript" src="'.$statserver.'stat.php" reload="1"></script>';
			$stat_code .= '<script text="text/javascript" reload="1">
			if(window.addEventListener) {
				window.addEventListener("load", function () {document.body.stat("", "'.$GLOBALS['BASESCRIPT'].'")}, false);
			} else if(document.attachEvent) {
				window.attachEvent("onload", function () {document.body.stat("", "'.$GLOBALS['BASESCRIPT'].'")});
			}
			</script>';
		}
	}
	return $stat_code;
}

function stat_query_tenpay($type, $bargainor_id, $price) {
	global $_G;
	$siteurl = parse_url($_G['siteurl']);
	$siteurl = $siteurl['host'];
	$query = array(
		'type' => $type,
		'bargainor_id' => $bargainor_id,
		'siteurl'	=> $siteurl,
		'timestamp'	=> $_G['timestamp'],
		'sid'		=> $_G['sid'],
		'uid'		=> $_G['uid'],
		'ip'		=> $_G['clientip'],
		'price'	=> $price,
	);
	$s = '';
	foreach($query as $k => $v) {
		trim($v) !== '' && $s .= '&'.$k.'='.rawurlencode($v);
	}
	return substr($s, 1);
}
*/
$PHP_SELF = $_SERVER['PHP_SELF'];
$_G['siteurl'] = dhtmlspecialchars('http://'.$_SERVER['HTTP_HOST'].preg_replace("/\/+(api\/trade)?\/*$/i", '', substr($PHP_SELF, 0, strrpos($PHP_SELF, '/'))).'/');
$notifydata = trade_notifycheck('credit');

if($notifydata['validator']) {

	$orderid = $notifydata['order_no'];
	$postprice = $notifydata['price'];
	//$order = DB::fetch_first("SELECT o.*, m.username FROM ".DB::table('forum_order')." o LEFT JOIN ".DB::table('common_member')." m USING (uid) WHERE o.orderid='$orderid'");
	$order = C::t('forum_order')->fetch($orderid);
	$order = array_merge($order, C::t('common_member')->fetch_by_username($order['uid']));
	if($order && floatval($postprice) == floatval($order['price']) && ($apitype == 'tenpay' || strtolower($_G['setting']['ec_account']) == strtolower($_REQUEST['seller_email']))) {

		if($order['status'] == 1) {
			//DB::query("UPDATE ".DB::table('forum_order')." SET status='2', buyer='$notifydata[trade_no]\t$apitype', confirmdate='$_G[timestamp]' WHERE orderid='$orderid'");
			C::t('forum_order')->update($orderid, array('status' => '2', 'buyer' => "$notifydata[trade_no]\t$apitype", 'confirmdate' => $_G['timestamp']));
			updatemembercount($order['uid'], array($_G['setting']['creditstrans'] => $order['amount']), 1, 'AFD', $order['uid']);
			updatecreditbyaction($action, $uid = 0, $extrasql = array(), $needle = '', $coef = 1, $update = 1, $fid = 0);
			//DB::query("DELETE FROM ".DB::table('forum_order')." WHERE submitdate<'$_G[timestamp]'-60*86400");
			C::t('forum_order')->delete_by_submitdate($_G['timestamp']-60*86400);
			$submitdate = dgmdate($order['submitdate']);
			$confirmdate = dgmdate(TIMESTAMP);

			notification_add($order['uid'], 'credit', 'addfunds', array(
				'orderid' => $order['orderid'],
				'price' => $order['price'],
				'value' => $_G['setting']['extcredits'][$_G['setting']['creditstrans']]['title'].' '.$order['amount'].' '.$_G['setting']['extcredits'][$_G['setting']['creditstrans']]['unit']
			), 1);
		}

	}

}

if($notifydata['location']) {
	$url = rawurlencode('home.php?mod=spacecp&ac=credit');
	//noteX $stat_code = stat_code_tenpay($notifydata['bargainor_id'], $notifydata['price']);
	if($apitype == 'tenpay') {
		echo <<<EOS
<meta name="TENCENT_ONLINE_PAYMENT" content="China TENCENT">
<html>
<body>
<script language="javascript" type="text/javascript">
window.location.href='$_G[siteurl]forum.php?mod=misc&action=paysucceed';
</script>
</body>
</html>
EOS;
	} else {
		dheader('location: '.$_G['siteurl'].'forum.php?mod=misc&action=paysucceed');
	}
} else {
	exit($notifydata['notify']);
}

?>
<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: cache_domainwhitelist.php 24152 2011-08-26 10:04:08Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

function build_cache_domainwhitelist() {
	/*
	$query = DB::query("SELECT svalue FROM ".DB::table('common_setting')." WHERE skey='domainwhitelist'");
	if($result = DB::result($query, 0)) {
		$data = explode("\r\n", $result);
	} else {
		$data = array();
	}*/

	$data = C::t('common_setting')->fetch('domainwhitelist');
	$data = $data ? explode("\r\n", $data) : array();
	savecache('domainwhitelist', $data);
}

?>
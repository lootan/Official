<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: cache_smileytypes.php 24968 2011-10-19 09:51:28Z zhengqingpeng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

function build_cache_smileytypes() {
	$data = array();
//	$query = DB::query("SELECT typeid, name, directory FROM ".DB::table('forum_imagetype')." WHERE type='smiley' AND available='1' ORDER BY displayorder");
//	while($type = DB::fetch($query)) {
	foreach(C::t('forum_imagetype')->fetch_all_by_type('smiley', 1) as $type) {
		$typeid = $type['typeid'];
		unset($type['typeid']);
		//if(DB::result_first("SELECT COUNT(*) FROM ".DB::table('common_smiley')." WHERE type='smiley' AND code<>'' AND typeid='$typeid'")) {
		if(C::t('common_smiley')->count_by_type_code_typeid('smiley', $typeid)) {
			$data[$typeid] = $type;
		}
	}

	savecache('smileytypes', $data);
}

?>
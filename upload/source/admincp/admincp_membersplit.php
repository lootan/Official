<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: admincp_membersplit.php 29851 2012-05-02 02:18:40Z zhangguosheng $
 */

if(!defined('IN_DISCUZ') || !defined('IN_ADMINCP')) {
	exit('Access Denied');
}

cpheader();
//if(empty($operation)) {
//	$operation = 'manage';
//}

//恢复备份数据
//discuz_database_safecheck::setconfigstatus(0);
//C::t('common_member')->truncate();
//C::t('common_member_count')->truncate();
//C::t('common_member_status')->truncate();
//C::t('common_member_profile')->truncate();
//C::t('common_member_field_forum')->truncate();
//C::t('common_member_field_home')->truncate();
//DB::query('insert into pre_common_member (select * from pre_common_member2)');
//DB::query('insert into pre_common_member_profile (select * from pre_common_member_profile2)');
//DB::query('insert into pre_common_member_field_forum (select * from pre_common_member_field_forum2)');
//DB::query('insert into pre_common_member_field_home (select * from pre_common_member_field_home2)');
//DB::query('insert into pre_common_member_count (select * from pre_common_member_count2)');
//DB::query('insert into pre_common_member_status (select * from pre_common_member_status2)');exit;
//

//恢复优化的数据
//discuz_database_safecheck::setconfigstatus(0);
//DB::query('insert into pre_common_member (select * from pre_common_member_archive)');
//DB::query('insert into pre_common_member_profile (select * from pre_common_member_profile_archive)');
//DB::query('insert into pre_common_member_field_forum (select * from pre_common_member_field_forum_archive)');
//DB::query('insert into pre_common_member_field_home (select * from pre_common_member_field_home_archive)');
//DB::query('insert into pre_common_member_count (select * from pre_common_member_count_archive)');
//DB::query('insert into pre_common_member_status (select * from pre_common_member_status_archive)');//exit;
//C::t('common_member_archive')->truncate();
//C::t('common_member_count_archive')->truncate();
//C::t('common_member_status_archive')->truncate();
//C::t('common_member_profile_archive')->truncate();
//C::t('common_member_field_forum_archive')->truncate();
//C::t('common_member_field_home_archive')->truncate();
//exit;
if(!$operation) {
	$operation = 'check';
}
loadcache(array('membersplitdata', 'userstats'));
//loadcache(array('membersplitstep', 'membersplitdata'));
//单进程进行中时再次打开窗口循环查检此值
if(!empty($_G['cache']['membersplitstep'])) {
	cpmsg('membersplit_split_in_backstage', 'action=membersplit&operation=check', 'loadingform');
}

if($operation == 'check') {
	shownav('founder', 'nav_membersplit');
	showsubmenu('membersplit');
	/*search={"nav_membersplit":"action=membersplit","nav_membersplit":"action=membersplit&operation=check"}*/
	showtips('membersplit_check_tips');
	/*search*/
	showformheader('membersplit&operation=manage');
	showtableheader('membersplit_table_orig');
	$membercount = $_G['cache']['userstats']['totalmembers'];

	showsubtitle(array('','','membersplit_count', 'membersplit_lasttime_check'));


	if($membercount < 20000) {
		$color = 'green';
		$msg = $lang['membersplit_without_optimization'];
	} else {
		$color = empty($_G['cache']['membersplitdata']) || $_G['cache']['membersplitdata']['dateline'] < TIMESTAMP - 86400*10 ?
			'red' : 'green';
		$msg = empty($_G['cache']['membersplitdata']) ? $lang['membersplit_has_no_check'] : dgmdate($_G['cache']['membersplitdata']['dateline']);
	}
	showtablerow('', '', array('','', number_format($membercount), '<span style="color:'.$color.'">'.$msg.'</span>'));

	if($membercount >= 20000) {
		showsubmit('membersplit_check_submit', 'membersplit_check');
	}
	showtablefooter();
	showformfooter();

} else if($operation == 'manage') {
	shownav('founder', 'nav_membersplit');
	if(!submitcheck('membersplit_split_submit', 1)) {
		showsubmenu('membersplit');
		/*search={"nav_membersplit":"action=membersplit","nav_membersplit":"action=membersplit&operation=check"}*/
		showtips('membersplit_tips');
		/*search*/
		showformheader('membersplit&operation=manage');
		showtableheader('membersplit_table_orig');

		//保存一个总用户数和可优化用户数，一天一处理
		if($_G['cache']['membersplitdata'] && $_G['cache']['membersplitdata']['dateline'] > TIMESTAMP - 86400) {
			$zombiecount = $_G['cache']['membersplitdata']['zombiecount'];
		} else {
			$zombiecount = C::t('common_member')->count_zombie();
			savecache('membersplitdata', array('zombiecount' => $zombiecount, 'dateline' => TIMESTAMP));
		}
		$membercount = $_G['cache']['userstats']['totalmembers'];
		$percentage = round($zombiecount/$membercount, 4)*100;

		showsubtitle(array('','','membersplit_count', 'membersplit_combie_count', 'membersplit_splitnum'));
		$color = $percentage > 0 ? 'red' : 'green';
		if($percentage == 0) {
			$msg = $lang['membersplit_message0'];
		} else if($percentage < 10) {
			$msg = $lang['membersplit_message10'];
		} else {
			$msg = $lang['membersplit_message100'];
		}
		showtablerow('', '',
				array('','', number_format($membercount), '<span style="color:'.$color.'">'.number_format($zombiecount).'('.$percentage.'%) '.$msg.'</span>', '<input name="splitnum" value="200" type="text" class="txt"/>'));

		if($percentage > 0) {
			showsubmit('membersplit_split_submit', 'membersplit_archive');
		}
		showtablefooter();
		showformfooter();

	} else {
//		if(!$_G['setting']['bbclosed']) {
//			cpmsg('membersplit_split_must_be_closed', 'action=membersplit&operation=manage', 'error');
//		}
		$step = intval($_GET['step'])+1;
		$splitnum = max(10, intval($_GET['splitnum']));
		if(!$_GET['nocheck'] && $step == 1 && !C::t('common_member_archive')->check_table()) {
			cpmsg('membersplit_split_check_table', 'action=membersplit&operation=rebuildtable&splitnum='.$splitnum, 'loadingform', array());
			cpmsg('', 'action=membersplit&operation=manage', 'error');
		}
		if(!C::t('common_member')->split($splitnum)) {
			//存档完毕，已经没有可以存档的用户
			cpmsg('membersplit_split_succeed', 'action=membersplit&operation=manage', 'succeed');
		}
		cpmsg('membersplit_split_doing', 'action=membersplit&operation=manage&membersplit_split_submit=1&step='.$step.'&splitnum='.$splitnum, 'loadingform', array('num' => $step*$splitnum));
	}
} else if($operation == 'rebuildtable') {
//	if(!$_G['setting']['bbclosed']) {
//		cpmsg('membersplit_split_must_be_closed', 'action=membersplit&operation=manage', 'error');
//	}
	$step = intval($_GET['step']);
	$splitnum = max(10, intval($_GET['splitnum']));
	$ret = C::t('common_member_archive')->rebuild_table($step);
	if($ret === false) {
		cpmsg('membersplit_split_check_table_done', 'action=membersplit&operation=manage&membersplit_split_submit=1&nocheck=1&splitnum='.$splitnum, 'loadingform');
	} else if($ret === true) {
		cpmsg('membersplit_split_checking_table', 'action=membersplit&operation=rebuildtable&splitnum='.$splitnum.'&step='.($step+1), 'loadingform', array('step' => $step+1));
	} else {
		cpmsg('membersplit_split_check_table_fail', 'action=membersplit&operation=manage&splitnum='.$splitnum, 'error', array('tablename' => $ret));
	}
}

?>

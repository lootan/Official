<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: Cloud.php 29177 2012-03-28 05:56:17Z yexinhao $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class Cloud_Service_Server_Cloud extends Cloud_Service_Server_Restful {

	/**
	 * $_instance
	 */
	protected static $_instance;

	/**
	 * getInstance
	 *
	 * @return self
	 */
	public static function getInstance() {

		if (!(self::$_instance instanceof self)) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * onCloudGetApps
	 *
	 * @access public
	 * @params $appName 云应用名称可选
	 * @return array 开启状态二维数组
	 *  + name connect 应用名称
	 *    + name 应用名称
	 *    + status 开启状态
	 *  + name search 应用名称
	 *    + name 应用名称
	 *    + status 开启状态
	 *  + apiVersion Discuz! 平台客户端版本号
	 * @return array
	 *  + status true | false 是否开启成功
	 *  + apiVersion
	 */
	public function onCloudGetApps($appName = '') {

		$appService = Cloud::loadClass('Service_App');
		$utilService = Cloud::loadClass('Service_Util');
		$apps = $appService->getCloudApps(false);

		if ($appName) {
			$apps = array($appName => $apps[$appName]);
		}

		$apps['apiVersion'] = $utilService->getApiVersion();

		$apps['siteInfo'] = $this->_getBaseInfo();

		return $apps;
	}

	/**
	 * onCloudSetApp
	 *
	 * @params $appName (manyou, payment, search, stats, connect, security) 服务英文名
	 * @params $status 服务状态(normal, close)
	 * @access public
	 * @return array
	 *  二维数组
	 *  + 服务英文名
	 *   + Discuz!返回本次操作是否成功 true false
	 *  + apiVersion
	 */
	public function onCloudSetApps($apps) {

		if (!is_array($apps)) {
			return false;
		}

		$appService = Cloud::loadClass('Service_App');
		$utilService = Cloud::loadClass('Service_Util');

		$res = array();
		$res['apiVersion'] = $utilService->getApiVersion();

		foreach ($apps as $appName => $status) {
			$res[$appName] = $appService->setCloudAppStatus($appName, $status, false, false);
		}

		// 批量操作时，在批量操作完成后统一更新缓存
		try {
			require_once libfile('function/cache');
			updatecache(array('plugin', 'setting', 'styles'));
			cleartemplatecache();
		} catch (Exception $e) {
			// 更新缓存失败不抛异常
		}

		$res['siteInfo'] = $this->_getBaseInfo();

		return $res;
	}

	/**
	 * onCloudOpenCloud
	 *
	 * @return array
	 *  + status boolean
	 */
	public function onCloudOpenCloud() {

		$appService = Cloud::loadClass('Service_App');
		$utilService = Cloud::loadClass('Service_Util');

		$this->_openCloud();

		$res = array();
		$res['status'] = true;
		$res['siteInfo'] = $this->_getBaseInfo();

		return $res;
	}

	protected function _openCloud() {

		require_once libfile('function/cache');

		$result = C::t('common_setting')->update('cloud_status', 1);

		// 开通云之后删除connect的id
		try {
			C::t('common_setting')->delete(array('connectsiteid', 'connectsitekey', 'my_siteid_old', 'my_sitekey_sign_old'));
		} catch (Exception $e) {
			// 删除历史数据失败不抛异常
		}

		updatecache('setting');
		return true;
	}

	protected function _getBaseInfo() {
		global $_G;
		$info = array();

		loadcache(array('userstats', 'historyposts'));
		$indexData = memory('get', 'forum_index_page_1');
		if(is_array($indexData) && $indexData) {
			$indexData = array();
			$info['threads'] = $indexData['threads'] ? $indexData['threads'] : 0;
			$info['todayPosts'] = $indexData['todayposts'] ? $indexData['todayposts'] : 0;
			$info['allPosts'] = $indexData['posts'] ? $indexData['posts'] : 0;
		} else {
			$threads = $posts = $todayposts = 0;
			$query = C::t('forum_forum')->fetch_all_by_status(1, 0);
			foreach($query as $forum) {
				if($forum['type'] != 'group') {
					$threads += $forum['threads'];
					$posts += $forum['posts'];
					$todayposts += $forum['todayposts'];
				}
			}
			$info['threads'] = $threads ? $threads : 0;
			$info['allPosts'] = $posts ? $posts : 0;
			$info['todayPosts'] = $todayposts ? $todayposts : 0;
		}

		$info['members'] = $_G['cache']['userstats']['totalmembers'] ? intval($_G['cache']['userstats']['totalmembers']) : 0;
		$postdata = $_G['cache']['historyposts'] ? explode("\t", $_G['cache']['historyposts']) : array(0,0);
		$info['yesterdayPosts'] = intval($postdata[0]);

		return $info;
	}

	public function onCloudGetStats() {
		global $_G;

		$info = array();

		$tableprelen = strlen(C::t('common_setting')->get_tablepre());
		$table = array(
			'forum_thread' => 'threads',
			'forum_post' => 'allPosts',
			'common_member' => 'members'
		);
		//$query = DB::query("SHOW TABLE STATUS");
		//while($row = DB::fetch($query)) {
		foreach(C::t('common_setting')->fetch_all_table_status() as $row) {
			$tablename = substr($row['Name'], $tableprelen);
			if(!isset($table[$tablename])) {
				continue;
			}
			$info[$table[$tablename]] = $row['Rows'];
		}

		loadcache(array('historyposts'));
		$postdata = $_G['cache']['historyposts'] ? explode("\t", $_G['cache']['historyposts']) : array(0,0);
		$info['yesterdayPosts'] = intval($postdata[0]);

		$postnum = 0;
		$postrow = 0;
		$avg_posts = C::t('common_stat')->fetch_post_avg();

		$info['avgPosts'] = intval($avg_posts);
		$info['statsCode'] = $_G['setting']['statcode'];

		return $info;
	}

}
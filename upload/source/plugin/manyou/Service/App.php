<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: App.php 34007 2013-09-18 06:43:17Z nemohou $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

Cloud::loadFile('Service_AppException');

class Cloud_Service_App {

	/**
	 * $_instance
	 */
	protected static $_instance;

	/**
	 * getInstance
	 *
	 * @return self
	 */
	public static function getInstance() {

		if (!(self::$_instance instanceof self)) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct() {

	}

	/**
	 *
	 * 检查云服务状态
	 * @return mixed 已开通云cloud，只开通漫游需要升级manyou，未开通false, 状态throw
	 */
	public function checkCloudStatus() {
		global $_G;

		$res = false;

		$cloudStatus = $_G['setting']['cloud_status'];
		$sId = $_G['setting']['my_siteid'];
		$sKey = $_G['setting']['my_sitekey'];

		// 已开通云
		if($sId && $sKey) {
			switch($cloudStatus) {
			case 1:
				$res = 'cloud';
				break;
			case 2:
				$res = 'unconfirmed';
				break;
			default:
				$res = 'upgrade';
			}
		// 未注册状态
		} elseif(!$cloudStatus && !$sId && !$sKey) {
			$res = 'register';
		} else {
			throw new Cloud_Service_AppException('Cloud status error!', 52101);
		}

		return $res;
	}

	/**
	 *
	 * 获取云应用列表
	 * @params boolean $cache 是否从缓存中获取信息
	 * @return array 列表信息二维数组
	 *  + name connect 应用名称
	 *    + name 应用名称
	 *    + status 开启状态
	 *  + name search 应用名称
	 *    + name 应用名称
	 *    + status 开启状态
	 */
	public function getCloudApps($cache = true) {

		$apps = array();

		if($cache) {
			global $_G;
			$apps = $_G['setting']['cloud_apps'];
		} else {
			$apps = C::t('common_setting')->fetch('cloud_apps', true);
		}

		if (!$apps) {
			$apps = array();
		}
		if (!is_array($apps)) {
			$apps = dunserialize($apps);
		}

		// 修正common_setting自动反序列为空时的问题
		unset($apps[0]);

		return $apps;
	}

	/**
	 *
	 * 获取单个云应用状态
	 * @params string $appName 云应用英文名称
	 * @params boolean $cache 是否从缓存中获取信息
	 * @return boolean true | false 程序会将normal状态返回true，其他状态均返回false
	 */
	public function getCloudAppStatus($appName, $cache = true) {

		$res = false;

		$apps = $this->getCloudApps($cache);
		if ($apps && $apps[$appName]) {
			$res = ($apps[$appName]['status'] == 'normal');
		}

		return $res;
	}

	/**
	 *
	 * 设置云应用状态
	 * @params string $appName 云应用英文名称
	 * @params integer $status 状态( normal | audit | close | disable | pause)
	 * @params boolean $cache 是否从缓存中获取信息
	 * @params boolean $updateCache 是否重建缓存
	 * @params boolean true | false
	 */
	public function setCloudAppStatus($appName, $status, $cache = true, $updateCache = true) {

		// 云应用设置状态时，本地客户端没有对应的函数，认为没有该应用客户端
		$method = '_' . strtolower($appName) . 'StatusCallback';
		if(!method_exists($this, $method)) {
			throw new Cloud_Service_AppException('Cloud callback: ' . $method . ' not exists!', 51001);
		}

		// 回调各应用自身的处理方法
		try {
			$callbackRes = $this->$method($appName, $status);
		} catch (Exception $e) {
			throw new Cloud_Service_AppException($e);
		}
		if (!$callbackRes) {
			throw new Cloud_Service_AppException('Cloud callback: ' . $method . ' error!', 51003);
		}

		// 更新云应用列表状态
		$apps = $this->getCloudApps($cache);

		$app = array('name' => $appName, 'status' => $status);
		$apps[$appName] = $app;

		C::t('common_setting')->update('cloud_apps', $apps);

		if ($updateCache) {
			// 更新缓存统一在云基础平台处理，不在各应用更新
			require_once libfile('function/cache');
			updatecache(array('plugin', 'setting', 'styles'));
			cleartemplatecache();
		}

		return true;
	}

	/**
	 *
	 * 兼容处理，设置各个应用的状态
	 */
	private function _manyouStatusCallback($appName, $status) {

		$available = 0;
		if($status == 'normal') {
			$available = 1;
		}
		return C::t('common_setting')->update('my_app_status', $available);
	}

	/**
	 *
	 * 兼容处理，设置各个应用的状态
	 */
	private function _connectStatusCallback($appName, $status) {

		$available = 0;
		if($status == 'normal') {
			$available = 1;
		}

		// 获取原有Connect设置数据
		$setting = C::t('common_setting')->fetch('connect', true);
		if(!$setting) {
			$setting = array();
		}
		$setting['allow'] = $available;

		C::t('common_setting')->update('connect', $setting);

		// 更新插件状态
		$this->setPluginAvailable('qqconnect', $available);

		return true;
	}

	/**
	 *
	 * 兼容处理，设置各个应用的状态
	 */
	private function _securityStatusCallback($appName, $status) {

		$available = 0;
		if($status == 'normal') {
			$available = 1;
		}

		$this->setPluginAvailable('security', $available);

		return true;
	}

	/**
	 *
	 * 兼容处理，设置各个应用的状态
	 */
	private function _storageStatusCallback($appName, $status) {

		$available = 0;
		if($status == 'normal') {
			$available = 1;
		}

		$this->setPluginAvailable('xf_storage', $available);

		return true;
	}

	/**
	 *
	 * 兼容处理，设置各个应用的状态
	 */
	private function _statsStatusCallback($appName, $status) {

		$available = 0;
		if($status == 'normal') {
			$available = 1;
		}

		$this->setPluginAvailable('cloudstat', $available);

		return true;
	}

	/**
	 *
	 * 兼容处理，设置各个应用的状态
	 */
	private function _searchStatusCallback($appName, $status) {

		$available = 0;
		if($status == 'normal') {
			$available = 1;
		}

		C::t('common_setting')->update('my_search_status', $available);

		if($available) {
			Cloud::loadFile('Service_SearchHelper');
			Cloud_Service_SearchHelper::allowSearchForum();
		}

		$this->setPluginAvailable('cloudsearch', $available);

		return true;
	}

	/**
	 *
	 * 兼容处理，设置各个应用的状态
	 */
	private function _smiliesStatusCallback($appName, $status) {

		$available = 0;
		if($status == 'normal') {
			$available = 1;
		}

		$this->setPluginAvailable('soso_smilies', $available);

		return true;
	}

	/**
	 *
	 * 兼容处理，设置各个应用的状态
	 */
	private function _qqgroupStatusCallback($appName, $status) {

		$available = 0;
		if($status == 'normal') {
			$available = 1;
		}

		$this->setPluginAvailable('qqgroup', $available);

		return true;
	}

	/**
	 *
	 * 兼容处理，设置各个应用的状态
	 */
	private function _unionStatusCallback($appName, $status) {
		return true;
	}

	private function _mobileOemStatusCallback($appName, $status) {
		return true;
	}

	/**
	 *
	 * 更新云系统插件可用状态
	 */
	function setPluginAvailable($identifier, $available) {

		$available = intval($available);

		$plugin = C::t('common_plugin')->fetch_by_identifier($identifier);

		if(!$plugin || !$plugin['pluginid']) {
			throw new Cloud_Service_AppException('Cloud plugin: ' . $identifier . ' not exists!', 51108);
		}

		C::t('common_plugin')->update($plugin['pluginid'], array('available' => $available));

		return true;
	}

}

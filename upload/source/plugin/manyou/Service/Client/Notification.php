<?php
/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: Notification.php 34003 2013-09-18 04:31:14Z nemohou $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

Cloud::loadFile('Service_Client_Restful');

class Cloud_Service_Client_Notification extends Cloud_Service_Client_Restful {

	/**
	 * $_instance
	 */
	protected static $_instance;

	/**
	 * getInstance
	 *
	 * @return self
	 */
	public static function getInstance($debug = false) {

		if (!(self::$_instance instanceof self)) {
			self::$_instance = new self($debug);
		}

		return self::$_instance;
	}

	/**
	 * __construct
	 *
	 * @return void
	 */
	public function __construct($debug = false) {

		return parent::__construct($debug);
	}
	/**
	 * 上报提醒消息
	 * @param Integer $siteUid: 提醒所有者uid
	 * @param Integer $pkId: 提醒主键Id
	 * @param String $type: 提醒类型
	 * @param Integer $authorId: 提醒来源者uid
	 * @param String $author: 提醒作者
	 * @param Integer|String $fromId: 提醒来源类型id
	 * @param String $fromIdType: 提醒来源类型
	 * @param String $note: 提醒内容
	 * @param Integer $fromNum: 重复次数
	 * @param Integer $dateline: 发表时间
	 * @param Array $extra: 扩展信息
	 */
	public function add($siteUid, $pkId, $type, $authorId, $author, $fromId, $fromIdType, $note, $fromNum, $dateline, $extra = array()) {

		$_params = array(
				'openid' => $this->getUserOpenId($siteUid),
				'sSiteUid' => $siteUid,
				'pkId' => $pkId,
				'type' => $type,
				'authorId' => $authorId,
				'author' => $author,
				'fromId' => $fromId,
				'fromIdType' => $fromIdType,
				'fromNum' => $fromNum,
				'content' => $note,
				'dateline' => $dateline,
				'deviceToken' => $this->getUserDeviceToken($siteUid),
				'extra' => array(
							'isAdminGroup' => getglobal('adminid'),
							'groupId' => getglobal('groupid'),
							'groupName' => getglobal('group/grouptitle')
						)
			);
		if($extra) {
			foreach($extra as $key => $value) {
				$_params['extra'][$key] = $value;
			}
		}
		return $this->_callMethod('connect.discuz.notification.add', $_params);
	}

	public function update($siteUid, $pkId, $fromNum, $dateline, $notekey) {
		$_params = array(
				'openid' => $this->getUserOpenId($siteUid),
				'sSiteUid' => $siteUid,
				'pkId' => $pkId,
				'fromNum' => $fromNum,
				'dateline' => $dateline,
				'notekey' => $notekey,
			);
		return $this->_callMethod('connect.discuz.notification.update', $_params);
	}

	/**
	 * 标识提醒已读
	 * @todo $pkIds可以不从，产品中默认一次性把所有提醒标识为已读
	 * @param Integer $siteUid: 提醒所有者uid
	 */
	public function setNoticeFlag($siteUid, $dateline) {

		$_params = array(
				'openid' => $this->getUserOpenId($siteUid),
				'sSiteUid' => $siteUid,
				'dateline' => $dateline
			);
		return $this->_callMethod('connect.discuz.notification.read', $_params);
	}

	public function addSiteMasterUserNotify($siteUids, $subject, $content, $authorId, $author, $fromType, $dateline) {
		$toUids = array();
		if($siteUids) {
			// 如果用户不存在 openid 的话，传一个假的 openid，兼容2.5早期版本每个用户都有 openid
			$users = C::t('#qqconnect#common_member')->fetch_all((array)$siteUids);
			$connectUsers = C::t('#qqconnect#common_member_connect')->fetch_all((array)$siteUids);
			$i = 1;
			foreach ($users as $uid => $user) {
				$conopenid = $connectUsers[$uid]['conopenid'];
				if (!$conopenid) {
					$conopenid = 'n' . $i ++;
				}
				$toUids[$conopenid] = $user['uid'];
			}

			$_params = array(
					'openidData' => $toUids,
					'subject' => $subject,
					'content' => $content,
					'authorId' => $authorId,
					'author' => $author,
					'fromType' => $fromType == 1 ? 1 : 2,
					'dateline' => $dateline,
					'deviceToken' => $this->getUserDeviceToken($siteUids)
			);
			return parent::_callMethod('connect.discuz.notification.addSiteMasterUserNotify', $_params);
		}
		return false;
	}

	protected function _callMethod($method, $args) {
		try {
			return parent::_callMethod($method, $args);
		} catch (Exception $e) {

		}
	}
}

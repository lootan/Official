<?php

/**
 *		[Discuz!] (C)2001-2099 Comsenz Inc.
 *		This is NOT a freeware, use is subject to license terms
 *
 *		$Id: Disk.php 30715 2012-06-14 03:02:00Z songlixin $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class Cloud_Service_Disk {

	/*
	 * @brief Util 类
	 */
	protected $_util;

	/*
	 * @brief Restful Client
	 */
	protected $_client;

	/*
	 * @brief 下载链接的基础链接
	 */
	protected $_baseUrl;

	/**
	 * $_instance
	 */
	protected static $_instance;

	/**
	 * getInstance
	 *
	 * @return self
	 */
	public static function getInstance() {
		global $_G;

		if (!(self::$_instance instanceof self)) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	public function __construct() {
		global $_G;

		$this->_util = Cloud::loadClass('Service_Util');
		$this->_client = Cloud::loadClass('Service_Client_Disk');
		// 下载链接基本URL
		$this->_baseUrl = $_G['siteurl'] . 'apptest.php?';
		// $this->_baseUrl = $_G['siteurl'] . 'plugin.php?id=qqconnect:disk&';
	}

	public function saveTask($attach, $openId) {
		global $_G;

		if (!$_G['uid']) {
			throw new Cloud_Exception('noLogin', '30001');
		}
		if (!$openId) {
			throw new Cloud_Exception('noopenId', '30002');
		}
		$verifyCode = md5($openId . $attach['aid'] . $_G['timestamp'] . $_G['uid']);
		$taskData = array(
			'aid' => $attach['aid'],
			'uid' => $_G['uid'],
			'openId' => $openId,
			'filename' => $attach['filename'],
			'verifycode' => $verifyCode,
			'status' => 0,
			'dateline' => $_G['timestamp'],
		);
		// 先保存任务
		$taskId = C::t('#qqconnect#connect_disktask')->insert($taskData, true);

		// 下载链接
		$downParams = array(
			'taskid' => $taskId,
			'verifycode' => $verifyCode,
		);
		$downloadUrl = $this->_baseUrl . $this->_util->generateSiteSignUrl($downParams);
		// 小于5M 的文件,如果文件超过10M，单项计算时间超过 0.1 秒，下面的计算方式只保留一项
		$filePath = self::_getFullPath($attach['attachment']);
		if ($attach['filesize'] <= 10000000 && file_exists($filePath)) {
			$md5 = md5_file($filePath);
			$hash = hash_file('sha1', $filePath);
		}
		// 向云平台发送的数据,支持批量
		$cloudData = array(
			'sId' => $_G['setting']['my_siteid'],
			'openId' => $openId,
			'batchTasks' => array(
				array(
					'aId' => $attach['aid'],
					'fileName' => $attach['filename'],
					'downloadUrl' => $downloadUrl,
					'size' => filesize($filePath),
					'md5' => $md5,
					'hash' => $hash,
				),
			),
			'clientIp' => $_G['clientip'],
		);
		// debug($cloudData);
		// 向云平台发送请求
		return $this->_client->sendTask($cloudData);
	}

	public function getAttachment() {
		global $_G;

		try {
			$taskData = $this->checkTask();
		} catch (Exception $e) {
			throw new Cloud_Exception($e);
		}
		$task = $taskData['task'];
		$attach = $taskData['attach'];
		$taskId = $task['taskid'];

		// 设置已经获取
		C::t('#qqconnect#connect_disktask')->update($taskId, array(
			'status' => 1,
			'downloadtime' => $_G['timestamp'],
		));
		// 直接关闭数据库连接
		$db = DB::object();
		$db->close();
		ob_end_clean();
		// 输出文件，先检查是否开启了 xsendfile 如果开启了就使用
		// 没开启的话根据是否远程附件输出文件二进制数据
		// 二进制文件头，文件大小
		$attach['filename'] = '"'.(strtolower(CHARSET) == 'utf-8' && strexists($_SERVER['HTTP_USER_AGENT'], 'MSIE') ? urlencode($attach['filename']) : $attach['filename']).'"';
		dheader('Content-Type: application/octet-stream');
		dheader('Content-Length: ' . $attach['filesize']);
		dheader('Content-Disposition: attachment; filename='.$attach['filename']);

		self::_checkXSendFile($attach['attachment']);
		if ($attach['remote']) {
			self::_getRemoteFile($attach['attachment']);
		} else {
			self::_getLocalFile($attach['attachment']);
		}
		exit;
	}

	/*
	 * @brief checkTask 检测task 是否允许下载
	 *
	 * @returns
	 */
	public function checkTask() {
		global $_G;
		// 验证sid
		$sId = $_G['setting']['my_siteid'];
		$ts = $_GET['ts'];
		$taskId = $_GET['taskid'];
		$verifyCode = $_GET['verifycode'];
		if ($sId != $_GET['s_id']) {
			throw new Cloud_Exception('sIdError', '30004');
		}
		// 过期时间验证
		if ($_G['timestamp'] - $ts > 86400) {
			throw new Cloud_Exception('downloadTimeOut', '30005');
		}
		// url 未被伪造验证
		$params = array(
			'taskid' => $taskId,
			'verifycode' => $verifyCode,
			's_id' => $_GET['s_id'],
			's_site_uid' => $_GET['s_site_uid'],
		);
		$sig = $_GET['sig'];
		$sKey = $_G['setting']['my_sitekey'];
		ksort($params);
		$str = $this->_util->httpBuildQuery($params, '', '&');
		if ($sig != md5(sprintf('%s|%s|%s', $str, $sKey, $ts))) {
			throw new Cloud_Exception('sigError', '30003');
		}

		// task 验证
		$task = C::t('#qqconnect#connect_disktask')->fetch($taskId);
		if (!$task) {
			throw new Cloud_Exception('noTask', '30006');
		}
		// 下载验证码
		if ($verifyCode != $task['verifycode']) {
			throw new Cloud_Exception('verifyError', '30009');
		}
		// 这个任务的附件已经被下载过
		// if ($task['status'] == 1) {
			// throw new Cloud_Exception('attachmentHadDownloaded', '30008');
		// }

		$attach = C::t('forum_attachment_n')->fetch('aid:' . $task['aid'], $task['aid']);
		if (!$attach) {
			throw new Cloud_Exception('noAttachment', '30007');
		}
		// 将task 和 attach 放到数组中输出
		$return = array(
			'task' => $task,
			'attach' => $attach,
		);

		return $return;
	}

	/*
	 * @brief clearDirtyData 清理已经下载过的数据
	 *
	 * @returns
	 */
	public function clearDirtyData() {
		return C::t('#qqconnect#connect_disktask')->delete_by_status(1);
	}

	/*
	 * @brief _getFullPath 获取文件全路径
	 *
	 * @param $attachment
	 *
	 * @returns
	 */
	private static function _getFullPath($attachment) {
		global $_G;

		return $_G['setting']['attachdir'] . 'forum/' . $attachment;
	}

	/*
	 * @brief _getLocalFile 从 Discuz! 移植读取本地文件方法
	 *
	 * @param $file
	 *
	 * @returns
	 */
	private static function _getLocalFile($file) {
		$filename = self::_getFullPath($file);
		$readmod = getglobal('config/download/readmod');
		$readmod = $readmod > 0 && $readmod < 5 ? $readmod : 2;
		if($readmod == 1 || $readmod == 3 || $readmod == 4) {
			if($fp = @fopen($filename, 'rb')) {
				if(function_exists('fpassthru') && ($readmod == 3 || $readmod == 4)) {
					@fpassthru($fp);
				} else {
					echo @fread($fp, filesize($filename));
				}
			}
			@fclose($fp);
		} else {
			@readfile($filename);
		}
		@flush(); @ob_flush();
	}

	/*
	 * @brief _getLocalFile 从 Discuz! 移植读取远程文件方法
	 *
	 * @param $file
	 *
	 * @returns
	 */
	private static function _getRemoteFile($file) {
		global $_G;

		@set_time_limit(0);
		if(!@readfile($_G['setting']['ftp']['attachurl'] . 'forum/' . $file)) {
			$ftp = ftpcmd('object');
			$tmpfile = @tempnam($_G['setting']['attachdir'], '');
			if($ftp->ftp_get($tmpfile, 'forum/'.$file, FTP_BINARY)) {
				@readfile($tmpfile);
				@unlink($tmpfile);
			} else {
				@unlink($tmpfile);
				return FALSE;
			}
		}

		return TRUE;
	}

	/*
	 * @brief _checkXSendFile 移植论坛检查是否启用了 xsendfile
	 *
	 * @param $file
	 *
	 * @returns
	 */
	private static function _checkXSendFile($file) {
		$filename = self::_getFullPath($file);
		$xsendfile = getglobal('config/download/xsendfile');
		if(!empty($xsendfile)) {
			$type = intval($xsendfile['type']);
			$cmd = '';
			switch ($type) {
				case 1:
					$cmd = 'X-Accel-Redirect';
					$url = $xsendfile['dir'] . $file;
					break;
				case 2:
					$cmd = $_SERVER['SERVER_SOFTWARE'] <'lighttpd/1.5' ? 'X-LIGHTTPD-send-file' : 'X-Sendfile';
					$url = $filename;
					break;
				case 3:
					$cmd = 'X-Sendfile';
					$url = $filename;
					break;
			}
			if($cmd) {
				dheader("$cmd: $url");
				exit();
			}
		}
	}
}

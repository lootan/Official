<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: discuz_base.php 30321 2012-05-22 09:09:35Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

abstract class discuz_base
{
	private $_e;
	private $_m;

	public function __construct() {

	}

	/**
	 * PHP magic method for write Property
	 * Do not call this method. This is a PHP magic method that we override
	 * @param string $name Property name
	 * @param mixed $value Property value
	 * @return true
	 */
	public function __set($name, $value) {
		$setter='set'.$name;
		if(method_exists($this,$setter)) {
			return $this->$setter($value);
		} elseif($this->canGetProperty($name)) {
			throw new Exception('The property "'.get_class($this).'->'.$name.'" is readonly');
		} else {
			throw new Exception('The property "'.get_class($this).'->'.$name.'" is not defined');
		}
	}

	/**
	 * PHP magic method for read Property
	 * Do not call this method. This is a PHP magic method that we override
	 * @param string $name Property name
	 * @return mixed
	 */
	public function __get($name) {
		$getter='get'.$name;
		if(method_exists($this,$getter)) {
			return $this->$getter();
		} else {
			throw new Exception('The property "'.get_class($this).'->'.$name.'" is not defined');
		}
	}

	/**
	 * Calls the named method which is not a class method.
	 * Do not call this method. This is a PHP magic method that we override
	 * to implement the behavior feature.
	 * @param string $name the method name
	 * @param array $parameters method parameters
	 */
	public function __call($name,$parameters) {
		throw new Exception('Class "'.get_class($this).'" does not have a method named "'.$name.'".');
	}

	/**
	 * Does a property has get method ?
	 * @param string $name property name
	 * @return boolean
	 */
	public function canGetProperty($name)
	{
		return method_exists($this,'get'.$name);
	}

	/**
	 * Does a property has set method ?
	 * @param string $name property name
	 * @return boolean
	 */
	public function canSetProperty($name)
	{
		return method_exists($this,'set'.$name);
	}

	/**
	 * PHP magic method tostring
	 * @return string
	 */
	public function __toString() {
		return get_class($this);
	}

	public function __invoke() {
		return get_class($this);
	}

}
?>
<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: table_common_template_block.php 29445 2012-04-12 07:14:40Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_common_template_block extends discuz_table
{
	public function __construct() {

		$this->_table = 'common_template_block';
		$this->_pk    = '';

		parent::__construct();
	}

	/**
	 * 删除指定页面的所有数据
	 * @param string $tpl 目标页面标识
	 * @param string $tpldirectory 原模板所在目录
	 * @return bool
	 */
	public function delete_by_targettplname($tpl, $tpldirectory = NULL) {
		$add = $tpldirectory !== NULL ? ' AND '.DB::field('tpldirectory', $tpldirectory) : '';
		return $tpl ? DB::delete($this->_table, DB::field('targettplname', $tpl).$add) : false;
	}

	/**
	 * 获取指定模块的所在页面的名称
	 * @param int $bid 模块id
	 * @return string
	 */
	public function fetch_targettplname_by_bid($bid) {
		return ($bid = dintval($bid)) ? DB::result_first('SELECT targettplname FROM %t WHERE bid=%d', array($this->_table, $bid)) : '';
	}

	/**
	 * 获取指定页面的所有模块ID
	 * @param string $tpl 目标页面标识
	 * @param int $notinherited 是否不继承页面权限
	 * @return array
	 */
	public function fetch_all_bid_by_targettplname_notinherited($tpl, $notinherited) {
		$bids = array();
		if($tpl) {
			$query = DB::query('SELECT tb.bid FROM %t tb LEFT JOIN %t b ON b.bid=tb.bid WHERE '.DB::field('targettplname', $tpl).' AND b.notinherited=%d', array($this->_table, 'common_block', $notinherited));
			while($value = DB::fetch($query)) {
				$bids[$value['bid']] = $value['bid'];
			}
		}
		return $bids;
	}

	/**
	 * 根据模块ID获取指定单条数据
	 * @param int $bid 模块ID
	 * @return array
	 */
	public function fetch_by_bid($bid) {
		return ($bid = dintval($bid)) ? DB::fetch_first('SELECT * FROM '.DB::table($this->_table).' WHERE '.DB::field('bid', $bid)) : array();
	}

	/**
	 * 获取多个模块的数据
	 * @param array $bids 模块ID
	 * @return array
	 */
	public function fetch_all_by_bid($bids) {
		return ($bids = dintval($bids, true)) ? DB::fetch_all('SELECT * FROM '.DB::table($this->_table).' WHERE '.DB::field('bid', $bids), null, 'bid') : array();
	}

	/**
	 * 获取指定页面的所有模块数据
	 * @param string $targettplname 目标页面标识
	 * @param string $tpldirectory 原模板所在目录
	 * @return array
	 */
	public function fetch_all_by_targettplname($targettplname, $tpldirectory = NULL) {
		$add = ($tpldirectory !== NULL) ? ' AND '.DB::field('tpldirectory', $tpldirectory) : '';
		return DB::fetch_all('SELECT * FROM %t WHERE targettplname=%s'.$add, array($this->_table, $targettplname), 'bid');
	}

	/**
	 * 批量插入数据
	 * @param string $targettplname 目标页面标识
	 * @param string $tpldirectory 原模板所在目录
	 * @param array $bids 模块ID
	 */
	public function insert_batch($targettplname, $tpldirectory, $bids) {
		if($targettplname && ($bids = dintval($bids, true))) {
			$values = array();
			foreach ($bids as $bid) {
				if($bid) {
					$values[] = "('$targettplname','$tpldirectory', '$bid')";
				}
			}
			DB::query("INSERT INTO ".DB::table($this->_table)." (targettplname, tpldirectory, bid) VALUES ".implode(',', $values));
		}
	}
}

?>
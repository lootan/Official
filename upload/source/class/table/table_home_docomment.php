<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: table_home_docomment.php 27819 2012-02-15 05:12:23Z svn_project_zhangjie $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class table_home_docomment extends discuz_table 
{
	public function __construct() {

		$this->_table = 'home_docomment';
		$this->_pk    = 'id';

		parent::__construct();
	}

	/**
	 * 根据doid及uid删除用户记录回复
	 * @param int|array $doids
	 * @param int|array $uids
	 * @return bool 
	 */
	public function delete_by_doid_uid($doids = null, $uids = null) {
		$sql = array();
		$doids && $sql[] = DB::field('doid', $doids);
		$uids && $sql[] = DB::field('uid', $uids);
		if($sql) {
			return DB::query('DELETE FROM %t WHERE %i', array($this->_table, implode(' OR ', $sql)));
		} else {
			return false;
		}
	}

	/**
	 * 通过doid获取用户记录的回复列表
	 * @param int|array $doids
	 * @return array 
	 */
	public function fetch_all_by_doid($doids) {
		if(empty($doids)) {
			return array();
		}
		return DB::fetch_all('SELECT * FROM %t WHERE '.DB::field('doid', $doids).' ORDER BY dateline', array($this->_table));
	}

}

?>
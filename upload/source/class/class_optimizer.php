<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: class_optimizer.php 30871 2012-06-27 09:32:37Z zhangjie $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class optimizer {

	private $optimizer = array();

	public function __construct($type) {
		$this->optimizer = new $type();
	}

//	public function register($type) {
//		$this->type[] = $type;
//	}

	public function check() {
//		$return = array();
//		foreach($this->type as $type) {
//			$$type = new $type();
//			$return[] = $$type->check();
//		}
//		return $return;
		return $this->optimizer->check();
	}

	public function optimizer() {
//		$return = array();
//		foreach($this->type as $type) {
//			$$type = new $type();
//			$return[] = $$type->optimizer();
//		}
//		return $return;
		return $this->optimizer->optimizer();
	}

	public function option_optimizer($options) {
//		$return = array();
//		foreach($this->type as $type) {
//			$$type = new $type();
//			$return[] = $$type->option_optimizer($options);
//		}
//		return $return;
		return $this->optimizer->option_optimizer($options);
	}

	public function get_option() {
//		$return = array();
//		foreach($this->type as $type) {
//			$$type = new $type();
//			$return[] = $$type->get_option($options);
//		}
//		return $return;
		return $this->optimizer->get_option();
	}
}
?>
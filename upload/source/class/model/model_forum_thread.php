<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: model_forum_thread.php 33881 2013-08-27 03:10:46Z nemohou $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

class model_forum_thread extends discuz_model
{

	/**
	 * 当前版块
	 * @var array
	 */
	public $forum;

	public $thread;

	public $post;

	/**
	 * 主题ID
	 * @var int
	 */
	public $tid;

	/**
	 * 帖子ID
	 * @var int
	 */
	public $pid;

	/**
	 * feed数据
	 * @var array
	 */
	public $feed = array();

	public function __construct($fid = null) {
		parent::__construct();
		if($fid) {
			include_once libfile('function/forum');
			loadforum($fid);
		}
		$this->forum = &$this->app->var['forum'];
	}

	/**
	 * 发新主题
	 * @param array $parameters 主题相关数据
	 * <code>
	 *	$parameters数组的key可保含以下内容
			//公共数据
			'member', 'group', 'forum', 'extramessage',
			//threa数据
			'subject', 'sticktopic', 'save', 'ordertype', 'hiddenreplies',
			'allownoticeauthor', 'readperm', 'price', 'typeid', 'sortid',
			'publishdate', 'digest', 'moderated', 'tstatus', 'isgroup',
			'replycredit', 'closed', 'special', 'tags',
			//post数据
			'message','clientip', 'invisible', 'isanonymous', 'usesig',
			'htmlon', 'bbcodeoff', 'smileyoff', 'parseurloff', 'pstatus'
	 * </code>
	 * @return string
	 */
	public function newthread($parameters) {

		require_once libfile('function/post');
		$this->tid = $this->pid = 0;
		//处理数据
		$this->_init_parameters($parameters);

		//note 主题或者信息为空
		if(trim($this->param['subject']) == '') {
			return $this->showmessage('post_sm_isnull');
		}

		if(!$this->param['sortid'] && !$this->param['special'] && trim($this->param['message']) == '') {
			return $this->showmessage('post_sm_isnull');
		}
		//modnewthreads, modnewreplies
		list($this->param['modnewthreads'], $this->param['modnewreplies']) = threadmodstatus($this->param['subject']."\t".$this->param['message'].$this->param['extramessage']);

		//note 主题属性检查
		if(($post_invalid = checkpost($this->param['subject'], $this->param['message'], ($this->param['special'] || $this->param['sortid'])))) {
			return $this->showmessage($post_invalid, '', array('minpostsize' => $this->setting['minpostsize'], 'maxpostsize' => $this->setting['maxpostsize']));
		}

		//note 灌水
		if(checkflood()) {
			return $this->showmessage('post_flood_ctrl', '', array('floodctrl' => $this->setting['floodctrl']));
		} elseif(checkmaxperhour('tid')) {
			return $this->showmessage('thread_flood_ctrl_threads_per_hour', '', array('threads_per_hour' => $this->group['maxthreadsperhour']));
		}
		$this->param['save'] = $this->member['uid'] ? $this->param['save'] : 0;

		$this->param['typeid'] = isset($this->param['typeid']) && isset($this->forum['threadtypes']['types'][$this->param['typeid']]) && (!$this->forum['threadtypes']['moderators'][$this->param['typeid']] || $this->forum['ismoderator']) ? $this->param['typeid'] : 0;

		$this->param['displayorder'] = $this->param['modnewthreads'] ? -2 : (($this->forum['ismoderator'] && $this->group['allowstickthread'] && !empty($this->param['sticktopic'])) ? 1 : (empty($this->param['save']) ? 0 : -4));
		if($this->param['displayorder'] == -2) {
			C::t('forum_forum')->update($this->forum['fid'], array('modworks' => '1'));
		}

		$this->param['digest'] = $this->forum['ismoderator'] && $this->group['allowdigestthread'] && !empty($this->param['digest']) ? 1 : 0;
		$this->param['readperm'] = $this->group['allowsetreadperm'] ? $this->param['readperm'] : 0;
		$this->param['isanonymous'] = $this->group['allowanonymous'] && $this->param['isanonymous'] ? 1 : 0;
		//note 帖子售价，如为特殊帖子则不可出售
		$this->param['price'] = intval($this->param['price']);
		if(!$this->param['special']) {
			$this->param['price'] = $this->group['maxprice'] ? ($this->param['price'] <= $this->group['maxprice'] ? $this->param['price'] : $this->group['maxprice']) : 0;
		}

		if(!$this->param['typeid'] && $this->forum['threadtypes']['required'] && !$this->param['special']) {
			return $this->showmessage('post_type_isnull');
		}

		if(!$this->param['sortid'] && $this->forum['threadsorts']['required'] && !$this->param['special']) {
			return $this->showmessage('post_sort_isnull');
		}

		if(!$this->param['special'] && $this->param['price'] > 0 && floor($this->param['price'] * (1 - $this->setting['creditstax'])) == 0) {
			return $this->showmessage('post_net_price_iszero');
		}


		$this->param['sortid'] = $this->param['special'] && $this->forum['threadsorts']['types'][$this->param['sortid']] ? 0 : $this->param['sortid'];
		$this->param['typeexpiration'] = intval($this->param['typeexpiration']);

		if($this->forum['threadsorts']['expiration'][$this->param['typeid']] && !$this->param['typeexpiration']) {
			return $this->showmessage('threadtype_expiration_invalid');
		}

		//note 作者
		$author = !$this->param['isanonymous'] ? $this->member['username'] : '';

		//note 斑竹管理过标志
		$this->param['moderated'] = $this->param['digest'] || $this->param['displayorder'] > 0 ? 1 : 0;

		//note 初始化状态变量
		//$thread['status'] = 0;

		//note 是否倒序看帖
		$this->param['ordertype'] && $this->param['tstatus'] = setstatus(4, 1, $this->param['tstatus']);

		//note 是否内容生成图片
		$this->param['imgcontent'] && $this->param['tstatus'] = setstatus(15, $this->param['imgcontent'], $this->param['tstatus']);

		//note 是否只有作者和管理人员可见回复
		$this->param['hiddenreplies'] && $this->param['tstatus'] = setstatus(2, 1, $this->param['tstatus']);


		$this->param['allownoticeauthor'] && $this->param['tstatus'] = setstatus(6, 1, $this->param['tstatus']);
		$this->param['isgroup'] = $this->forum['status'] == 3 ? 1 : 0;
		
		$this->param['publishdate'] = !$this->param['modnewthreads'] ? $this->param['publishdate'] : TIMESTAMP;

		$newthread = array(
			'fid' => $this->forum['fid'],
			'posttableid' => 0,
			'readperm' => $this->param['readperm'],
			'price' => $this->param['price'],
			'typeid' => $this->param['typeid'],
			'sortid' => $this->param['sortid'],
			'author' => $author,
			'authorid' => $this->member['uid'],
			'subject' => $this->param['subject'],
			'dateline' => $this->param['publishdate'],
			'lastpost' => $this->param['publishdate'],
			'lastposter' => $author,
			'displayorder' => $this->param['displayorder'],
			'digest' => $this->param['digest'],
			'special' => $this->param['special'],
			'attachment' => 0,
			'moderated' => $this->param['moderated'],
			'status' => $this->param['tstatus'],
			'isgroup' => $this->param['isgroup'],
			'replycredit' => $this->param['replycredit'],
			'closed' => $this->param['closed'] ? 1 : 0
		);
		$this->tid = C::t('forum_thread')->insert($newthread, true);
		C::t('forum_newthread')->insert(array(
		    'tid' => $this->tid,
		    'fid' => $this->forum['fid'],
		    'dateline' => $this->param['publishdate'],
		));
		useractionlog($this->member['uid'], 'tid');

		//note 新人第一主题帖处理
		if(!getuserprofile('threads') && $this->setting['newbie']) {
			C::t('forum_thread')->update($this->tid, array('icon' => $this->setting['newbie']));
		}
		//定时发帖
		if ($this->param['publishdate'] != TIMESTAMP) {
			//loadcache('cronpublish');
			$cron_publish_ids = dunserialize($this->cache('cronpublish'));
			$cron_publish_ids[$this->tid] = $this->tid;
			$cron_publish_ids = serialize($cron_publish_ids);
			savecache('cronpublish', $cron_publish_ids);
		}

		//更新空间动态
		//DB::update('common_member_field_home', array('recentnote'=>$subject), array('uid'=>$this->member['uid']));
		if(!$this->param['isanonymous']) {
			C::t('common_member_field_home')->update($this->member['uid'], array('recentnote'=>$this->param['subject']));
		}

		//note 斑竹管理
		if($this->param['moderated']) {
			updatemodlog($this->tid, ($this->param['displayorder'] > 0 ? 'STK' : 'DIG'));
			updatemodworks(($this->param['displayorder'] > 0 ? 'STK' : 'DIG'), 1);
		}

		//note 检查bbocde是否关闭或者不包含
		$this->param['bbcodeoff'] = checkbbcodes($this->param['message'], !empty($this->param['bbcodeoff']));
		//note 检查smiley是否关闭或者不包含
		$this->param['smileyoff'] = checksmilies($this->param['message'], !empty($this->param['smileyoff']));
		//note 禁止url解析
		$this->param['parseurloff'] = !empty($this->param['parseurloff']);
		//note 允许html
		$this->param['htmlon'] = $this->group['allowhtml'] && !empty($this->param['htmlon']) ? 1 : 0;
		$this->param['usesig'] = !empty($this->param['usesig']) && $this->group['maxsigsize'] ? 1 : 0;
		$class_tag = new tag();
		$this->param['tagstr'] = $class_tag->add_tag($this->param['tags'], $this->tid, 'tid');


		$this->param['pinvisible'] = $this->param['modnewthreads'] ? -2 : (empty($this->param['save']) ? 0 : -3);
		$this->param['message'] = preg_replace('/\[attachimg\](\d+)\[\/attachimg\]/is', '[attach]\1[/attach]', $this->param['message']);

		$this->param['pstatus'] = intval($this->param['pstatus']);
		defined('IN_MOBILE') && $this->param['pstatus'] = setstatus(4, 1, $this->param['pstatus']);

		if($this->param['imgcontent']) {
			stringtopic($this->param['message'], $this->tid, true, $this->param['imgcontentwidth']);
		}
		$this->pid = insertpost(array(
			'fid' => $this->forum['fid'],
			'tid' => $this->tid,
			'first' => '1',
			'author' => $this->member['username'],
			'authorid' => $this->member['uid'],
			'subject' => $this->param['subject'],
			'dateline' => $this->param['publishdate'],
			'message' => $this->param['message'],
			'useip' => $this->param['clientip'] ? $this->param['clientip'] : getglobal('clientip'),
			'port' => $this->param['remoteport'] ? $this->param['remoteport'] : getglobal('remoteport'),
			'invisible' => $this->param['pinvisible'],
			'anonymous' => $this->param['isanonymous'],
			'usesig' => $this->param['usesig'],
			'htmlon' => $this->param['htmlon'],
			'bbcodeoff' => $this->param['bbcodeoff'],
			'smileyoff' => $this->param['smileyoff'],
			'parseurloff' => $this->param['parseurloff'],
			'attachment' => '0',
			'tags' => $this->param['tagstr'],
			'replycredit' => 0,
			//添加手机状态位(IN_MOBILE)
			'status' => $this->param['pstatus']
		));

		//统计
		$statarr = array(0 => 'thread', 1 => 'poll', 2 => 'trade', 3 => 'reward', 4 => 'activity', 5 => 'debate', 127 => 'thread');
		include_once libfile('function/stat');
		updatestat($this->param['isgroup'] ? 'groupthread' : $statarr[$this->param['special']]);

// 		//更新发帖者端口号
// 		C::t('common_remote_port')->insert(array('id'=>$this->pid,'idtype'=>'post','useip'=>getglobal('clientip'),'port'=>getglobal('remoteport')), false, true);

		//note 记录地理位置信息
		if($this->param['geoloc'] && IN_MOBILE == 2) {
			list($mapx, $mapy, $location) = explode('|', $this->param['geoloc']);
			if($mapx && $mapy && $location) {
				C::t('forum_post_location')->insert(array(
					'pid' => $this->pid,
					'tid' => $this->tid,
					'uid' => $this->member['uid'],
					'mapx' => $mapx,
					'mapy' => $mapy,
					'location' => $location,
				));
			}
		}

		//note 管理 $modnewthreads
		if($this->param['modnewthreads']) {
			updatemoderate('tid', $this->tid);
			//DB::query("UPDATE ".DB::table('forum_forum')." SET todayposts=todayposts+1 WHERE fid='$_G[fid]'", 'UNBUFFERED');
			C::t('forum_forum')->update_forum_counter($this->forum['fid'], 0, 0, 1);
			manage_addnotify('verifythread');
			return 'post_newthread_mod_succeed';
			//return $this->showmessage('post_newthread_mod_succeed', $returnurl, $values, $param);
		} else {

			//note 更新论坛信息
			if($this->param['displayorder'] != -4) {
				//note 精华
				if($this->param['digest']) {
					updatepostcredits('+',  $this->member['uid'], 'digest', $this->forum['fid']);
				}
				//note 加积分
				updatepostcredits('+',  $this->member['uid'], 'post', $this->forum['fid']);
				if($this->param['isgroup']) { //note 给群组用户累记发帖数
					//DB::query("UPDATE ".DB::table('forum_groupuser')." SET threads=threads+1, lastupdate='".TIMESTAMP."' WHERE uid='$_G[uid]' AND fid='$_G[fid]'");
					C::t('forum_groupuser')->update_counter_for_user($this->member['uid'], $this->forum['fid'], 1);
				}

				$subject = str_replace("\t", ' ', $this->param['subject']);
				$lastpost = "$this->tid\t".$subject."\t".TIMESTAMP."\t$author";
				//DB::query("UPDATE ".DB::table('forum_forum')." SET lastpost='$lastpost', threads=threads+1, posts=posts+1, todayposts=todayposts+1 WHERE fid='$_G[fid]'", 'UNBUFFERED');
				C::t('forum_forum')->update($this->forum['fid'], array('lastpost' => $lastpost));
				C::t('forum_forum')->update_forum_counter($this->forum['fid'], 1, 1, 1);
				//note 如果为子论坛, 更新一级论坛的信息
				if($this->forum['type'] == 'sub') {
					//DB::query("UPDATE ".DB::table('forum_forum')." SET lastpost='$lastpost' WHERE fid='".$this->forum[fup]."'", 'UNBUFFERED');
					C::t('forum_forum')->update($this->forum['fup'], array('lastpost' => $lastpost));
				}
			}

			if($this->param['isgroup']) {
				C::t('forum_forumfield')->update($this->forum['fid'], array('lastupdate' => TIMESTAMP));
				require_once libfile('function/grouplog');
				updategroupcreditlog($this->forum['fid'], $this->member['uid']);
			}

			//抢沙发
			C::t('forum_sofa')->insert(array('tid' => $this->tid,'fid' => $this->forum['fid']));

			return 'post_newthread_succeed';
			//return $this->showmessage('post_newthread_succeed', $returnurl, $values, $param);

		}

	}

	public function feed() {
		if($this->forum('allowfeed') && !$this->param['isanonymous']) {
			if(empty($this->feed)) {
				$this->feed = array(
					'icon' => '',
					'title_template' => '',
					'title_data' => array(),
					'body_template' => '',
					'body_data' => array(),
					'title_data' => array(),
					'images' => array()
				);

				$message = !$this->param['price'] && !$this->param['readperm'] ? $this->param['message'] : '';
				$message = messagesafeclear($message);
				$this->feed['icon'] = 'thread';									//note feed 类型
				$this->feed['title_template'] = 'feed_thread_title'; //note 标题模板
				$this->feed['body_template'] = 'feed_thread_message';					//note 内容模板
				$this->feed['body_data'] = array(
					'subject' => "<a href=\"forum.php?mod=viewthread&tid={$this->tid}\">{$this->param['subject']}</a>",
					'message' => messagecutstr($message, 150)		//note 取内容摘要，最好别超过150个字符
				);
				if(getglobal('forum_attachexist')) {//					$firstaid = DB::result_first("SELECT aid FROM ".DB::table(getattachtablebytid($tid))." WHERE pid='$pid' AND dateline>'0' AND isimage='1' ORDER BY dateline LIMIT 1");
					$imgattach = C::t('forum_attachment_n')->fetch_max_image('tid:'.$this->tid, 'pid', $this->pid);
					$firstaid = $imgattach['aid'];
					unset($imgattach);
					if($firstaid) {
						$this->feed['images'] = array(getforumimg($firstaid));
						$this->feed['image_links'] = array("forum.php?mod=viewthread&do=tradeinfo&tid={$this->tid}&pid={$this->pid}");
					}
				}

			}


			$this->feed['title_data']['hash_data'] = 'tid'.$this->tid;
			$this->feed['id'] = $this->tid;
			$this->feed['idtype'] = 'tid';
			if($this->feed['icon']) {
				postfeed($this->feed);
			}
		}
	}
	protected function _init_parameters($parameters){
//
//				'fid' => $this->forum('fid'),
//				'posttableid' => 0,
//				'readperm' => $readperm,
//				'price' => $price,
//				'typeid' => $typeid,
//				'sortid' => $sortid,
//				'author' => $author,
//				'authorid' => $this->member['uid'],
//				'subject' => $subject,
//				'dateline' => $publishdate,
//				'lastpost' => $publishdate,
//				'lastposter' => $author,
//				'displayorder' => $displayorder,
//				'digest' => $digest,
//				'special' => $special,
//				'attachment' => 0,
//				'moderated' => $moderated,
//				'status' => $thread['status'],
//				'isgroup' => $isgroup,
//				'replycredit' => $replycredit,
//				'closed' => $closed ? 1 : 0

//
//			$pid = insertpost(array(
//				'fid' => $this->forum('fid'),
//				'tid' => $tid,
//				'first' => '1',
//				'author' => $this->member['username'],
//				'authorid' => $this->member['uid'],
//				'subject' => $subject,
//				'dateline' => $publishdate,
//				'message' => $message,
//				'useip' => $_G['clientip'],
//				'invisible' => $pinvisible,
//				'anonymous' => $isanonymous,
//				'usesig' => $usesig,
//				'htmlon' => $htmlon,
//				'bbcodeoff' => $bbcodeoff,
//				'smileyoff' => $smileyoff,
//				'parseurloff' => $parseurloff,
//				'attachment' => '0',
//				'tags' => $tagstr,
//				'replycredit' => 0,
//				//添加手机状态位(IN_MOBILE)
//				'status' => (defined('IN_MOBILE') ? 8 : 0)
//			));
		$varname = array(
			//公共数据
			'member', 'group', 'forum', 'extramessage',
			//threa数据
			'subject', 'sticktopic', 'save', 'ordertype', 'hiddenreplies',
			'allownoticeauthor', 'readperm', 'price', 'typeid', 'sortid',
			'publishdate', 'digest', 'moderated', 'tstatus', 'isgroup', 'imgcontent', 'imgcontentwidth',
			'replycredit', 'closed', 'special', 'tags',
			//post数据
			'message','clientip', 'invisible', 'isanonymous', 'usesig',
			'htmlon', 'bbcodeoff', 'smileyoff', 'parseurloff', 'pstatus', 'geoloc',
		);
		foreach($varname as $name) {
			if(!isset($this->param[$name]) && isset($parameters[$name])) {
				$this->param[$name] = $parameters[$name];
			}
		}

	}


	/**
	 * 获取/设置当前 版块 的某个键值
	 *
	 * <code>
	 * EXP: $this->forum('fid');
	 * RET: (string) 1
	 *
	 * EXP: $this->forum('name');
	 * RET: 版块名
	 *
	 * </code>
	 * @param string $name
	 * @return Mixed
	 */
	public function forum($name = null, $val = null) {
		if(isset($val)) {
			return $this->setvar($this->forum, $name, $val);
		} else {
			return $this->getvar($this->forum, $name);
		}
	}


}

?>

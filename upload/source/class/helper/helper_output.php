<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: helper_output.php 31663 2012-09-19 09:56:03Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}
/**
 * Description of helper_output
 * <code>
 * 
 * //输出json数据
 * helper_output::json(array('description'=>'我是JSON数据', 'datalist' => array('数据1', '数据2', '数据3')));
 * 
 * //对应的js脚本
 * <script>
 * var x = new Ajax();
 * //获取JSON数据
 * x.getJSON('forum.php?mod=misc&action=getJSON', function(s) {
 * 	//JSON数据错误时,s为null
 * 	if(s !== null) {
 * 		//code
 * 	}
 * });
 * </script>
 *
 * 
 * //输出HTML数据
 * helper_output::html('<div>我是HTML</div>');
 * 
 * //对应的js脚本
 * <script>
 * var x = new Ajax();
 * //获取HTML数据
 * x.getHTML('forum.php?mod=misc&action=getHTML', function(s) {
 * 	//code
 * });
 * </script>
 *
 * 
 * //输出xml数据
 * helper_output::xml('我是XML数据');//最终返回的数据是带xml头格式数据
 * 
 * //对应的js脚本
 * <script>
 * var x = new Ajax();
 * //获取XML数据(系统默认的)
 * x.get('forum.php?mod=misc&action=getdata', function(s) {
 * 	//code
 * });
 * </script>
 * </code>
 * 
 * @author zhangguosheng
 */

class helper_output {

	protected static function _header() {
		global $_G;
		ob_end_clean();
		$_G['gzipcompress'] ? ob_start('ob_gzhandler') : ob_start();
		@header("Expires: -1");
		@header("Cache-Control: no-store, private, post-check=0, pre-check=0, max-age=0", FALSE);
		@header("Pragma: no-cache");
		@header("Content-type: text/xml; charset=".CHARSET);
	}

	/**
	 * 以XML形式输出指定的字符串
	 * @global array $_G
	 * @param string $s  要输入的xml字符串
	 */
	public static function xml($s) {
		self::_header();
		echo '<?xml version="1.0" encoding="'.CHARSET.'"?>'."\r\n", '<root><![CDATA[', $s, ']]></root>';
		exit();
	}

	/**
	 * 输出JSON数据
	 * @param  [mixed]  $data [要输出结构化数据]
	 * @output [string]       [格式化的JSON数据]
	 * @return [null]
	 */
	public static function json($data) {
		self::_header();
		echo helper_json::encode($data);
		exit();
	}

	/**
	 * 输出HTML数据
	 * @param  [string] $s [要输出的HTML数据]
	 * @output [string]    [直接输入内容]
	 * @return null
	 */
	public static function html($s) {
		self::_header();
		echo $s;
		exit();
	}
}

?>
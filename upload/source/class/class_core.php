<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: class_core.php 33982 2013-09-12 06:36:35Z hypowang $
 */

error_reporting(E_ALL);

define('IN_DISCUZ', true);
define('DISCUZ_ROOT', substr(dirname(__FILE__), 0, -12));
define('DISCUZ_CORE_DEBUG', false); //记录PHP错误信息，可以指定错误信息的类型，参考errro_reporting参数；false为关闭;常用：E_ERROR ^ E_CORE_ERROR ^ E_COMPILE_ERROR ^ E_PARSE
define('DISCUZ_TABLE_EXTENDABLE', false); //设置数据层表类的扩展文件是否具有可扩展性
//#start
if(defined('DEBUG_MEMORY')) {
	if(!defined('MEMORY_FUNC')){
		define('MEMORY_FUNC', 'memory_get_peak_usage');
		define('MEMORY_FUNC_PARA', false);
	}
}
//#end

set_exception_handler(array('core', 'handleException'));

if(DISCUZ_CORE_DEBUG) {
	set_error_handler(array('core', 'handleError'));
	register_shutdown_function(array('core', 'handleShutdown'));
}

if(function_exists('spl_autoload_register')) {
	spl_autoload_register(array('core', 'autoload'));
} else {
	function __autoload($class) {
		return core::autoload($class);
	}
}

C::creatapp();

class core
{
	private static $_tables;
	private static $_imports;
	private static $_app;
	private static $_memory;

	/**
	 * 当前 application object
	 * @return discuz_application
	 */
	public static function app() {
		return self::$_app;
	}

	/**
	 * 创建 application
	 *
	 * @return discuz_application
	 */
	public static function creatapp() {
		if(!is_object(self::$_app)) {
			self::$_app = discuz_application::instance();
		}
		return self::$_app;
	}

	/**
	 * 引入 table 对象
	 * @param string $name table名称
	 * @return discuz_table|table_common_admincp_cmenu|table_common_admincp_group|table_common_admincp_member|table_common_admincp_perm|table_common_admincp_session|table_common_admingroup|table_common_adminnote|table_common_advertisement|table_common_advertisement_custom|table_common_banned|table_common_block|table_common_block_favorite|table_common_block_item|table_common_block_item_data|table_common_block_permission|table_common_block_pic|table_common_block_style|table_common_block_xml|table_common_cache|table_common_card|table_common_card_log|table_common_card_type|table_common_credit_log|table_common_credit_rule|table_common_credit_rule_log|table_common_credit_rule_log_field|table_common_cron|table_common_district|table_common_diy_data|table_common_domain|table_common_failedlogin|table_common_friendlink|table_common_grouppm|table_common_invite|table_common_magic|table_common_magiclog|table_common_mailcron|table_common_mailqueue|table_common_member|table_common_member_action_log|table_common_member_connect|table_common_member_count|table_common_member_crime|table_common_member_field_forum|table_common_member_field_home|table_home_follow|table_home_follow_feed|table_common_member_grouppm|table_common_member_log|table_common_member_magic|table_common_member_medal|table_common_member_profile|table_common_member_profile_setting|table_common_member_security|table_common_member_status|table_common_member_stat_field|table_common_member_validate|table_common_member_verify|table_common_member_verify_info|table_common_moderate|table_common_myapp|table_common_myinvite|table_common_mytask|table_common_nav|table_common_onlinetime|table_common_plugin|table_common_pluginvar|table_common_process|table_common_regip|table_common_relatedlink|table_common_report|table_common_searchindex|table_common_secquestion|table_common_session|table_common_setting|table_common_smiley|table_common_sphinxcounter|table_common_stat|table_common_statuser|table_common_style|table_common_stylevar|table_common_syscache|table_common_tag|table_common_tagitem|table_common_task|table_common_taskvar|table_common_template|table_common_template_block|table_common_template_permission|table_common_uin_black|table_common_usergroup|table_common_usergroup_field|table_common_word|table_common_word_type|table_connect_feedlog|table_connect_memberbindlog|table_connect_tlog|table_forum_access|table_forum_activity|table_forum_activityapply|table_forum_announcement|table_forum_attachment|table_forum_attachment_n|table_forum_attachment_exif|table_forum_attachment_unused|table_forum_attachtype|table_forum_bbcode|table_forum_creditslog|table_forum_debate|table_forum_debatepost|table_forum_faq|table_forum_forum|table_forum_forumfield|table_forum_forumrecommend|table_forum_forum_threadtable|table_forum_groupcreditslog|table_forum_groupfield|table_forum_groupinvite|table_forum_grouplevel|table_forum_groupranking|table_forum_groupuser|table_forum_imagetype|table_forum_medal|table_forum_medallog|table_forum_memberrecommend|table_forum_moderator|table_forum_modwork|table_forum_onlinelist|table_forum_order|table_forum_poll|table_forum_polloption|table_forum_pollvoter|table_forum_post|table_forum_postcomment|table_forum_postlog|table_forum_postposition|table_forum_poststick|table_forum_post_tableid|table_forum_promotion|table_forum_ratelog|table_forum_relatedthread|table_forum_replycredit|table_forum_rsscache|table_forum_spacecache|table_forum_statlog|table_forum_thread|table_forum_threadclass|table_forum_threadimage|table_forum_threadlog|table_forum_threadmod|table_forum_threadpartake|table_forum_threadrush|table_forum_threadtype|table_forum_trade|table_forum_tradecomment|table_forum_tradelog|table_forum_typeoption|table_forum_typeoptionvar|table_forum_typevar|table_forum_warning|table_home_album|table_home_album_category|table_home_appcreditlog|table_home_blacklist|table_home_blog|table_home_blogfield|table_home_blog_category|table_home_class|table_home_click|table_home_clickuser|table_home_comment|table_home_docomment|table_home_doing|table_home_favorite|table_home_feed|table_home_feed_app|table_home_friend|table_home_friendlog|table_home_friend_request|table_home_notification|table_home_pic|table_home_picfield|table_home_poke|table_home_pokearchive|table_home_share|table_home_show|table_home_specialuser|table_home_userapp|table_home_userappfield|table_home_visitor|table_portal_article_content|table_portal_article_count|table_portal_article_related|table_portal_article_title|table_portal_article_trash|table_portal_attachment|table_portal_category|table_portal_category_permission|table_portal_comment|table_portal_rsscache|table_portal_topic|table_portal_topic_pic|table_common_member_archive|table_common_member_profile_archive|table_common_member_count_archive|table_common_member_status_archive|table_common_member_field_forum_archive|table_common_member_field_home_archive
	 */
	public static function t($name) {
		return self::_make_obj($name, 'table', DISCUZ_TABLE_EXTENDABLE);
	}

	/**
	 * 引入 model 对象
	 * @param string $name model名称
	 * @return discuz_model|model_forum_thread
	 */
	public static function m($name) {
		$args = array();
		if(func_num_args() > 1) {
			$args = func_get_args();
			unset($args[0]);
		}
		return self::_make_obj($name, 'model', true, $args);
	}

	/**
	 * 生成指定类名的实例
	 * @param string $name 类的名称
	 * @param string $type 类型 table|model
	 * @param bool $extendable 是否可扩展方法，即可注册方法执行前后的扩展功能
	 * @param array $p method parameters，实例初始化的参数
	 * @return object
	 */
	protected static function _make_obj($name, $type, $extendable = false, $p = array()) {
		$pluginid = null;
		if($name[0] === '#') {
			list(, $pluginid, $name) = explode('#', $name);
		}
		$cname = $type.'_'.$name;
		if(!isset(self::$_tables[$cname])) {
			if(!class_exists($cname, false)) {
				self::import(($pluginid ? 'plugin/'.$pluginid : 'class').'/'.$type.'/'.$name);
			}
			if($extendable) {
				self::$_tables[$cname] = new discuz_container();
				switch (count($p)) {
					case 0:	self::$_tables[$cname]->obj = new $cname();break;
					case 1:	self::$_tables[$cname]->obj = new $cname($p[1]);break;
					case 2:	self::$_tables[$cname]->obj = new $cname($p[1], $p[2]);break;
					case 3:	self::$_tables[$cname]->obj = new $cname($p[1], $p[2], $p[3]);break;
					case 4:	self::$_tables[$cname]->obj = new $cname($p[1], $p[2], $p[3], $p[4]);break;
					case 5:	self::$_tables[$cname]->obj = new $cname($p[1], $p[2], $p[3], $p[4], $p[5]);break;
					default: $ref = new ReflectionClass($cname);self::$_tables[$cname]->obj = $ref->newInstanceArgs($p);unset($ref);break;
				}
			} else {
				self::$_tables[$cname] = new $cname();
			}
		}
		return self::$_tables[$cname];
	}

	/**
	 * 内存读写 object
	 * @return discuz_memory
	 */
	public static function memory() {
		if(!self::$_memory) {
			self::$_memory = new discuz_memory();
			self::$_memory->init(self::app()->config['memory']);
		}
		return self::$_memory;
	}

	/**
	 * 引入系统文件
	 * <code>
	 * <?php
	 * D::import('class/task/avatar');
	 * ?>
	 * </code>
	 * 相当于引入代码
	 * <code>
	 * include './source/class/task/task_avatar.php';
	 * </code>
	 * @param type $name 名称
	 * @param type $folder  特定目录
	 * @param bool $force 是否强制加载, 当强制加载的时候, 如果文件不存在则程序终止运行
	 */
	public static function import($name, $folder = '', $force = true) {
		$key = $folder.$name;
		if(!isset(self::$_imports[$key])) {
			$path = DISCUZ_ROOT.'/source/'.$folder;
			if(strpos($name, '/') !== false) {
				$pre = basename(dirname($name));
				$filename = dirname($name).'/'.$pre.'_'.basename($name).'.php';
			} else {
				$filename = $name.'.php';
			}

			if(is_file($path.'/'.$filename)) {
				include $path.'/'.$filename;
				self::$_imports[$key] = true;

				return true;
			} elseif(!$force) {
				return false;
			} else {
				throw new Exception('Oops! System file lost: '.$filename);
			}
		}
		return true;
	}

	/**
	 * Exception 消息截获
	 * @param Exception $exception
	 */
	public static function handleException($exception) {
		discuz_error::exception_error($exception);
	}


	/**
	 * 记录脚本执行时的错误信息
	 * @param int $errno
	 * @param string $errstr
	 * @param string $errfile
	 * @param int $errline
	 */
	public static function handleError($errno, $errstr, $errfile, $errline) {
		if($errno & DISCUZ_CORE_DEBUG) {
			discuz_error::system_error($errstr, false, true, false);
		}
	}

	/**
	 * 记录脚本执行结束时的错误信息
	 */
	public static function handleShutdown() {
		if(($error = error_get_last()) && $error['type'] & DISCUZ_CORE_DEBUG) {
			discuz_error::system_error($error['message'], false, true, false);
		}
	}

	/**
	 * PHP5 autoload class 支持
	 *
	 * 请注意class目录中文件以及类名的规则
	 * 当使用 class_exists() 检测的时候可以返回 true 或者 false
	 *
	 * @param string $class 类名称
	 * @return boolean
	 *
	 */
	public static function autoload($class) {
		$class = strtolower($class);
		if(strpos($class, '_') !== false) {
			list($folder) = explode('_', $class);
			$file = 'class/'.$folder.'/'.substr($class, strlen($folder) + 1);
		} else {
			$file = 'class/'.$class;
		}

		try {

			self::import($file);
			return true;

		} catch (Exception $exc) {

			//note 以下代码主要是为了保持系统函数 class_exists 正常运行，免得中断执行
			$trace = $exc->getTrace();
			foreach ($trace as $log) {
				if(empty($log['class']) && $log['function'] == 'class_exists') {
					return false;
				}
			}
			discuz_error::exception_error($exc);
		}
	}

	/**
	 * 开始指定名称的分析项
	 * @param string $name 分析项名称
	 */
	public static function analysisStart($name){
		$key = 'other';
		if($name[0] === '#') {
			list(, $key, $name) = explode('#', $name);
		}
		if(!isset($_ENV['analysis'])) {
			$_ENV['analysis'] = array();
		}
		if(!isset($_ENV['analysis'][$key])) {
			$_ENV['analysis'][$key] = array();
			$_ENV['analysis'][$key]['sum'] = 0;
		}
		$_ENV['analysis'][$key][$name]['start'] = microtime(TRUE);
		$_ENV['analysis'][$key][$name]['start_memory_get_usage'] = memory_get_usage();
		$_ENV['analysis'][$key][$name]['start_memory_get_real_usage'] = memory_get_usage(true);
		$_ENV['analysis'][$key][$name]['start_memory_get_peak_usage'] = memory_get_peak_usage();
		$_ENV['analysis'][$key][$name]['start_memory_get_peak_real_usage'] = memory_get_peak_usage(true);
	}

	/**
	 * 停止指定名称的分析项
	 * @param string $name 分析项名称
	 * @return array
	 */
	public static function analysisStop($name) {
		$key = 'other';
		if($name[0] === '#') {
			list(, $key, $name) = explode('#', $name);
		}
		if(isset($_ENV['analysis'][$key][$name]['start'])) {
			$diff = round((microtime(TRUE) - $_ENV['analysis'][$key][$name]['start']) * 1000, 5);
			$_ENV['analysis'][$key][$name]['time'] = $diff;
			$_ENV['analysis'][$key]['sum'] = $_ENV['analysis'][$key]['sum'] + $diff;
			unset($_ENV['analysis'][$key][$name]['start']);
			$_ENV['analysis'][$key][$name]['stop_memory_get_usage'] = memory_get_usage();
			$_ENV['analysis'][$key][$name]['stop_memory_get_real_usage'] = memory_get_usage(true);
			$_ENV['analysis'][$key][$name]['stop_memory_get_peak_usage'] = memory_get_peak_usage();
			$_ENV['analysis'][$key][$name]['stop_memory_get_peak_real_usage'] = memory_get_peak_usage(true);
		}
		return $_ENV['analysis'][$key][$name];
	}
}

class C extends core {}
class DB extends discuz_database {}

?>
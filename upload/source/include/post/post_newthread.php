<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: post_newthread.php 33695 2013-08-03 04:39:22Z nemohou $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

if(empty($_G['forum']['fid']) || $_G['forum']['type'] == 'group') {
	showmessage('forum_nonexistence');
}

if(($special == 1 && !$_G['group']['allowpostpoll']) || ($special == 2 && !$_G['group']['allowposttrade']) || ($special == 3 && !$_G['group']['allowpostreward']) || ($special == 4 && !$_G['group']['allowpostactivity']) || ($special == 5 && !$_G['group']['allowpostdebate'])) {
	showmessage('group_nopermission', NULL, array('grouptitle' => $_G['group']['grouptitle']), array('login' => 1));
}

if($_G['setting']['connect']['allow'] && $_G['setting']['accountguard']['postqqonly'] && !$_G['member']['conisbind']) {
	showmessage('postperm_qqonly_nopermission');
}

//note 判断用户组是否有权限
if(!$_G['uid'] && !((!$_G['forum']['postperm'] && $_G['group']['allowpost']) || ($_G['forum']['postperm'] && forumperm($_G['forum']['postperm'])))) {
	if(!defined('IN_MOBILE')) {
		showmessage('postperm_login_nopermission', NULL, array(), array('login' => 1));
	} else {
		showmessage('postperm_login_nopermission_mobile', NULL, array('referer' => rawurlencode(dreferer())), array('login' => 1));
	}
} elseif(empty($_G['forum']['allowpost'])) {
	if(!$_G['forum']['postperm'] && !$_G['group']['allowpost']) {
		showmessage('postperm_none_nopermission', NULL, array(), array('login' => 1));
	} elseif($_G['forum']['postperm'] && !forumperm($_G['forum']['postperm'])) {
		showmessagenoperm('postperm', $_G['fid'], $_G['forum']['formulaperm']);
	}
} elseif($_G['forum']['allowpost'] == -1) {
	showmessage('post_forum_newthread_nopermission', NULL);
}

if(!$_G['uid'] && ($_G['setting']['need_avatar'] || $_G['setting']['need_email'] || $_G['setting']['need_friendnum'])) {
	showmessage('postperm_login_nopermission', NULL, array(), array('login' => 1));
}

//note 检查积分
checklowerlimit('post', 0, 1, $_G['forum']['fid']);

//note 没有提交数据, 显示表单
if(!submitcheck('topicsubmit', 0, $seccodecheck, $secqaacheck)) {
	
	$st_t = $_G['uid'].'|'.TIMESTAMP;
	dsetcookie('st_t', $st_t.'|'.md5($st_t.$_G['config']['security']['authkey']));

	//取当前用户的最近关注的20个群组
	if(helper_access::check_module('group')) {
		$mygroups = $groupids = array();
		$groupids = C::t('forum_groupuser')->fetch_all_fid_by_uids($_G['uid']);
		array_slice($groupids, 0, 20);
		$query = C::t('forum_forum')->fetch_all_info_by_fids($groupids);
		foreach($query as $group) {
			$mygroups[$group['fid']] = $group['name'];
		}
	}

	$savethreads = array();
	$savethreadothers = array();
	//$query = DB::query("SELECT dateline, fid, tid, pid, subject FROM ".DB::table(getposttable())." WHERE authorid='$_G[uid]' AND invisible='-3' AND first='1'");
	foreach(C::t('forum_post')->fetch_all_by_authorid(0, $_G['uid'], false, '', 0, 20, 1, -3) as $savethread) {
		$savethread['dateline'] = dgmdate($savethread['dateline'], 'u');
		if($_G['fid'] == $savethread['fid']) {
			$savethreads[] = $savethread;
		} else {
			$savethreadothers[] = $savethread;
		}
	}
	$savethreadcount = count($savethreads);
	$savethreadothercount = count($savethreadothers);
	if($savethreadothercount) {
		loadcache('forums');
	}
	$savecount = $savethreadcount + $savethreadothercount;
	unset($savethread);

	$isfirstpost = 1;
	$allownoticeauthor = 1;
	$tagoffcheck = '';
	$showthreadsorts = !empty($sortid) || $_G['forum']['threadsorts']['required'] && empty($special);
	if(empty($sortid) && empty($special) && $_G['forum']['threadsorts']['required'] && $_G['forum']['threadsorts']['types']) {
		$tmp = array_keys($_G['forum']['threadsorts']['types']);
		$sortid = $tmp[0];

		require_once libfile('post/threadsorts', 'include');
	}

	if($special == 2 && $_G['group']['allowposttrade']) {

		$expiration_7days = date('Y-m-d', TIMESTAMP + 86400 * 7);
		$expiration_14days = date('Y-m-d', TIMESTAMP + 86400 * 14);
		$trade['expiration'] = $expiration_month = date('Y-m-d', mktime(0, 0, 0, date('m')+1, date('d'), date('Y')));
		$expiration_3months = date('Y-m-d', mktime(0, 0, 0, date('m')+3, date('d'), date('Y')));
		$expiration_halfyear = date('Y-m-d', mktime(0, 0, 0, date('m')+6, date('d'), date('Y')));
		$expiration_year = date('Y-m-d', mktime(0, 0, 0, date('m'), date('d'), date('Y')+1));

	} elseif($specialextra) {

		$threadpluginclass = null;
		if(isset($_G['setting']['threadplugins'][$specialextra]['module'])) {
			$threadpluginfile = DISCUZ_ROOT.'./source/plugin/'.$_G['setting']['threadplugins'][$specialextra]['module'].'.class.php';
			if(file_exists($threadpluginfile)) {
				@include_once $threadpluginfile;
				$classname = 'threadplugin_'.$specialextra;
				if(class_exists($classname) && method_exists($threadpluginclass = new $classname, 'newthread')) {
					$threadplughtml = $threadpluginclass->newthread($_G['fid']);
					$buttontext = lang('plugin/'.$specialextra, $threadpluginclass->buttontext);
					$iconfile = $threadpluginclass->iconfile;
					$iconsflip = array_flip($_G['cache']['icons']);
					$thread['iconid'] = $iconsflip[$iconfile];
				}
			}
		}

		if(!is_object($threadpluginclass)) {
			$specialextra = '';
		}
	}

	if($special == 4) {
		$activity = array('starttimeto' => '', 'starttimefrom' => '', 'place' => '', 'class' => '', 'cost' => '', 'number' => '', 'gender' => '', 'expiration' => '');
		$activitytypelist = $_G['setting']['activitytype'] ? explode("\n", trim($_G['setting']['activitytype'])) : '';
	}

	if($_G['group']['allowpostattach'] || $_G['group']['allowpostimage']) {
		$attachlist = getattach(0);
		$attachs = $attachlist['attachs'];
		$imgattachs = $attachlist['imgattachs'];
		unset($attachlist);
	}

	!isset($attachs['unused']) && $attachs['unused'] = array();
	!isset($imgattachs['unused']) && $imgattachs['unused'] = array();

	getgpc('infloat') ? include template('forum/post_infloat') : include template('forum/post');

//note 处理发送过来的post数据
} else {
	if($_GET['mygroupid']) {
		$mygroupid = explode('__', $_GET['mygroupid']);
		$mygid = intval($mygroupid[0]);
		if($mygid) {
			$mygname = $mygroupid[1];
			if(count($mygroupid) > 2) {
				unset($mygroupid[0]);
				$mygname = implode('__', $mygroupid);
			}
			$message .= '[groupid='.intval($mygid).']'.$mygname.'[/groupid]';
			//发帖时选择群组给群组加一分
			C::t('forum_forum')->update_commoncredits(intval($mygroupid[0]));
		}
	}
	$modthread = C::m('forum_thread');
	$bfmethods = $afmethods = array();

	$params = array(
		'subject' => $subject,
		'message' => $message,
		'typeid' => $typeid,
		'sortid' => $sortid,
		'special' => $special,
		//'modnewthreads' => $modnewthreads,
	);

//	//note 主题或者信息为空
//	if(trim($subject) == '') {
//		showmessage('post_sm_isnull');
//	}
//
//	if(!$sortid && !$special && trim($message) == '') {
//		showmessage('post_sm_isnull');
//	}
//
//	//note 主题属性检查
//	if($post_invalid = checkpost($subject, $message, ($special || $sortid))) {
//		showmessage($post_invalid, '', array('minpostsize' => $_G['setting']['minpostsize'], 'maxpostsize' => $_G['setting']['maxpostsize']));
//	}
//
//	//note 灌水
//	if(checkflood()) {
//		showmessage('post_flood_ctrl', '', array('floodctrl' => $_G['setting']['floodctrl']));
//	} elseif(checkmaxperhour('tid')) {
//		showmessage('thread_flood_ctrl_threads_per_hour', '', array('threads_per_hour' => $_G['group']['maxthreadsperhour']));
//	}
//
	$_GET['save'] = $_G['uid'] ? $_GET['save'] : 0;

	//note 定时发布
	if ($_G['group']['allowsetpublishdate'] && $_GET['cronpublish'] && $_GET['cronpublishdate']) {
		$publishdate = strtotime($_GET['cronpublishdate']);
		if ($publishdate > $_G['timestamp']) {
			$_GET['save'] = 1;
		} else {
			$publishdate = $_G['timestamp'];
		}
	} else {
		$publishdate = $_G['timestamp'];
	}
	$params['publishdate'] = $publishdate;
	$params['save'] = $_GET['save'];

//	$typeid = isset($typeid) && isset($_G['forum']['threadtypes']['types'][$typeid]) && (empty($_G['forum']['threadtypes']['moderators'][$typeid]) || $_G['forum']['ismoderator']) ? $typeid : 0;
	//$displayorder = $modnewthreads ? -2 : (($_G['forum']['ismoderator'] && $_G['group']['allowstickthread'] && !empty($_GET['sticktopic'])) ? 1 : (empty($_GET['save']) ? 0 : -4));
	//$params['displayorder'] = $displayorder;
	$params['sticktopic'] = $_GET['sticktopic'];

	//$digest = $_G['forum']['ismoderator'] && $_G['group']['allowdigestthread'] && !empty($_GET['addtodigest']) ? 1 : 0;
	$params['digest'] = $_GET['addtodigest'];
	//$readperm = $_G['group']['allowsetreadperm'] ? $readperm : 0;
	$params['readperm'] = $readperm;
	//$isanonymous = $_G['group']['allowanonymous'] && $_GET['isanonymous'] ? 1 : 0;
	$params['isanonymous'] = $_GET['isanonymous'];
//	if($displayorder == -2) {
//		/* debug
//		$has_moderated = $_G['cookie']['has_moderated'];
//		$has_moderated_arr = explode('|', $has_moderated);
//		if(!in_array($_G['fid'], $has_moderated_arr)) {
//			$has_moderated_arr[] = $_G['fid'];
//		}
//		dsetcookie('has_moderated', implode('|', $has_moderated_arr), 86400);*/
//		//DB::update('forum_forum', array('modworks' => '1'), "fid='{$_G['fid']}'");
//		C::t('forum_forum')->update($_G['fid'], array('modworks' => '1'));
//	} elseif($displayorder == -4) {
//		$_GET['addfeed'] = 0;
//	}
//	$digest = $_G['forum']['ismoderator'] && $_G['group']['allowdigestthread'] && !empty($_GET['addtodigest']) ? 1 : 0;
//	$readperm = $_G['group']['allowsetreadperm'] ? $readperm : 0;
//	$isanonymous = $_G['group']['allowanonymous'] && $_GET['isanonymous'] ? 1 : 0;
		//note 帖子售价，如为特殊帖子则不可出售
	//$price = intval($price);
	//$price = $_G['group']['maxprice'] && !$special ? ($price <= $_G['group']['maxprice'] ? $price : $_G['group']['maxprice']) : 0;
	$params['price'] = $_GET['price'];

//	if(!$typeid && $_G['forum']['threadtypes']['required'] && !$special) {
//		showmessage('post_type_isnull');
//	}
//
//	if(!$sortid && $_G['forum']['threadsorts']['required'] && !$special) {
//		showmessage('post_sort_isnull');
//	}
//
//	if($price > 0 && floor($price * (1 - $_G['setting']['creditstax'])) == 0) {
//		showmessage('post_net_price_iszero');
//	}

	if(in_array($special, array(1, 2, 3, 4, 5))) {
		$specials = array(
			1 => 'extend_thread_poll',
			2 => 'extend_thread_trade',
			3 => 'extend_thread_reward',
			4 => 'extend_thread_activity',
			5 => 'extend_thread_debate'
		);
		$bfmethods[] = array('class' => $specials[$special], 'method' => 'before_newthread');
		$afmethods[] = array('class' => $specials[$special], 'method' => 'after_newthread');

		if(!empty($_GET['addfeed'])) {
			$modthread->attach_before_method('feed', array('class' => $specials[$special], 'method' => 'before_feed'));
			if($special == 2) {
				$modthread->attach_before_method('feed', array('class' => $specials[$special], 'method' => 'before_replyfeed'));
			}
		}
	}

	//note 投票选项
	if($special == 1) {

//		$polloption = $_GET['tpolloption'] == 2 ? explode("\n", $_GET['polloptions']) : $_GET['polloption'];
//		$pollarray = array();
//		foreach($polloption as $key => $value) {
//			$polloption[$key] = censor($polloption[$key]);
//			if(trim($value) === '') {
//				unset($polloption[$key]);
//			}
//		}
//
//		if(count($polloption) > $_G['setting']['maxpolloptions']) {
//			showmessage('post_poll_option_toomany', '', array('maxpolloptions' => $_G['setting']['maxpolloptions']));
//		} elseif(count($polloption) < 2) {
//			showmessage('post_poll_inputmore');
//		}
//
//		$curpolloption = count($polloption);
//		$pollarray['maxchoices'] = empty($_GET['maxchoices']) ? 0 : ($_GET['maxchoices'] > $curpolloption ? $curpolloption : $_GET['maxchoices']);
//		$pollarray['multiple'] = empty($_GET['maxchoices']) || $_GET['maxchoices'] == 1 ? 0 : 1;
//		$pollarray['options'] = $polloption;//note cdb_polloptions:pollopts
//		$pollarray['visible'] = empty($_GET['visibilitypoll']);//note cdb_polls:vilible
//		$pollarray['overt'] = !empty($_GET['overt']);
//
//		if(preg_match("/^\d*$/", trim($_GET['expiration']))) {
//			if(empty($_GET['expiration'])) {
//				$pollarray['expiration'] = 0;
//			} else {
//				$pollarray['expiration'] = TIMESTAMP + 86400 * $_GET['expiration'];//note cdb_polls:expiration
//			}
//		} else {
//			showmessage('poll_maxchoices_expiration_invalid');
//		}

	} elseif($special == 3) {

//		$rewardprice = intval($_GET['rewardprice']);
//		if($rewardprice < 1) {
//			showmessage('reward_credits_please');
//		} elseif($rewardprice > 32767) {
//			showmessage('reward_credits_overflow');
//		} elseif($rewardprice < $_G['group']['minrewardprice'] || ($_G['group']['maxrewardprice'] > 0 && $rewardprice > $_G['group']['maxrewardprice'])) {
//			if($_G['group']['maxrewardprice'] > 0) {
//				showmessage('reward_credits_between', '', array('minrewardprice' => $_G['group']['minrewardprice'], 'maxrewardprice' => $_G['group']['maxrewardprice']));
//			} else {
//				showmessage('reward_credits_lower', '', array('minrewardprice' => $_G['group']['minrewardprice']));
//			}
//		} elseif(($realprice = $rewardprice + ceil($rewardprice * $_G['setting']['creditstax'])) > getuserprofile('extcredits'.$_G['setting']['creditstransextra'][2])) {
//			showmessage('reward_credits_shortage');
//		}
//		$price = $rewardprice;

	} elseif($special == 4) {
//
//		$activitytime = intval($_GET['activitytime']);
//		if(empty($_GET['starttimefrom'][$activitytime])) {
//			showmessage('activity_fromtime_please');
//		} elseif(@strtotime($_GET['starttimefrom'][$activitytime]) === -1 || @strtotime($_GET['starttimefrom'][$activitytime]) === FALSE) {
//			showmessage('activity_fromtime_error');
//		} elseif($activitytime && ((@strtotime($_GET['starttimefrom']) > @strtotime($_GET['starttimeto']) || !$_GET['starttimeto']))) {
//			showmessage('activity_fromtime_error');
//		} elseif(!trim($_GET['activityclass'])) {
//			showmessage('activity_sort_please');
//		} elseif(!trim($_GET['activityplace'])) {
//			showmessage('activity_address_please');
//		} elseif(trim($_GET['activityexpiration']) && (@strtotime($_GET['activityexpiration']) === -1 || @strtotime($_GET['activityexpiration']) === FALSE)) {
//			showmessage('activity_totime_error');
//		}
//
//		$activity = array();
//		$activity['class'] = censor(dhtmlspecialchars(trim($_GET['activityclass'])));
//		$activity['starttimefrom'] = @strtotime($_GET['starttimefrom'][$activitytime]);
//		$activity['starttimeto'] = $activitytime ? @strtotime($_GET['starttimeto']) : 0;
//		$activity['place'] = censor(dhtmlspecialchars(trim($_GET['activityplace'])));
//		$activity['cost'] = intval($_GET['cost']);
//		$activity['gender'] = intval($_GET['gender']);
//		$activity['number'] = intval($_GET['activitynumber']);
//
//		if($_GET['activityexpiration']) {
//			$activity['expiration'] = @strtotime($_GET['activityexpiration']);
//		} else {
//			$activity['expiration'] = 0;
//		}
//		if(trim($_GET['activitycity'])) {
//			$subject .= '['.dhtmlspecialchars(trim($_GET['activitycity'])).']';
//		}
//		$extfield = $_GET['extfield'];
//		$extfield = explode("\n", $_GET['extfield']);
//		foreach($extfield as $key => $value) {
//			$extfield[$key] = censor(trim($value));
//			if($extfield[$key] === '' || is_numeric($extfield[$key])) {
//				unset($extfield[$key]);
//			}
//		}
//		$extfield = array_unique($extfield);
//		if(count($extfield) > $_G['setting']['activityextnum']) {
//			showmessage('post_activity_extfield_toomany', '', array('maxextfield' => $_G['setting']['activityextnum']));
//		}
//		$activity['ufield'] = array('userfield' => $_GET['userfield'], 'extfield' => $extfield);
//		$activity['ufield'] = serialize($activity['ufield']);
//		if(intval($_GET['activitycredit']) > 0) {
//			$activity['credit'] = intval($_GET['activitycredit']);
//		}
	} elseif($special == 5) {

//		if(empty($_GET['affirmpoint']) || empty($_GET['negapoint'])) {
//			showmessage('debate_position_nofound');
//		} elseif(!empty($_GET['endtime']) && (!($endtime = @strtotime($_GET['endtime'])) || $endtime < TIMESTAMP)) {
//			showmessage('debate_endtime_invalid');
//		} elseif(!empty($_GET['umpire'])) {
//			//if(!DB::result_first("SELECT COUNT(*) FROM ".DB::table('common_member')." WHERE username='$_GET[umpire]'")) {
//			if(!C::t('common_member')->fetch_uid_by_username($_GET['umpire'])) {
//				$_GET['umpire'] = dhtmlspecialchars($_GET['umpire']);
//				showmessage('debate_umpire_invalid', '', array('umpire' => $umpire));
//			}
//		}
//		$affirmpoint = censor(dhtmlspecialchars($_GET['affirmpoint']));
//		$negapoint = censor(dhtmlspecialchars($_GET['negapoint']));
//		$stand = censor(intval($_GET['stand']));

	} elseif($specialextra) {

		@include_once DISCUZ_ROOT.'./source/plugin/'.$_G['setting']['threadplugins'][$specialextra]['module'].'.class.php';
		$classname = 'threadplugin_'.$specialextra;
		if(class_exists($classname) && method_exists($threadpluginclass = new $classname, 'newthread_submit')) {
			$threadpluginclass->newthread_submit($_G['fid']);
		}
		$special = 127;
		$params['special'] = 127;
		$params['message'] .= chr(0).chr(0).chr(0).$specialextra;

	}

//	$sortid = $special && $_G['forum']['threadsorts']['types'][$sortid] ? 0 : $sortid;
//	$typeexpiration = intval($_GET['typeexpiration']);
	$params['typeexpiration'] = $_GET['typeexpiration'];

//	if($_G['forum']['threadsorts']['expiration'][$typeid] && !$typeexpiration) {
//		showmessage('threadtype_expiration_invalid');
//	}

//	$_G['forum_optiondata'] = array();
//	if($_G['forum']['threadsorts']['types'][$sortid] && !$_G['forum']['allowspecialonly']) {
//		$_G['forum_optiondata'] = threadsort_validator($_GET['typeoption'], $pid);
//	}

	//note 作者
//	$author = !$isanonymous ? $_G['username'] : '';

	//note 斑竹管理过标志
//	$moderated = $digest || $displayorder > 0 ? 1 : 0;

	//note 初始化状态变量
//	$thread['status'] = 0;

	//note 是否倒序看帖
//	$_GET['ordertype'] && $thread['status'] = setstatus(4, 1, $thread['status']);
	$params['ordertype'] = $_GET['ordertype'];

	//note 是否只有作者和管理人员可见回复
//	$_GET['hiddenreplies'] && $thread['status'] = setstatus(2, 1, $thread['status']);
	$params['hiddenreplies'] = $_GET['hiddenreplies'];
//
//	//note 抢楼贴
//	if($_G['group']['allowpostrushreply'] && $_GET['rushreply']) {
//		$_GET['rushreplyfrom'] = strtotime($_GET['rushreplyfrom']);
//		$_GET['rushreplyto'] = strtotime($_GET['rushreplyto']);
//		$_GET['rewardfloor'] = trim($_GET['rewardfloor']);
//		$_GET['stopfloor'] = intval($_GET['stopfloor']);
//		$_GET['creditlimit'] = $_GET['creditlimit'] == '' ? '-996' : intval($_GET['creditlimit']);
//		if($_GET['rushreplyfrom'] > $_GET['rushreplyto'] && !empty($_GET['rushreplyto'])) {
//			showmessage('post_rushreply_timewrong');
//		}
//		if(($_GET['rushreplyfrom'] > $_G['timestamp']) || (!empty($_GET['rushreplyto']) && $_GET['rushreplyto'] < $_G['timestamp']) || ($_GET['stopfloor'] == 1) ) {
//			$closed = true;
//		}
//		//noteX 删除规则中奖励楼层大于截止楼层的用户输入,带有*号的不处理
//		if(!empty($_GET['rewardfloor']) && !empty($_GET['stopfloor'])) {
//			$floors = explode(',', $_GET['rewardfloor']);
//			if(!empty($floors) && is_array($floors)) {
//				foreach($floors AS $key => $floor) {
//					if(strpos($floor, '*') === false) {
//						if(intval($floor) == 0) {
//							unset($floors[$key]);
//						} elseif($floor > $_GET['stopfloor']) {
//							unset($floors[$key]);
//						}
//					}
//				}
//				$_GET['rewardfloor'] = implode(',', $floors);
//			}
//		}
//		$thread['status'] = setstatus(3, 1, $thread['status']);
//		$thread['status'] = setstatus(1, 1, $thread['status']);//note 抢楼贴需要缓存每个帖子的位置
//	}

//	$_GET['allownoticeauthor'] && $thread['status'] = setstatus(6, 1, $thread['status']);
	$params['allownoticeauthor'] = $_GET['allownoticeauthor'];
//	$isgroup = $_G['forum']['status'] == 3 ? 1 : 0;
	$params['tags'] = $_GET['tags'];
	$params['bbcodeoff'] = $_GET['bbcodeoff'];
	$params['smileyoff'] = $_GET['smileyoff'];
	$params['parseurloff'] = $_GET['parseurloff'];
	$params['usesig'] = $_GET['usesig'];
	$params['htmlon'] = $_GET['htmlon'];
	if($_G['group']['allowimgcontent']) {
		$params['imgcontent'] = $_GET['imgcontent'];
		$params['imgcontentwidth'] = $_G['setting']['imgcontentwidth'] ? intval($_G['setting']['imgcontentwidth']) : 100;
	}

	//note 地理位置信息传递
	$params['geoloc'] = diconv($_GET['geoloc'], 'UTF-8');

	//note 抢楼贴
	if($_GET['rushreply']) {
		$bfmethods[] = array('class' => 'extend_thread_rushreply', 'method' => 'before_newthread');
		$afmethods[] = array('class' => 'extend_thread_rushreply', 'method' => 'after_newthread');
	}

	//noteX 回帖送积分(不使用标志位)
	$bfmethods[] = array('class' => 'extend_thread_replycredit', 'method' => 'before_newthread');
	$afmethods[] = array('class' => 'extend_thread_replycredit', 'method' => 'after_newthread');

	if($sortid) {
		$bfmethods[] = array('class' => 'extend_thread_sort', 'method' => 'before_newthread');
		$afmethods[] = array('class' => 'extend_thread_sort', 'method' => 'after_newthread');
	}
	$bfmethods[] = array('class' => 'extend_thread_allowat', 'method' => 'before_newthread');
	$afmethods[] = array('class' => 'extend_thread_allowat', 'method' => 'after_newthread');
	$afmethods[] = array('class' => 'extend_thread_image', 'method' => 'after_newthread');

	if(!empty($_GET['adddynamic'])) {
		$afmethods[] = array('class' => 'extend_thread_follow', 'method' => 'after_newthread');
	}

	$modthread->attach_before_methods('newthread', $bfmethods);
	$modthread->attach_after_methods('newthread', $afmethods);

//	$modthread->setting('a', '1');
//	$modthread->member(null, C::t('member')->fetch('32423'));
//	$modthread->showmessage = var_dump;
//	$modthread->showmessage = array('discuz_model', 'fetch');
	$return = $modthread->newthread($params);
	$tid = $modthread->tid;
	$pid = $modthread->pid;
//
//	//noteX 回帖送积分(不使用标志位)
//	if($_G['group']['allowreplycredit']) {
//		$_GET['replycredit_extcredits'] = intval($_GET['replycredit_extcredits']);
//		$_GET['replycredit_times'] = intval($_GET['replycredit_times']);
//		$_GET['replycredit_membertimes'] = intval($_GET['replycredit_membertimes']);
//		$_GET['replycredit_random'] = intval($_GET['replycredit_random']);
//
//		$_GET['replycredit_random'] = $_GET['replycredit_random'] < 0 || $_GET['replycredit_random'] > 99 ? 0 : $_GET['replycredit_random'] ;
//		$replycredit = $replycredit_real = 0;
//		if($_GET['replycredit_extcredits'] > 0 && $_GET['replycredit_times'] > 0) {
//			$replycredit_real = ceil(($_GET['replycredit_extcredits'] * $_GET['replycredit_times']) + ($_GET['replycredit_extcredits'] * $_GET['replycredit_times'] *  $_G['setting']['creditstax']));
//			if($replycredit_real > getuserprofile('extcredits'.$_G['setting']['creditstransextra'][10])) {
//				showmessage('replycredit_morethan_self');
//			} else {
//				$replycredit = ceil($_GET['replycredit_extcredits'] * $_GET['replycredit_times']);
//			}
//		}
//	}

////	DB::query("INSERT INTO ".DB::table('forum_thread')." (fid, posttableid, readperm, price, typeid, sortid, author, authorid, subject, dateline, lastpost, lastposter, displayorder, digest, special, attachment, moderated, status, isgroup, replycredit, closed)
////		VALUES ('$_G[fid]', '0', '$readperm', '$price', '$typeid', '$sortid', '$author', '$_G[uid]', '$subject', '$publishdate', '$publishdate', '$author', '$displayorder', '$digest', '$special', '0', '$moderated', '$thread[status]', '$isgroup', '$replycredit', '".($closed ? "1" : '0')."')");
////	$tid = DB::insert_id();
//
//	$newthread = array(
//		'fid' => $_G['fid'],
//		'posttableid' => 0,
//		'readperm' => $readperm,
//		'price' => $price,
//		'typeid' => $typeid,
//		'sortid' => $sortid,
//		'author' => $author,
//		'authorid' => $_G['uid'],
//		'subject' => $subject,
//		'dateline' => $publishdate,
//		'lastpost' => $publishdate,
//		'lastposter' => $author,
//		'displayorder' => $displayorder,
//		'digest' => $digest,
//		'special' => $special,
//		'attachment' => 0,
//		'moderated' => $moderated,
//		'status' => $thread['status'],
//		'isgroup' => $isgroup,
//		'replycredit' => $replycredit,
//		'closed' => $closed ? 1 : 0
//	);
//	$tid = C::t('forum_thread')->insert($newthread, true);
//	useractionlog($_G['uid'], 'tid');
//
//	//note 新人第一主题帖处理
//	if(!getuserprofile('threads') && $_G['setting']['newbie']) {
////		DB::query("UPDATE ".DB::table('forum_thread')." SET icon='".$_G['setting']['newbie']."' WHERE tid='$tid'");
//		C::t('forum_thread')->update($tid, array('icon' => $_G['setting']['newbie']));
//	}
//	if ($publishdate != $_G['timestamp']) {
//		loadcache('cronpublish');
//		$cron_publish_ids = dunserialize($_G['cache']['cronpublish']);
//		$cron_publish_ids[$tid] = $tid;
//		$cron_publish_ids = serialize($cron_publish_ids);
//		savecache('cronpublish', $cron_publish_ids);
//	}
//
//
//	//更新空间动态
//	//DB::update('common_member_field_home', array('recentnote'=>$subject), array('uid'=>$_G['uid']));
//	if(!$isanonymous) {
//		C::t('common_member_field_home')->update($_G['uid'], array('recentnote'=>$subject));
//	}
//
//	if($special == 3 && $_G['group']['allowpostreward']) {
//		updatemembercount($_G['uid'], array($_G['setting']['creditstransextra'][2] => -$realprice), 1, 'RTC', $tid);
//	}
//
//	//note 斑竹管理
//	if($moderated) {
//		updatemodlog($tid, ($displayorder > 0 ? 'STK' : 'DIG'));
//		updatemodworks(($displayorder > 0 ? 'STK' : 'DIG'), 1);
//	}

//	if($special == 1) {//note 投票入库
//
//		foreach($pollarray['options'] as $polloptvalue) {
//			$polloptvalue = dhtmlspecialchars(trim($polloptvalue));
//			//DB::query("INSERT INTO ".DB::table('forum_polloption')." (tid, polloption) VALUES ('$tid', '$polloptvalue')");
//			C::t('forum_polloption')->insert(array('tid' => $tid, 'polloption' => $polloptvalue));
//		}
//		$polloptionpreview = '';
//		//$query = DB::query("SELECT polloption FROM ".DB::table('forum_polloption')." WHERE tid='$tid' ORDER BY displayorder LIMIT 2");
//		$query = C::t('forum_polloption')->fetch_all_by_tid($tid, 1, 2);
//		//while($option = DB::fetch($query)) {
//		foreach($query as $option) {
//			$polloptvalue = preg_replace("/\[url=(https?){1}:\/\/([^\[\"']+?)\](.+?)\[\/url\]/i", "<a href=\"\\1://\\2\" target=\"_blank\">\\3</a>", $option['polloption']);
//			$polloptionpreview .= $polloptvalue."\t";
//		}
//
//		$polloptionpreview = daddslashes($polloptionpreview);
//
//		//note 投票入库
//		//DB::query("INSERT INTO ".DB::table('forum_poll')." (tid, multiple, visible, maxchoices, expiration, overt, pollpreview) VALUES ('$tid', '$pollarray[multiple]', '$pollarray[visible]', '$pollarray[maxchoices]', '$pollarray[expiration]', '$pollarray[overt]', '$polloptionpreview')");
//		$data = array('tid' => $tid, 'multiple' => $pollarray['multiple'], 'visible' => $pollarray['visible'], 'maxchoices' => $pollarray['maxchoices'], 'expiration' => $pollarray['expiration'], 'overt' => $pollarray['overt'], 'pollpreview' => $polloptionpreview);
//		C::t('forum_poll')->insert($data);
//	} elseif($special == 4 && $_G['group']['allowpostactivity']) {//note 活动入库
		//note 活动入库
		//DB::query("INSERT INTO ".DB::table('forum_activity')." (tid, uid, cost, starttimefrom, starttimeto, place, class, gender, number, expiration, aid, ufield, credit) VALUES ('$tid', '$_G[uid]', '$activity[cost]', '$activity[starttimefrom]', '$activity[starttimeto]', '$activity[place]', '$activity[class]', '$activity[gender]', '$activity[number]', '$activity[expiration]', '$_GET[activityaid]', '$activity[ufield]', '$activity[credit]')");
//		$data = array('tid' => $tid, 'uid' => $_G['uid'], 'cost' => $activity['cost'], 'starttimefrom' => $activity['starttimefrom'], 'starttimeto' => $activity['starttimeto'], 'place' => $activity['place'], 'class' => $activity['class'], 'gender' => $activity['gender'], 'number' => $activity['number'], 'expiration' => $activity['expiration'], 'aid' => $_GET['activityaid'], 'ufield' => $activity['ufield'], 'credit' => $activity['credit']);
//		C::t('forum_activity')->insert($data);

//	} elseif($special == 5 && $_G['group']['allowpostdebate']) {//note 辩论入库

////		DB::query("INSERT INTO ".DB::table('forum_debate')." (tid, uid, starttime, endtime, affirmdebaters, negadebaters, affirmvotes, negavotes, umpire, winner, bestdebater, affirmpoint, negapoint, umpirepoint)
////			VALUES ('$tid', '$_G[uid]', '$publishdate', '$endtime', '0', '0', '0', '0', '$_GET[umpire]', '', '', '$affirmpoint', '$negapoint', '')");
//		C::t('forum_debate')->insert(array(
//		    'tid' => $tid,
//		    'uid' => $_G['uid'],
//		    'starttime' => $publishdate,
//		    'endtime' => $endtime,
//		    'affirmdebaters' => 0,
//		    'negadebaters' => 0,
//		    'affirmvotes' => 0,
//		    'negavotes' => 0,
//		    'umpire' => $_GET['umpire'],
//		    'winner' => '',
//		    'bestdebater' => '',
//		    'affirmpoint' => $affirmpoint,
//		    'negapoint' => $negapoint,
//		    'umpirepoint' => ''
//		));

//	} elseif($special == 127) {
//
//		$message .= chr(0).chr(0).chr(0).$specialextra;
//
//	}

//	if($_G['forum']['threadsorts']['types'][$sortid] && !empty($_G['forum_optiondata']) && is_array($_G['forum_optiondata'])) {
//		$filedname = $valuelist = $separator = '';
//		foreach($_G['forum_optiondata'] as $optionid => $value) {
//			if($value) {
//				$filedname .= $separator.$_G['forum_optionlist'][$optionid]['identifier'];
//				$valuelist .= $separator."'".daddslashes($value)."'";
//				$separator = ' ,';
//			}
//
//			if($_G['forum_optionlist'][$optionid]['type'] == 'image') {
//				$identifier = $_G['forum_optionlist'][$optionid]['identifier'];
//				$sortaids[] = intval($_GET['typeoption'][$identifier]['aid']);
//			}
//
//			//DB::query("INSERT INTO ".DB::table('forum_typeoptionvar')." (sortid, tid, fid, optionid, value, expiration)
//			//	VALUES ('$sortid', '$tid', '$_G[fid]', '$optionid', '$value', '".($typeexpiration ? $publishdate + $typeexpiration : 0)."')");
//			C::t('forum_typeoptionvar')->insert(array(
//				'sortid' => $sortid,
//				'tid' => $tid,
//				'fid' => $_G['fid'],
//				'optionid' => $optionid,
//				'value' => censor($value),
//				'expiration' => ($typeexpiration ? $publishdate + $typeexpiration : 0),
//			));
//		}
//
//		if($filedname && $valuelist) {
////			DB::query("INSERT INTO ".DB::table('forum_optionvalue')."$sortid ($filedname, tid, fid) VALUES ($valuelist, '$tid', '$_G[fid]')");
//			C::t('forum_optionvalue')->insert($sortid, "($filedname, tid, fid) VALUES ($valuelist, '$tid', '$_G[fid]')");
//		}
//	}
//
//	if($_G['group']['allowat']) {
//		$atlist = $atlist_tmp = array();
//		preg_match_all("/@([^\r\n]*?)\s/i", $message.' ', $atlist_tmp);
//		$atlist_tmp = array_slice(array_unique($atlist_tmp[1]), 0, $_G['group']['allowat']);
//		if(!empty($atlist_tmp)) {
//			if(empty($_G['setting']['at_anyone'])) {
//	//			$query = DB::query("SELECT * FROM ".DB::table('home_follow')." WHERE uid='$_G[uid]' AND fusername IN(".dimplode($atlist_tmp).")");
//	//			while($row = DB::fetch($query)) {
//				foreach(C::t('home_follow')->fetch_all_by_uid_fusername($_G['uid'], $atlist_tmp) as $row) {
//					$atlist[$row['followuid']] = $row['fusername'];
//				}
//				if(count($atlist) < $_G['group']['allowat']) {
//					//$query = DB::query("SELECT * FROM ".DB::table('home_friend')." WHERE uid='$_G[uid]' AND fusername IN(".dimplode($atlist_tmp).")");
//					$query = C::t('home_friend')->fetch_all_by_uid_username($_G['uid'], $atlist_tmp);
//					//while($row = DB::fetch($query)) {
//					foreach($query as $row) {
//						$atlist[$row['fuid']] = $row['fusername'];
//					}
//				}
//			} else {
//				//$query = DB::query("SELECT * FROM ".DB::table('common_member')." WHERE username IN(".dimplode($atlist_tmp).")");
//				//while($row = DB::fetch($query)) {
//				foreach(C::t('common_member')->fetch_all_by_username($atlist_tmp) as $row) {
//					$atlist[$row['uid']] = $row['username'];
//				}
//			}
//		}
//		if($atlist) {
//			foreach($atlist as $atuid => $atusername) {
//				$atsearch[] = "/@$atusername /i";
//				$atreplace[] = "[url=home.php?mod=space&uid=$atuid]@{$atusername}[/url] ";
//			}
//			$message = preg_replace($atsearch, $atreplace, $message.' ', 1);
//		}
//	}
//
//	//note 检查bbocde是否关闭或者不包含
//	$bbcodeoff = checkbbcodes($message, !empty($_GET['bbcodeoff']));
//	//note 检查smiley是否关闭或者不包含
//	$smileyoff = checksmilies($message, !empty($_GET['smileyoff']));
//	//note 禁止url解析
//	$parseurloff = !empty($_GET['parseurloff']);
//	//note 允许html
//	$htmlon = $_G['group']['allowhtml'] && !empty($_GET['htmlon']) ? 1 : 0;
//	$usesig = !empty($_GET['usesig']) && $_G['group']['maxsigsize'] ? 1 : 0;
//	$class_tag = new tag();
//	$tagstr = $class_tag->add_tag($_GET['tags'], $tid, 'tid');
//
//	//noteX 回帖送积分规则保存及减扣积分并记录
//	if($_G['group']['allowreplycredit']) {
//		if($replycredit > 0 && $replycredit_real > 0) {
//			updatemembercount($_G['uid'], array('extcredits'.$_G['setting']['creditstransextra'][10] => -$replycredit_real), 1, 'RCT', $tid);
////			DB::query("INSERT INTO ".DB::table('forum_replycredit')." (tid, extcredits, extcreditstype, times, membertimes, random) VALUES ('$tid', '$_GET[replycredit_extcredits]', '{$_G[setting][creditstransextra][10]}', '$_GET[replycredit_times]', '$_GET[replycredit_membertimes]', '$_GET[replycredit_random]')");
//			$insertdata = array(
//					'tid' => $tid,
//					'extcredits' => $_GET['replycredit_extcredits'],
//					'extcreditstype' => $_G['setting']['creditstransextra'][10],
//					'times' => $_GET['replycredit_times'],
//					'membertimes' => $_GET['replycredit_membertimes'],
//					'random' => $_GET['replycredit_random']
//				);
//			C::t('forum_replycredit')->insert($insertdata);
//		}
//	}
//
//	//noteX 抢楼帖设置项
//	if($_G['group']['allowpostrushreply'] && $_GET['rushreply']) {
//		//DB::query("INSERT INTO ".DB::table('forum_threadrush')." (tid, stopfloor, starttimefrom, starttimeto, rewardfloor, creditlimit) VALUES ('$tid', '$_GET[stopfloor]', '$_GET[rushreplyfrom]', '$_GET[rushreplyto]', '$_GET[rewardfloor]', '$_GET[creditlimit]')");
//		$rushdata = array('tid' => $tid, 'stopfloor' => $_GET['stopfloor'], 'starttimefrom' => $_GET['rushreplyfrom'], 'starttimeto' => $_GET['rushreplyto'], 'rewardfloor' => $_GET['rewardfloor'], 'creditlimit' => $_GET['creditlimit']);
//		C::t('forum_threadrush')->insert($rushdata);
//	}
//
//	$pinvisible = $modnewthreads ? -2 : (empty($_GET['save']) ? 0 : -3);
//	$message = preg_replace('/\[attachimg\](\d+)\[\/attachimg\]/is', '[attach]\1[/attach]', $message);
//	/*
//	DB::query("INSERT INTO ".DB::table(getposttable())." (fid, tid, first, author, authorid, subject, dateline, message, useip, invisible, anonymous, usesig, htmlon, bbcodeoff, smileyoff, parseurloff, attachment, tags)
//		VALUES ('$_G[fid]', '$tid', '1', '$_G[username]', '$_G[uid]', '$subject', '$_G[timestamp]', '$message', '$_G[clientip]', '$pinvisible', '$isanonymous', '$_GET[usesig]', '$htmlon', '$bbcodeoff', '$smileyoff', '$parseurloff', '0', '".implode(',', $tagarray)."')");
//	$pid = DB::insert_id();*/
//	$pid = insertpost(array(
//		'fid' => $_G['fid'],
//		'tid' => $tid,
//		'first' => '1',
//		'author' => $_G['username'],
//		'authorid' => $_G['uid'],
//		'subject' => $subject,
//		'dateline' => $publishdate,
//		'message' => $message,
//		'useip' => $_G['clientip'],
//		'invisible' => $pinvisible,
//		'anonymous' => $isanonymous,
//		'usesig' => $usesig,
//		'htmlon' => $htmlon,
//		'bbcodeoff' => $bbcodeoff,
//		'smileyoff' => $smileyoff,
//		'parseurloff' => $parseurloff,
//		'attachment' => '0',
//		'tags' => $tagstr,
//		'replycredit' => 0,
//		//添加手机状态位(IN_MOBILE)
//		'status' => (defined('IN_MOBILE') ? 8 : 0)
//	));
//	if($_G['group']['allowat'] && $atlist) {
//		foreach($atlist as $atuid => $atusername) {
//			notification_add($atuid, 'at', 'at_message', array('from_id' => $tid, 'from_idtype' => 'at', 'buyerid' => $_G['uid'], 'buyer' => $_G['username'], 'tid' => $tid, 'subject' => $subject, 'pid' => $pid, 'message' => messagecutstr($message, 150)));
//		}
//		set_atlist_cookie(array_keys($atlist));
//	}
/*
	if($pid && getstatus($thread['status'], 1)) {
		savepostposition($tid, $pid);
	}
 *
 */

//	$threadimageaid = 0;
//	$threadimage = array();
//	if($special == 4 && $_GET['activityaid']) {
//		$threadimageaid = $_GET['activityaid'];
//		convertunusedattach($_GET['activityaid'], $tid, $pid);
//	}
//
////	if($_G['forum']['threadsorts']['types'][$sortid] && !empty($_G['forum_optiondata']) && is_array($_G['forum_optiondata']) && $sortaids) {
////		foreach($sortaids as $sortaid) {
////			convertunusedattach($sortaid, $tid, $pid);
////		}
////	}
//
//	if(($_G['group']['allowpostattach'] || $_G['group']['allowpostimage']) && ($_GET['attachnew'] || $sortid || !empty($_GET['activityaid']))) {
//		updateattach($displayorder == -4 || $modnewthreads, $tid, $pid, $_GET['attachnew']);
//		if(!$threadimageaid) {
////			$threadimage = DB::fetch_first("SELECT aid, attachment, remote FROM ".DB::table(getattachtablebytid($tid))." WHERE tid='$tid' AND isimage IN ('1', '-1') ORDER BY width DESC LIMIT 1");
//			$threadimage = C::t('forum_attachment_n')->fetch_max_image('tid:'.$tid, 'tid', $tid);
//			$threadimageaid = $threadimage['aid'];
//		}
//	}
//
//	$values = array('fid' => $_G['fid'], 'tid' => $tid, 'pid' => $pid, 'coverimg' => '');
//	$param = array();
//	if($_G['forum']['picstyle']) {
//		if(!setthreadcover($pid, 0, $threadimageaid)) {
//			preg_match_all("/(\[img\]|\[img=\d{1,4}[x|\,]\d{1,4}\])\s*([^\[\<\r\n]+?)\s*\[\/img\]/is", $message, $imglist, PREG_SET_ORDER);
//			$values['coverimg'] = "<p id=\"showsetcover\">".lang('message', 'post_newthread_set_cover')."<span id=\"setcoverwait\"></span></p><script>if($('forward_a')){\$('forward_a').style.display='none';setTimeout(\"$('forward_a').style.display=''\", 5000);};ajaxget('forum.php?mod=ajax&action=setthreadcover&tid=$tid&pid=$pid&fid=$_G[fid]&imgurl={$imglist[0][2]}&newthread=1', 'showsetcover', 'setcoverwait')</script>";
//			$param['clean_msgforward'] = 1;
//			$param['timeout'] = $param['refreshtime'] = 15;
//		}
//	}
//
//	if($threadimageaid) {
//		if(!$threadimage) {
////			$threadimage = DB::fetch_first("SELECT attachment, remote FROM ".DB::table(getattachtablebytid($tid))." WHERE aid='$threadimageaid'");
//			$threadimage = C::t('forum_attachment_n')->fetch('tid:'.$tid, $threadimageaid);
//		}
//		$threadimage = daddslashes($threadimage);
//		//DB::insert('forum_threadimage', array(
//		C::t('forum_threadimage')->insert(array(
//			'tid' => $tid,
//			'attachment' => $threadimage['attachment'],
//			'remote' => $threadimage['remote'],
//		));
//	}

//
//	$statarr = array(0 => 'thread', 1 => 'poll', 2 => 'trade', 3 => 'reward', 4 => 'activity', 5 => 'debate', 127 => 'thread');
//	//统计
//	include_once libfile('function/stat');
//	updatestat($isgroup ? 'groupthread' : $statarr[$special]);

	dsetcookie('clearUserdata', 'forum');
	if($specialextra) {
		$classname = 'threadplugin_'.$specialextra;
		if(class_exists($classname) && method_exists($threadpluginclass = new $classname, 'newthread_submit_end')) {
			$threadpluginclass->newthread_submit_end($_G['fid'], $modthread->tid);
		}
	}
	if(!$modthread->param('modnewthreads') && !empty($_GET['addfeed'])) {
		$modthread->feed();
	}

	if(!empty($_G['setting']['rewriterule']['forum_viewthread']) && in_array('forum_viewthread', $_G['setting']['rewritestatus'])) {
		$returnurl = rewriteoutput('forum_viewthread', 1, '', $modthread->tid, 1, '', $extra);
	} else {
		$returnurl = "forum.php?mod=viewthread&tid={$modthread->tid}&extra=$extra";
	}
	$values = array('fid' => $modthread->forum('fid'), 'tid' => $modthread->tid, 'pid' => $modthread->pid, 'coverimg' => '', 'sechash' => !empty($_GET['sechash']) ? $_GET['sechash'] : '');
	showmessage($return, $returnurl, array_merge($values, (array)$modthread->param('values')), $modthread->param('param'));

	//note 管理 $modnewthreads
//	if($modnewthreads) {
//		updatemoderate('tid', $tid);
//		//DB::query("UPDATE ".DB::table('forum_forum')." SET todayposts=todayposts+1 WHERE fid='$_G[fid]'", 'UNBUFFERED');
//		C::t('forum_forum')->update_forum_counter($_G['fid'], 0, 0, 1);
//		manage_addnotify('verifythread');
//		showmessage('post_newthread_mod_succeed', $returnurl, $values, $param);
//	} else {
//
//		if($displayorder >= 0 && helper_access::check_module('follow') && !empty($_GET['adddynamic']) && !$isanonymous) {
//			//是否取消广播操作
//			require_once libfile('function/discuzcode');
//			require_once libfile('function/followcode');
//			$feedcontent = array(
//				'tid' => $tid,
//				'content' => followcode($message, $tid, $pid, 1000),
//			);
//			C::t('forum_threadpreview')->insert($feedcontent);
//			C::t('forum_thread')->update_status_by_tid($tid, '512'); //note 标记forum_thread
//			$followfeed = array(
//				'uid' => $_G['uid'],
//				'username' => $_G['username'],
//				'tid' => $tid,
//				'note' => '',
//				'dateline' => TIMESTAMP
//			);
//			$values['feedid'] = C::t('home_follow_feed')->insert($followfeed, true);
//			C::t('common_member_count')->increase($_G['uid'], array('feeds'=>1));
//		}
//		$feed = array(
//			'icon' => '',
//			'title_template' => '',
//			'title_data' => array(),
//			'body_template' => '',
//			'body_data' => array(),
//			'title_data'=>array(),
//			'images'=>array()
//		);
//		if(!empty($_GET['addfeed']) && $_G['forum']['allowfeed'] && !$isanonymous) {
//			$message = !$price && !$readperm ? $message : '';
//			if($special == 0) {
////				$feed['icon'] = 'thread';									//note feed 类型
////				$feed['title_template'] = 'feed_thread_title'; //note 标题模板
////				$feed['body_template'] = 'feed_thread_message';					//note 内容模板
////				$feed['body_data'] = array(
////					'subject' => "<a href=\"forum.php?mod=viewthread&tid=$tid\">$subject</a>",
////					'message' => messagecutstr($message, 150)		//note 取内容摘要，最好别超过150个字符
////				);
////				if(!empty($_G['forum_attachexist'])) {
//////					$firstaid = DB::result_first("SELECT aid FROM ".DB::table(getattachtablebytid($tid))." WHERE pid='$pid' AND dateline>'0' AND isimage='1' ORDER BY dateline LIMIT 1");
////					$imgattach = C::t('forum_attachment_n')->fetch_max_image('tid:'.$tid, 'pid', $pid);
////					$firstaid = $imgattach['aid'];
////					unset($imgattach);
////					if($firstaid) {
////						$feed['images'] = array(getforumimg($firstaid));
////						$feed['image_links'] = array("forum.php?mod=viewthread&do=tradeinfo&tid=$tid&pid=$pid");
////					}
////				}
//			} elseif($special > 0) {
//				if($special == 1) {
////					$pvs = explode("\t", messagecutstr($polloptionpreview, 150));
////					$s = '';
////					$i = 1;
////					foreach($pvs as $pv) {
////						$s .= $i.'. '.$pv.'<br />';
////					}
////					$s .= '&nbsp;&nbsp;&nbsp;...';
////					$feed['icon'] = 'poll';
////					$feed['title_template'] = 'feed_thread_poll_title';
////					$feed['body_template'] = 'feed_thread_poll_message';
////					$feed['body_data'] = array(
////						'subject' => "<a href=\"forum.php?mod=viewthread&tid=$tid\">$subject</a>",
////						'message' => $s
////					);
//				} elseif($special == 3) {
////					$feed['icon'] = 'reward';
////					$feed['title_template'] = 'feed_thread_reward_title';
////					$feed['body_template'] = 'feed_thread_reward_message';
////					$feed['body_data'] = array(
////						'subject'=> "<a href=\"forum.php?mod=viewthread&tid=$tid\">$subject</a>",
////						'rewardprice'=> $rewardprice,
////						'extcredits' => $_G['setting']['extcredits'][$_G['setting']['creditstransextra'][2]]['title'],
////					);
//				} elseif($special == 4) {
////					$feed['icon'] = 'activity';
////					$feed['title_template'] = 'feed_thread_activity_title';
////					$feed['body_template'] = 'feed_thread_activity_message';
////					$feed['body_data'] = array(
////						'subject' => "<a href=\"forum.php?mod=viewthread&tid=$tid\">$subject</a>",
////						'starttimefrom' => $_GET['starttimefrom'][$activitytime],
////						'activityplace'=> $activity['place'],
////						'message' => messagecutstr($message, 150),
////					);
////					if($_GET['activityaid']) {
////						$feed['images'] = array(getforumimg($_GET['activityaid']));
////						$feed['image_links'] = array("forum.php?mod=viewthread&do=tradeinfo&tid=$tid&pid=$pid");
////					}
//				} elseif($special == 5) {
////					$feed['icon'] = 'debate';
////					$feed['title_template'] = 'feed_thread_debate_title';
////					$feed['body_template'] = 'feed_thread_debate_message';
////					$feed['body_data'] = array(
////						'subject' => "<a href=\"forum.php?mod=viewthread&tid=$tid\">$subject</a>",
////						'message' => messagecutstr($message, 150),
////						'affirmpoint'=> messagecutstr($affirmpoint, 150),
////						'negapoint'=> messagecutstr($negapoint, 150)
////					);
//				}
//			}
//
//			$feed['title_data']['hash_data'] = "tid{$tid}";
//			$feed['id'] = $tid;
//			$feed['idtype'] = 'tid';
//			if($feed['icon']) {
//				postfeed($feed);
//			}
//		}

//		//note 更新论坛信息
//		if($displayorder != -4) {
//			//note 精华
//			if($digest) {
//				updatepostcredits('+',  $_G['uid'], 'digest', $_G['fid']);
//			}
//			//note 加积分
//			updatepostcredits('+',  $_G['uid'], 'post', $_G['fid']);
//			if($isgroup) { //note 给群组用户累记发帖数
//				//DB::query("UPDATE ".DB::table('forum_groupuser')." SET threads=threads+1, lastupdate='".TIMESTAMP."' WHERE uid='$_G[uid]' AND fid='$_G[fid]'");
//				C::t('forum_groupuser')->update_counter_for_user($_G['uid'], $_G['fid'], 1);
//			}
//
//			$subject = str_replace("\t", ' ', $subject);
//			$lastpost = "$tid\t".$subject."\t$_G[timestamp]\t$author";
//			//DB::query("UPDATE ".DB::table('forum_forum')." SET lastpost='$lastpost', threads=threads+1, posts=posts+1, todayposts=todayposts+1 WHERE fid='$_G[fid]'", 'UNBUFFERED');
//			C::t('forum_forum')->update($_G['fid'], array('lastpost' => $lastpost));
//			C::t('forum_forum')->update_forum_counter($_G['fid'], 1, 1, 1);
//			//note 如果为子论坛, 更新一级论坛的信息
//			if($_G['forum']['type'] == 'sub') {
//				//DB::query("UPDATE ".DB::table('forum_forum')." SET lastpost='$lastpost' WHERE fid='".$_G['forum'][fup]."'", 'UNBUFFERED');
//				C::t('forum_forum')->update($_G['forum']['fup'], array('lastpost' => $lastpost));
//			}
//		}
//
//		if($_G['forum']['status'] == 3) {
//			C::t('forum_forumfield')->update($_G['fid'], array('lastupdate' => TIMESTAMP));
//			require_once libfile('function/grouplog');
//			updategroupcreditlog($_G['fid'], $_G['uid']);
//		}
//
//		showmessage('post_newthread_succeed', $returnurl, $values, $param);
//
//	}
}


?>

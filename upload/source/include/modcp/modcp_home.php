<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: modcp_home.php 25246 2011-11-02 03:34:53Z zhangguosheng $
 */

if(!defined('IN_DISCUZ') || !defined('IN_MODCP')) {
	exit('Access Denied');
}

//note modcp's home

//note ���ӹ�������
if($op == 'addnote' && submitcheck('submit')) {
	$newaccess = 4 + ($_GET['newaccess'][2] << 1) + $_GET['newaccess'][3];
	$newexpiration = TIMESTAMP + (intval($_GET['newexpiration']) > 0 ? intval($_GET['newexpiration']) : 30) * 86400;
	$newmessage = nl2br(dhtmlspecialchars(trim($_GET['newmessage'])));
	if($newmessage != '') {
		//DB::query("INSERT INTO ".DB::table('common_adminnote')." (admin, access, adminid, dateline, expiration, message)
		//	VALUES ('$_G[username]', '$newaccess', '$_G[adminid]', '$_G[timestamp]', '$newexpiration', '$newmessage')");
		C::t('common_adminnote')->insert(array(
			'admin' => $_G['username'],
			'access' => $newaccess,
			'adminid' => $_G['adminid'],
			'dateline' => $_G['timestamp'],
			'expiration' => $newexpiration,
			'message' => $newmessage,
		));
	}
}

//note ɾ����������
if($op == 'delete' && submitcheck('notlistsubmit')) {
	if(is_array($_GET['delete']) && $deleteids = dimplode($_GET['delete'])) {
		//DB::query("DELETE FROM ".DB::table('common_adminnote')." WHERE id IN($deleteids) AND ('$_G[adminid]'=1 OR admin='$_G[username]')");
		C::t('common_adminnote')->delete($_GET['delete'], ($_G['adminid'] == 1 ? '' : $_G['username']));
	}
}

//note ���������б�
switch($_G['adminid']) {
	case 1: $access = '1,2,3,4,5,6,7'; break;
	case 2: $access = '2,3,6,7'; break;
	default: $access = '1,3,5,7'; break;
}

$notelist = array();
//$query = DB::query("SELECT * FROM ".DB::table('common_adminnote')." WHERE access IN ($access) ORDER BY dateline DESC");
foreach(C::t('common_adminnote')->fetch_all_by_access(explode(',', $access)) as $note) {
	if($note['expiration'] < TIMESTAMP) {
		//DB::query("DELETE FROM ".DB::table('common_adminnote')." WHERE id='$note[id]'");
		C::t('common_adminnote')->delete($note['id']);
	} else {
		$note['expiration'] = ceil(($note['expiration'] - $note['dateline']) / 86400);
		$note['dateline'] = dgmdate($note['dateline']);
		$note['checkbox'] = '<input type="checkbox" name="delete[]" class="pc" '.($note['admin'] == $_G['member']['username'] || $_G['adminid'] == 1 ? "value=\"$note[id]\"" : 'disabled').'>';
		$note['admin'] = '<a href="home.php?mod=space&username='.rawurlencode($note['admin']).'" target="_blank">'.$note['admin'].'</a>';
		$notelist[] = $note;
	}
}

?>
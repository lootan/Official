<?php
/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: portalcp_portalblock.php 31958 2012-10-26 05:11:05Z zhangguosheng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

require_once libfile('function/block');
$op = in_array($_GET['op'], array('recommend', 'getblocklist', 'verifydata', 'verifieddata')) ? $_GET['op'] : 'getblocklist';
$initemdata = $op === 'verifydata' || $op === 'verifieddata' ? true : false; //在模块数据页面中
$_GET['idtype'] = dhtmlspecialchars($_GET['idtype']);
$_GET['id'] = intval($_GET['id']);

$allowdiy = checkperm('allowdiy');
if(!$allowdiy && !$admincp4 && !$admincp5 && !$admincp6) {
	showmessage('portal_nopermission', dreferer());
}
//可操作的页面
loadcache('diytemplatename');
$pagebids = $tpls = $blocks = $tplpermissions = $wherearr = $blockfavorite = $topblocks = $blockdata = array();

if(submitcheck('getblocklistsubmit') || submitcheck('verifieddatasubmit') || submitcheck('verifydatasubmit')) {

	if($allowdiy) {
		$tpls = array_keys($_G['cache']['diytemplatename']);
	} else {
		$permissions = getallowdiytemplate($_G['uid']);
		foreach($permissions as $value) {
			// 有管理权限 或者 有推送权限且不需要审核 或者 是推送的操作时只需要推荐权限，否则需要管理权限
			if($value['allowmanage'] || ($value['allowrecommend'] && empty($value['needverify']))) {
				$tpls[] = $value['targettplname'];
			}
		}
	}
	//非DIY权限的查找可以管理的模块
	if(!$allowdiy) {
		//$query = DB::query('SELECT bid FROM '.DB::table('common_block_permission')." WHERE uid='$_G[uid]' AND (allowmanage='1' OR (allowrecommend='1' AND needverify='0'))");
		//while(($value=DB::fetch($query))) {
		foreach(C::t('common_block_permission')->fetch_all_by_uid($_G['uid']) as $bid => $value) {
			if($value['allowmanage'] == 1 || ($value['allowrecommend'] == 1 && $value['needverify'] == 0)) {
				$bids[$value['bid']] = intval($value['bid']);
			}
		}
	}

	if(!$allowdiy && empty($bids)) {
		showmessage('portal_nopermission', dreferer());
	}

	if(submitcheck('getblocklistsubmit')) {

		$updatebids = $_GET['bids'];
		$updatebids = array_map('intval', $updatebids);
		$updatebids = array_filter($updatebids);
		$updatebids = !$allowdiy ? array_intersect($bids, $updatebids) : $updatebids;
		if($updatebids) {
			//DB::query('UPDATE '.DB::table('common_block').' SET `dateline`='.TIMESTAMP.'-cachetime WHERE bid IN ('.dimplode($updatebids).')');
			C::t('common_block')->update_dateline_to_expired($updatebids, TIMESTAMP);
		}
		showmessage('portalcp_block_push_the_update_line', dreferer());

	} else if (submitcheck('verifydatasubmit')) {

		if(!in_array($_POST['optype'], array('pass', 'delete'))) {
			showmessage('select_a_option', dreferer());
		}
		$ids = $updatebids = array();
		if($_POST['ids']) {
			//$query = DB::query('SELECT dataid, bid FROM '.DB::table('common_block_item_data')." WHERE dataid IN (".dimplode($_POST['ids']).')');
			//while(($value=DB::fetch($query))) {
			foreach(C::t('common_block_item_data')->fetch_all($_POST['ids']) as $value) {
				if($allowdiy || in_array($value['bid'], $bids)) {
					$ids[$value['dataid']] = intval($value['dataid']);
					$updatebids[$value['bid']] = $value['bid'];
				}
			}
		}
		if(empty($ids)) {
			showmessage('select_a_moderate_data', dreferer());
		}

		if($_POST['optype']=='pass') {
			//DB::query('UPDATE '.DB::table('common_block_item_data')." SET isverified='1', verifiedtime='$_G[timestamp]' WHERE dataid IN (".dimplode($ids).")");
			C::t('common_block_item_data')->update($ids, array('isverified' => '1', 'verifiedtime' => $_G['timestamp']));
			if($updatebids) {
				//DB::query('UPDATE '.DB::table('common_block').' SET dateline=dateline-cachetime-1000 WHERE bid IN ('.dimplode($updatebids).') AND cachetime>0');
				C::t('common_block')->update_dateline_to_expired($updatebids, TIMESTAMP);
			}
		} elseif($_POST['optype']=='delete') {
			//DB::query('DELETE FROM '.DB::table('common_block_item_data')." WHERE dataid IN (".dimplode($ids).")");
			C::t('common_block_item_data')->delete($ids);
		}
		showmessage('operation_done', dreferer());

	} else if (submitcheck('verifieddatasubmit')) {

		$ids = array();
		if(!empty($_POST['ids'])) {
			//$dataids = dimplode($_POST['ids']);
			//$query = DB::query('SELECT dataid, bid FROM '.DB::table('common_block_item_data')." WHERE dataid IN ($dataids)");
			//while(($value=DB::fetch($query))) {
			foreach(C::t('common_block_item_data')->fetch_all($_POST['ids']) as $value) {
				if($allowdiy || in_array($value['bid'], $bids)) {
					$ids[$value['dataid']] = intval($value['dataid']);
				}
			}
		}
		if($ids) {
			//DB::query('DELETE FROM '.DB::table('common_block_item_data')." WHERE dataid IN (".dimplode($ids).")");
			C::t('common_block_item_data')->delete($ids);
		}

		//推送数据所排列
		$displayorder = array_map('intval', $_POST['displayorder']);
		foreach($displayorder  as $dataid => $displayorder) {
			if($displayorder !== intval($_POST['olddisplayorder'][$dataid])) {
				C::t('common_block_item_data')->update($dataid, array('displayorder' => $displayorder));
			}
		}
		showmessage('do_success', dreferer());
	}
} else {

	//列表的处理
	$perpage = $op == 'recommend' ? 16 : 30;
	$page = max(1,intval($_GET['page']));
	$start = ($page-1)*$perpage;
	if($start<0) $start = 0;
	$theurl = 'portal.php?mod=portalcp&ac=portalblock&op='.$op.'&idtype='.$_GET['idtype'].'&id='.$_GET['id'];
	$showfavorite = $page == 1 ? true : false;

	$multi = $fields = $leftjoin = '';
	$blockfavorite = block_get_favorite($_G['uid']);
	//允许DIY即可管理所有页面
	if($allowdiy) {
		$tpls = $_G['cache']['diytemplatename'];
	} else {
		//当前用户可管理的页面
		$tplpermissions = getallowdiytemplate($_G['uid']);
		foreach($tplpermissions as $value) {
			// 有管理权限 或者 有推送权限且不需要审核 或者 是推送的操作时只需要推送权限，否则需要管理权限
			if($value['allowmanage'] || ($value['allowrecommend'] && empty($value['needverify'])) || ($op=='recommend' && $value['allowrecommend'])) {
				$tpls[$value['targettplname']] = isset($_G['cache']['diytemplatename'][$value['targettplname']]) ? $_G['cache']['diytemplatename'][$value['targettplname']] : $value['targettplname'];
			}
		}
		//权限字段
		$fields = ',bp.allowmanage,bp.allowrecommend,bp.needverify';
		//关联表
		$leftjoin = ' LEFT JOIN '.DB::table('common_block_permission').' bp ON b.bid=bp.bid';
		//当前用户条件
		$wherearr[] = "bp.uid='$_G[uid]'";
		$wherearr[] = "(bp.allowmanage='1' OR (bp.allowrecommend='1'".($op == 'recommend' ? '' : "AND bp.needverify='0'")."))";
	}

	//已经推送过的模块
	$hasinblocks = array();
	if($op == 'recommend' && in_array($_GET['idtype'], array('tid', 'gtid', 'blogid', 'picid', 'aid'), true) && ($_GET['id'] = dintval($_GET['id']))) {
		//$hasinblocks = DB::fetch_all('SELECT * FROM %t WHERE id=%d AND idtype=%s', array('common_block_item_data', $_GET['id'], $_GET['idtype']), 'bid');
		//$where = $wherearr ? ' AND '.implode(' AND ', $wherearr) : '';
	//		debug("SELECT bi.dataid,bi.uid,bi.username,bi.dateline,bi.isverified,bi.verifiedtime,b.bid,b.blockclass,b.name,b.script$fields FROM ".DB::table('common_block_item_data').' bi LEFT JOIN '.DB::table('common_block')." b ON bi.bid=b.bid $leftjoin
	//		WHERE bi.id='{$_GET['id']}' AND bi.idtype='{$_GET['idtype']}' $where ORDER BY b.bid DESC");
		//$hasinblocks = DB::fetch_all("SELECT b.bid,b.blockclass,b.name,b.script$fields FROM ".DB::table('common_block').' b
		//	LEFT JOIN '.DB::table('common_block_item_data')." bi ON b.bid=bi.bid $leftjoin WHERE bi.id='{$_GET['id']}' AND bi.idtype='{$_GET['idtype']}'$where ORDER BY b.bid DESC", null, 'bid');
		$hasinblocks = C::t('common_block')->fetch_all_recommended_block($_GET['id'], $_GET['idtype'], $wherearr, $leftjoin, $fields);
	}

	//搜索
	if($_GET['searchkey']) {
		$_GET['searchkey'] = trim($_GET['searchkey']);
		$showfavorite = false;
		if (preg_match('/^[#]?(\d+)$/', $_GET['searchkey'],$match)) {
			$bid = intval($match[1]);
			$wherearr[] = " (b.bid='$bid' OR b.name='$bid')";
		} else {
			$wherearr[] = " b.name LIKE '%".stripsearchkey($_GET['searchkey'])."%'";
			$perpage = 10000;
		}
		$_GET['searchkey'] = dhtmlspecialchars($_GET['searchkey']);
		$theurl .= '&searchkey='.$_GET['searchkey'];
	}
	//按页面查看
	if($_GET['targettplname']) {
		$showfavorite = false;
		$targettplname = trim($_GET['targettplname']);
		/*$query = DB::query("SELECT * FROM ".DB::table('common_template_block')." WHERE targettplname='$targettplname'");
		while(($value = DB::fetch($query))){
			$pagebids[] = $value['bid'];
		}*/
		$pagebids = array_keys(C::t('common_template_block')->fetch_all_by_targettplname($targettplname));
		if(!empty($pagebids)) {
			$wherearr[] = "b.bid IN (".dimplode($pagebids).")";
			$perpage = 10000;
		} else {
			$wherearr[] = "b.bid='0'"; //要查看的页面没有模块
		}
		$_GET['targettplname'] = dhtmlspecialchars($_GET['targettplname']);
		$theurl .= '&targettplname='.$_GET['targettplname'];
	}

	if($op == 'recommend') {

		$rewhere = array();
		switch ($_GET['idtype']) {
			case 'tid' :
				$rewhere[] = "(blockclass='forum_thread' OR blockclass='forum_activity' OR blockclass='forum_trade')";
				break;
			case 'gtid' :
				$rewhere[] = "(blockclass='group_thread' OR blockclass='group_activity' OR blockclass='group_trade')";
				break;
			case 'blogid' :
				$rewhere[] = "blockclass ='space_blog'";
				break;
			case 'picid' :
				$rewhere[] = "blockclass ='space_pic'";
				break;
			case 'aid' :
				$rewhere[] = "blockclass ='portal_article'";
				break;
		}
		$wherearr = array_merge($rewhere, $wherearr);
		$where = $wherearr ? ' WHERE '.implode(' AND ', $wherearr) : '';

		//$count = DB::result_first("SELECT COUNT(*) FROM ".DB::table('common_block').' b'."$leftjoin$where");
		if(($count = C::t('common_block')->count_by_where($where, $leftjoin))) {
			//$query = DB::query("SELECT b.bid,b.blockclass,b.name,b.script,b.param$fields FROM ".DB::table('common_block').' b'." $leftjoin $where ORDER BY b.bid DESC LIMIT $start, $perpage");
			//while(($value = DB::fetch($query))) {
			foreach(C::t('common_block')->fetch_all_by_where($where, $start, $perpage, $leftjoin, $fields) as $value) {
				$value = formatblockvalue($value);
				if(!$value['favorite'] || !$showfavorite) {
					$blocks[$value['bid']] = $value;
				}
			}
			if(!empty($blockfavorite) && $showfavorite) {
				$blocks = $blockfavorite + $blocks;
			}
			$theurl = $_G['inajax'] ? $theurl.'&getdata=yes' : $theurl;
			if($_G['inajax']) $_GET['ajaxtarget'] = 'itemeditarea';
			$multi = multi($count, $perpage, $page, $theurl);
		}
	} else {
		$where = empty($wherearr) ? '' : ' WHERE '.implode(' AND ', $wherearr);
		//$count = DB::result_first("SELECT COUNT(*) FROM ".DB::table('common_block').' b'."$leftjoin$where ORDER BY b.bid DESC");
		if(($count = C::t('common_block')->count_by_where($where, $leftjoin))) {
			//$query = DB::query("SELECT b.bid,b.blockclass,b.name,b.script,b.dateline,b.cachetime,b.param$fields FROM ".DB::table('common_block').' b'
			//		."$leftjoin$where ORDER BY b.bid DESC ".($initemdata ? '' : DB::limit($start, $perpage)));
			//while(($value = DB::fetch($query))) {
			foreach(C::t('common_block')->fetch_all_by_where($where, $initemdata ? 0 : $start, $initemdata ? 0 : $perpage, $leftjoin, $fields) as $value) {
				$value = formatblockvalue($value);
				if(!$value['favorite'] || !$showfavorite) {
					$blocks[$value['bid']] = $value;
				}
			}
			if(!empty($blockfavorite) && $showfavorite) {
				$blocks = $blockfavorite + $blocks;
			}
			$multi = $initemdata ? '' : multi($count, $perpage, $page, $theurl);
		}
	}

	//模块列表
	if($blocks) {
		$losttpls = $alldata = array();
		$bids = array_keys($blocks);
		if($bids) {
			//$query = DB::query("SELECT targettplname, bid FROM ".DB::table('common_template_block')." WHERE bid IN (".dimplode($bids).")");
			//while(($value = DB::fetch($query))) {
			foreach(C::t('common_template_block')->fetch_all_by_bid($bids) as $value) {
				$alldata[] = $value;
				if(!isset($_G['cache']['diytemplatename'][$value['targettplname']])) {
					$losttpls[$value['targettplname']] = $value['targettplname'];
				}
			}

			if($losttpls) {
				$lostnames = getdiytplnames($losttpls);
				foreach($lostnames as $pre => $datas) {
					foreach($datas as $id => $name) {
						$_G['cache']['diytemplatename'][$pre.$id] = $tpls[$pre.$id] = $name;
					}
				}
			}

			foreach($alldata as $value) {
				//所在页面的处理
				$diyurl = block_getdiyurl($value['targettplname']);
				$diyurl = $diyurl['url'];
				$tplname = isset($_G['cache']['diytemplatename'][$value['targettplname']]) ? $_G['cache']['diytemplatename'][$value['targettplname']] : $value['targettplname'];
				if(!isset($tpls[$value['targettplname']])) {
					$tpls[$value['targettplname']] = $tplname;
				}
				$blocks[$value['bid']]['page'][$value['targettplname']] = $diyurl ? '<a href="'.$diyurl.'" target="_blank">'.$tplname.'</a>' : $tplname;
			}
		}
		//已审核数据和未审核数据
		if($initemdata) {
			$isverified = $op === 'verifieddata' ? 1 : 0;
			$count = C::t('common_block_item_data')->count_by_bid($bids, $isverified);
			$blockdata = $count ? C::t('common_block_item_data')->fetch_all_by_bid($bids, $isverified, $start, $perpage) : array();
			$multi = multi($count, $perpage, $page, $theurl);
		}
	}
}
include_once template("portal/portalcp_portalblock");

function formatblockvalue($value) {
	global $blockfavorite;
	//$value['param'] = is_string($value['param']) ? unserialize($value['param']) : $value['param'];
	$value['name'] = empty($value['name']) ? '<strong>#'.$value['bid'].'</strong>' : $value['name'];
	//if(isset($value['param']['moreurl'])) {
	//	$value['name'] = '<a href="portal.php?mod=block&bid='.$value['bid'].'" target="_blank">'.$value['name'].'</a>';
	//}
	$theclass = block_getclass($value['blockclass']);
	$value['blockclassname'] = $theclass['name'];
	$value['datasrc'] = $theclass['script'][$value['script']];
	$value['isrecommendable'] = block_isrecommendable($value);
	$value['perm'] = formatblockpermissoin($value);
	$value['favorite'] = isset($blockfavorite[$value['bid']]) ? true : false;
	return $value;
}
/**
 * 格式化模块的权限
 * @param array $block
 * @return array
 */
function formatblockpermissoin($block) {
	static $allowdiy = null;
	$allowdiy = isset($allowdiy) ? $allowdiy : checkperm('allowdiy');;
	$perm = array('allowproperty' => 0, 'allowdata'=> 0);
	$bid = !empty($block) ? $block['bid'] : 0;
	if(!empty($bid)) {
		//allowdiy为最大权限
		if($allowdiy) {
			$perm = array('allowproperty' => 1, 'allowdata'=> 1);
		} else {
			//判断权限分配情况
			if($block['allowmanage']) {
				$perm = array('allowproperty' => 1, 'allowdata'=> 1);
			}
			if ($block['allowrecommend'] && !$block['needverify']) {
				$perm['allowdata'] = 1;
			}
		}
	}
	return $perm;
}

/**
 * 得到用户收藏的模块
 * @param int $uid
 * @return array
 */
function block_get_favorite($uid){
	static $allowdiy = null;
	$allowdiy = isset($allowdiy) ? $allowdiy : checkperm('allowdiy');
	$blockfavorite = $permission = array();
	$uid = intval($uid);
	if($uid) {
		//$query = DB::query('SELECT bid FROM '.DB::table('common_block_favorite')." WHERE uid='$uid' ORDER BY dateline DESC");
		//while($value = DB::fetch($query)) {
		foreach(C::t('common_block_favorite')->fetch_all_by_uid($uid) as $value) {
			$blockfavorite[$value['bid']] = $value['bid'];
		}
	}
	//$blockfields = 'b.bid,b.blockclass,b.name,b.script,b.dateline,b.cachetime,b.param';
	if(!empty($blockfavorite)) {
		$blocks = C::t('common_block')->fetch_all($blockfavorite);
		if(!$allowdiy) {
			$permission = C::t('common_block_permission')->fetch_all_by_uid($uid);
		}
		/*if($allowdiy) {
			$query = DB::query("SELECT $blockfields FROM ".DB::table('common_block').' b'." WHERE b.bid IN (".dimplode($blockfavorite).")");
		} else {
			$query = DB::query("SELECT $blockfields,bp.allowmanage,bp.allowrecommend,bp.needverify FROM ".DB::table('common_block').' b'.' LEFT JOIN '.DB::table('common_block_permission').' bp ON b.bid=bp.bid'." WHERE bp.uid='$uid' AND b.bid IN (".dimplode($blockfavorite).")");
		}
		while(($value = DB::fetch($query))) {*/
		foreach($blocks as $bid => $value) {
			if(!$allowdiy && $permission[$bid]) {
				$value = array_merge($value, $permission[$bid]);
			}
			$value = formatblockvalue($value);
			$value['favorite'] = true;
			$blockfavorite[$value['bid']] = $value;
		}
		$blockfavorite = array_filter($blockfavorite, 'is_array');
	}
	return $blockfavorite;
}

?>
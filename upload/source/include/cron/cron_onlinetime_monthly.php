<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: cron_onlinetime_monthly.php 24402 2011-09-16 12:18:32Z zhengqingpeng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

//note 清理本月在线时间
//DB::query("UPDATE ".DB::table('common_onlinetime')." SET thismonth='0'");
C::t('common_onlinetime')->update_thismonth();

?>
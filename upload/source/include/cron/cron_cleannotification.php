<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: cron_cleannotification.php 24556 2011-09-26 06:16:03Z monkey $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

//清理通知
//$deltime = $_G['timestamp'] - 2*3600*24;//只保留2天
//$notifytime = $_G['timestamp'] - 30*3600*24;//只保留30天
//DB::query("DELETE FROM ".DB::table('home_notification')." WHERE new='0' AND dateline < '$deltime'");
//DB::query("DELETE FROM ".DB::table('home_notification')." WHERE new='1' AND dateline < '$notifytime'");
C::t('home_notification')->delete_clear(0, 2);
C::t('home_notification')->delete_clear(1, 30);

//清理招呼
$deltime = $_G['timestamp'] - 7*3600*24;//只保留7天
//DB::query("DELETE FROM ".DB::table('home_pokearchive')." WHERE dateline < '$deltime'");
C::t('home_pokearchive')->delete_by_dateline($deltime);

//优化表
//DB::query("OPTIMIZE TABLE ".DB::table('home_notification'), 'SILENT');
C::t('home_notification')->optimize();

?>
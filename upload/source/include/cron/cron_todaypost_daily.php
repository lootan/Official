<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: cron_todaypost_daily.php 31920 2012-10-24 09:18:33Z zhengqingpeng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

//$yesterdayposts = intval(DB::result_first("SELECT sum(todayposts) FROM ".DB::table('forum_forum').""));
$yesterdayposts = intval(C::t('forum_forum')->fetch_sum_todaypost());

C::t('forum_forum')->update_oldrank_and_yesterdayposts();

//$historypost = DB::result_first("SELECT svalue FROM ".DB::table('common_setting')." WHERE skey='historyposts'");
$historypost = C::t('common_setting')->fetch('historyposts');
$hpostarray = explode("\t", $historypost);
$_G['setting']['historyposts'] = $hpostarray[1] < $yesterdayposts ? "$yesterdayposts\t$yesterdayposts" : "$yesterdayposts\t$hpostarray[1]";

//DB::query("REPLACE INTO ".DB::table('common_setting')." (skey, svalue) VALUES ('historyposts', '".$_G['setting']['historyposts']."')");
C::t('common_setting')->update('historyposts', $_G['setting']['historyposts']);
$date = date('Y-m-d', TIMESTAMP - 86400);
//DB::query("REPLACE INTO ".DB::table('forum_statlog')."(logdate, fid, `type`, `value`)
//	SELECT '$date', fid, '1', todayposts FROM ".DB::table('forum_forum')." WHERE `type` IN ('forum', 'sub') AND `status`<>'3'");

C::t('forum_statlog')->insert_stat_log($date);
//DB::query("UPDATE ".DB::table('forum_forum')." SET todayposts='0'");
C::t('forum_forum')->clear_todayposts();
$rank = 1;
foreach(C::t('forum_statlog')->fetch_all_rank_by_logdate($date) as $value) {
	C::t('forum_forum')->update($value['fid'], array('rank' => $rank));
	$rank++;
}
//require_once libfile('function/cache');
//$_G['setting']['historyposts'] = $_G['setting']['historyposts'];
//updatesettings();
savecache('historyposts', $_G['setting']['historyposts']);

?>
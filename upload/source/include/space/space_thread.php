<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: space_thread.php 31365 2012-08-20 03:19:33Z zhengqingpeng $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

$minhot = $_G['setting']['feedhotmin']<1?3:$_G['setting']['feedhotmin'];
$page = empty($_GET['page'])?1:intval($_GET['page']);
if($page<1) $page=1;
$id = empty($_GET['id'])?0:intval($_GET['id']);
$opactives['thread'] = 'class="a"';

//默认显示
if(empty($_GET['view'])) $_GET['view'] = 'me';
$_GET['order'] = empty($_GET['order']) ? 'dateline' : $_GET['order'];

//判断是否有权限看其它人的贴子
$allowviewuserthread = $_G['setting']['allowviewuserthread'];

//分页
$perpage = 20;
$start = ($page-1)*$perpage;
ckstart($start, $perpage);

$list = array();
$userlist = array();
$hiddennum = $count = $pricount = 0;
$_GET['from'] = dhtmlspecialchars(preg_replace("/[^\[A-Za-z0-9_\]]/", '', $_GET['from']));
$gets = array(
	'mod' => 'space',
	'uid' => $space['uid'],
	'do' => 'thread',
	'fid' => $_GET['fid'],
	'view' => $_GET['view'],
	'type' => $_GET['type'],
	'order' => $_GET['order'],
	'fuid' => $_GET['fuid'],
	'searchkey' => $_GET['searchkey'],
	'from' => $_GET['from'],
	'filter' => $_GET['filter']
);
$theurl = 'home.php?'.url_implode($gets);
unset($gets['fid']);
$forumurl = 'home.php?'.url_implode($gets);
$multi = '';
$authorid = 0;
$replies = $closed = $displayorder = null;
$dglue = '=';
$vfid = $_GET['fid'] ? intval($_GET['fid']) : null;

require_once libfile('function/misc');
require_once libfile('function/forum');
loadcache(array('forums'));
$fids = $comma = '';
//$wheresql = $_GET['view'] == 'me' || !$allowviewuserthread ? '1' : " t.fid IN(".$allowviewuserthread.")";
//$wheresql .= $_GET['view'] != 'me' ? " AND t.displayorder>='0'" : '';
if($_GET['view'] != 'me') {
	$displayorder = 0;
	$dglue = '>=';
}
$f_index = '';
$ordersql = 't.dateline DESC';
$need_count = true;
$viewuserthread = false;
$listcount = 0;

if($_GET['view'] == 'me') {

	//自定义识别
	if($_GET['from'] == 'space') $diymode = 1;
	$allowview = true;
	$viewtype = in_array($_GET['type'], array('reply', 'thread', 'postcomment')) ? $_GET['type'] : 'thread';
	$filter = in_array($_GET['filter'], array('recyclebin', 'ignored', 'save', 'aduit', 'close', 'common')) ? $_GET['filter'] : '';
	if($space['uid'] != $_G['uid'] && in_array($viewtype, array('reply', 'thread'))) {
		if($allowviewuserthread === -1 && $_G['adminid'] != 1) {
			$allowview = false;
//			showmessage('ban_view_other_thead');
		}
		if($allowview) {
			//将原来在这里限制查看版块的放到循环中判断
			$viewuserthread = true;
			$viewfids = str_replace("'", '', $allowviewuserthread);
			if(!empty($viewfids)) {
				$viewfids = explode(',', $viewfids);
			}
		}
	}

	//查看个人的
	if($viewtype == 'thread' && $allowview) {
		$authorid = $space['uid'];
//		$closed = $displayorder = null;
//		$dglue = '=';

//		$statusfield = 'displayorder';
//		$wheresql .= " AND t.authorid = '$space[uid]'";



		if($filter == 'recyclebin') {
//			$wheresql .= " AND t.displayorder='-1'";
			$displayorder = -1;
		} elseif($filter == 'aduit') {
//			$wheresql .= " AND t.displayorder='-2'";
			$displayorder = -2;
		} elseif($filter == 'ignored') {
//			$wheresql .= " AND t.displayorder='-3'";
			$displayorder = -3;
		} elseif($filter == 'save') {
//			$wheresql .= " AND t.displayorder='-4'";
			$displayorder = -4;
		} elseif($filter == 'close') {
//			$wheresql .= " AND t.closed='1'";
			$closed = 1;
		} elseif($filter == 'common') {
//			$wheresql .= " AND t.displayorder>='0' AND t.closed='0'";
			$closed = 0;
			$displayorder = 0;
			$dglue = '>=';
		}

		$ordersql = 't.tid DESC';
	} elseif($viewtype == 'postcomment') {
		$posttable = getposttable();
		require_once libfile('function/post');
//		$query = DB::query("SELECT c.*, p.authorid, p.tid, p.pid, p.fid, p.invisible, p.dateline, p.message, t.special, t.status, t.subject, t.digest,t.attachment, t.replies, t.views, t.lastposter, t.lastpost
//			FROM ".DB::table('forum_postcomment')." c
//			LEFT JOIN ".DB::table($posttable)." p ON p.pid = c.pid
//			LEFT JOIN ".DB::table('forum_thread')." t ON t.tid = c.tid
//			WHERE c.authorid = '$space[uid]' ORDER BY c.dateline DESC LIMIT $start, $perpage");
		$pids = $tids = array();
		$postcommentarr = C::t('forum_postcomment')->fetch_all_by_authorid($_G['uid'], $start, $perpage);
		foreach($postcommentarr as $value) {
			$pids[] = $value['pid'];
			$tids[] = $value['tid'];
		}
		$pids = C::t('forum_post')->fetch_all(0, $pids);
		$tids = C::t('forum_thread')->fetch_all($tids);

		$list = $fids = array();
		//while($value = DB::fetch($query)) {
		foreach($postcommentarr as $value) {
			$value['authorid'] = $pids[$value['pid']]['authorid'];
			$value['fid'] = $pids[$value['pid']]['fid'];
			$value['invisible'] = $pids[$value['pid']]['invisible'];
			$value['dateline'] = $pids[$value['pid']]['dateline'];
			$value['message'] = $pids[$value['pid']]['message'];
			$value['special'] = $tids[$value['tid']]['special'];
			$value['status'] = $tids[$value['tid']]['status'];
			$value['subject'] = $tids[$value['tid']]['subject'];
			$value['digest'] = $tids[$value['tid']]['digest'];
			$value['attachment'] = $tids[$value['tid']]['attachment'];
			$value['replies'] = $tids[$value['tid']]['replies'];
			$value['views'] = $tids[$value['tid']]['views'];
			$value['lastposter'] = $tids[$value['tid']]['lastposter'];
			$value['lastpost'] = $tids[$value['tid']]['lastpost'];
			$value['tid'] = $pids[$value['pid']]['tid'];

			$fids[] = $value['fid'];
			$value['comment'] = messagecutstr($value['comment'], 100);
			$list[] = procthread($value);
		}
		unset($pids, $tids, $postcommentarr);
		if($fids) {
			$fids = array_unique($fids);
			//$query = DB::query("SELECT fid, name FROM ".DB::table('forum_forum')." WHERE fid IN (".dimplode($fids).")");
			$query = C::t('forum_forum')->fetch_all($fids);
			//while($forum = DB::fetch($query)) {
			foreach($query as $forum) {
				$forums[$forum['fid']] = $forum['name'];
			}
		}

		$multi = simplepage(count($list), $perpage, $page, $theurl);
		$need_count = false;

	} elseif($allowview) { //我的回复
//		$statusfield = 'invisible';
		$invisible = null;

		$postsql = $threadsql = '';
		if($filter == 'recyclebin') {
//			$postsql .= " AND p.invisible='-5'";
			$invisible = -5;
		} elseif($filter == 'aduit') {
//			$postsql .= " AND p.invisible='-2'";
			$invisible = -2;
		} elseif($filter == 'save' || $filter == 'ignored') {
//			$postsql .= " AND p.invisible='-3' AND t.displayorder='-4'";
			$invisible = -3;
			$displayorder = -4;
//		} elseif($filter == 'ignored') {
//			$postsql .= " AND p.invisible='-3' AND t.displayorder!='-4'";
		} elseif($filter == 'close') {
//			$threadsql .= " AND t.closed='1'";
			$closed = 1;
		} elseif($filter == 'common') {
//			$postsql .= " AND p.invisible='0'";
//			$threadsql .= " AND t.displayorder>='0' AND t.closed='0'";
			$invisible = 0;
			$displayorder = 0;
			$dglue = '>=';
			$closed = 0;
		} else {
			if($space['uid'] != $_G['uid']) {
				$invisible = 0;
			}
		}
//		if($space['uid'] != $_G['uid']) {
//			if($allowviewuserthread === -1 && $_G['adminid'] != 1) {
//				showmessage('ban_view_other_thead');
//			}
//			$threadsql .= empty($allowviewuserthread) ? '' : " AND t.fid IN($allowviewuserthread) ";
//		}
		require_once libfile('function/post');
		$posts = C::t('forum_post')->fetch_all_by_authorid(0, $space['uid'], true, 'DESC', $start, $perpage, 0, $invisible, $vfid);
		$listcount = count($posts);
		//过滤不能看的版块
		foreach($posts as $pid => $post) {
			$delrow = false;
			if($post['anonymous'] && $post['authorid'] != $_G['uid']) {
				$delrow = true;
			} elseif($viewuserthread && $post['authorid'] != $_G['uid']) {	//判断是否是查看别人的帖子
				if(($_G['adminid'] != 1 && !empty($viewfids) && !in_array($post['fid'], $viewfids))) {
					$delrow = true;
				}
			}
			if($delrow) {
				unset($posts[$pid]);
				$hiddennum++;
				continue;
			} else {
				$tids[$post['tid']][] = $pid;
				$post['message'] = !getstatus($post['status'], 2) || $post['authorid'] == $_G['uid'] ? messagecutstr($post['message'], 100) : '';
				$posts[$pid] = $post;
			}
		}

		//取出相应的主题
		if(!empty($tids)) {

			$threads = C::t('forum_thread')->fetch_all_by_tid_displayorder(array_keys($tids), $displayorder, $dglue, array(), $closed);

			foreach($threads as $tid => $thread) {
				$delrow = false;
				if($_G['adminid'] != 1 && $thread['displayorder'] < 0) {	//判断是否是查看别人的帖子
					$delrow = true;
				} elseif($_G['adminid'] != 1 && $_G['uid'] != $thread['authorid'] && getstatus($thread['status'], 2)) {
					$delrow = true;
				} elseif(!isset($_G['cache']['forums'][$thread['fid']])) {
					if(!$_G['setting']['groupstatus']) {
						$delrow = true;
					} else {
						$gids[$thread['fid']] = $thread['tid'];
					}
				}
				if($delrow) {
					//释放部份post
					foreach($tids[$tid] as $pid) {
						unset($posts[$pid]);
						$hiddennum++;
					}
					unset($tids[$tid]);
					unset($threads[$tid]);
					continue;
				} else {
					$threads[$tid] = procthread($thread);
					$forums[$thread['fid']] = $threads[$tid]['forumname'];
				}

			}
			//重新获取群组的名称
			if(!empty($gids)) {
				$groupforums = C::t('forum_forum')->fetch_all_name_by_fid(array_keys($gids));
				foreach($gids as $fid => $tid) {
					$threads[$tid]['forumname'] = $groupforums[$fid]['name'];
					$forums[$fid] = $groupforums[$fid]['name'];
				}
			}
			if(!empty($tids)) {
				//过滤没取到主题的回复
				foreach($tids as $tid => $pids) {
					foreach($pids as $pid) {
						if(!isset($threads[$tid])) {
							unset($posts[$pid]);
							unset($tids[$tid]);
							$hiddennum++;
							continue;
						}
//						$posts[$pid] = array_merge($posts[$pid], $threads[$tid]);
					}
//					unset($tids[$tid]);
//					unset($threads[$tid]);
				}
			}
			$list = &$threads;
		}

//		$postsql .= " AND p.first='0'";
//		$posttable = getposttable();
//
//		require_once libfile('function/post');
//		$query = DB::query("SELECT p.authorid, p.tid, p.pid, p.fid, p.invisible, p.dateline, p.message, t.special, t.status, t.subject, t.digest,t.attachment, t.replies, t.views, t.lastposter, t.lastpost, t.displayorder FROM ".DB::table($posttable)." p
//		INNER JOIN ".DB::table('forum_thread')." t ON t.tid=p.tid $threadsql
//		WHERE p.authorid='$space[uid]' $postsql ORDER BY p.dateline DESC LIMIT $start,$perpage");
//
//		$list = $fids = array();
//		while($value = DB::fetch($query)) {
//			$fids[] = $value['fid'];
//			$value['message'] = !getstatus($value['status'], 2) || $value['authorid'] == $_G['uid'] ? messagecutstr($value['message'], 100) : '';
//			$list[] = procthread($value) ;
//			$tids[$value['tid']] = $value['tid'];
//		}
//		if($fids) {
//			$fids = array_unique($fids);
//			$query = DB::query("SELECT fid, name, status FROM ".DB::table('forum_forum')." WHERE fid IN (".dimplode($fids).")");
//			while($forum = DB::fetch($query)) {
//				if(!$_G['setting']['groupstatus'] && $forum['status'] == 3) {
//					//此处为特殊条件，关闭群组并且是群组版块，什么都不做
//				} else {
//					$forums[$forum['fid']] = $forum['name'];
//				}
//			}
//			foreach($list as $key => $val) {
//				if(!$forums[$val['fid']]) {
//					unset($list[$key]);
//				}
//			}
//		}

		$multi = simplepage($listcount, $perpage, $page, $theurl);

		$need_count = false;
	}
	if(!$allowview) {
		$need_count = false;
	}
	$orderactives = array($viewtype => ' class="a"');

} else {

	space_merge($space, 'field_home');

	if($space['feedfriend']) {

		$fuid_actives = array();

		//查看指定好友的
		require_once libfile('function/friend');
		$fuid = intval($_GET['fuid']);
		if($fuid && friend_check($fuid, $space['uid'])) {
//			$wheresql .= " AND t.authorid='$fuid'";
			$authorid = $fuid;
			$fuid_actives = array($fuid=>' selected');
		} else {
//			$wheresql .= " AND t.authorid IN ($space[feedfriend])";
			$authorid = explode(',', $space['feedfriend']);
//			$theurl = "home.php?mod=space&uid=$space[uid]&do=$do&view=we";
		}

		//好友列表
		//$query = DB::query("SELECT * FROM ".DB::table('home_friend')." WHERE uid='$_G[uid]' ORDER BY num DESC LIMIT 0,100");
		$query = C::t('home_friend')->fetch_all_by_uid($_G['uid'], 0, 100, true);
		//while ($value = DB::fetch($query)) {
		foreach($query as $value) {
			$userlist[] = $value;
		}
	} else {
		$need_count = false;
	}
}

$actives = array($_GET['view'] =>' class="a"');

if($need_count) {

	//搜索
	if($searchkey = stripsearchkey($_GET['searchkey'])) {
//		$wheresql .= " AND t.subject LIKE '%$searchkey%'";
		$searchkey = dhtmlspecialchars($searchkey);
	}


	loadcache('forums');
	$gids = $fids = $forums = array();

//		$threads = C::t('forum_thread')->fetch_all_by_authorid_displayorder($authorid, $displayorder, $dglue, $closed, $searchkey, $start, $perpage, $replies);
//		$query = DB::query("SELECT t.* FROM ".DB::table('forum_thread')." t WHERE $wheresql ORDER BY $ordersql LIMIT $start,$perpage");
//		while($value = DB::fetch($query)) {
	foreach(C::t('forum_thread')->fetch_all_by_authorid_displayorder($authorid, $displayorder, $dglue, $closed, $searchkey, $start, $perpage, $replies, $vfid) as $tid => $value) {
		if(empty($value['author']) && $value['authorid'] != $_G['uid']) {
			$hiddennum++;
			continue;
		} elseif($viewuserthread && $value['authorid'] != $_G['uid']) {
			//判断是否是查看别人的帖子
			if(($_G['adminid'] != 1 && !empty($viewfids) && !in_array($value['fid'], $viewfids)) || $value['displayorder'] < 0) {
				$hiddennum++;
				continue;
			}
		} elseif(!isset($_G['cache']['forums'][$value['fid']])) {
			if(!$_G['setting']['groupstatus']) {
				$hiddennum++;
				continue;
			} else {
				$gids[$value['fid']] = $value['tid'];
			}
		}
//
//			$fids[] = $value['fid'];
		$list[$value['tid']] = procthread($value);
		$forums[$value['fid']] = $list[$value['tid']]['forumname'];
	}

	if(!empty($gids)) {
		$gforumnames = C::t('forum_forum')->fetch_all_name_by_fid(array_keys($gids));
		foreach($gids as $fid => $tid) {
			$list[$tid]['forumname'] = $gforumnames[$fid]['name'];
			$forums[$fid] = $gforumnames[$fid]['name'];
		}
	}
//		if($fids) {
//			$fids = array_unique($fids);
//			$query = DB::query("SELECT fid, name, status FROM ".DB::table('forum_forum')." WHERE fid IN (".dimplode($fids).")");
//			while($forum = DB::fetch($query)) {
//				if(!$_G['setting']['groupstatus'] && $forum['status'] == 3) {
//					//此处为特殊条件，关闭群组并且是群组版块，什么都不做
//				} else {
//					$forums[$forum['fid']] = $forum['name'];
//				}
//			}
//		}
//		foreach($list as $key => $val) {
//			if(!$forums[$val['fid']] || $val['closed'] > 1) {
//				unset($list[$key]);
//				$hiddennum++;
//			}
//		}

	$threads = &$list;


	//分页
	if($_GET['view'] != 'all') {
		$listcount = count($list)+$hiddennum;
		$multi = simplepage($listcount, $perpage, $page, $theurl);
	}
}

require_once libfile('function/forumlist');
$forumlist = forumselect(FALSE, 0, intval($_GET['fid']));
dsetcookie('home_diymode', $diymode);

if($_G['uid']) {
	$_GET['view'] = !$_GET['view'] ? 'we' : $_GET['view'];
	$navtitle = lang('core', 'title_'.$_GET['view'].'_thread');
} else {
	$navtitle = lang('core', 'title_thread');
}

if($space['username']) {
	$navtitle = lang('space', 'sb_thread', array('who' => $space['username']));
}
$metakeywords = $navtitle;
$metadescription = $navtitle;
if(!$_G['follow']) {
	include_once template("diy:home/space_thread");
}
?>
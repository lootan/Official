<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: spacecp_poke.php 34369 2014-04-01 02:00:04Z jeffjzhang $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

$uid = empty($_GET['uid'])?0:intval($_GET['uid']);

if($uid == $_G['uid']) {
	showmessage('not_to_their_own_greeted');
}

if($op == 'send' || $op == 'reply') {

	if(!checkperm('allowpoke')) {
		showmessage('no_privilege_poke');
	}

	//新用户见习
	cknewuser();

	$tospace = array();

	//获取对象
	if($uid) {
		$tospace = getuserbyuid($uid);
	} elseif ($_POST['username']) {
		//$tospace = DB::fetch_first("SELECT uid FROM ".DB::table('common_member')." WHERE username='$_POST[username]' LIMIT 1");
		$tospace = C::t('common_member')->fetch_uid_by_username($_POST['username']);
	}

	//黑名单
	if($tospace && isblacklist($tospace['uid'])) {
		showmessage('is_blacklist');
	}

	//打招呼
	if(submitcheck('pokesubmit')) {
		if(empty($tospace)) {
			showmessage('space_does_not_exist');
		}

		$notetext = censor(htmlspecialchars(cutstr($_POST['note'], strtolower(CHARSET) == 'utf-8' ? 30 : 20, '')));
		//存档
		$setarr = array(
			'pokeuid' => $uid+$_G['uid'],
			'uid' => $uid,
			'fromuid' => $_G['uid'],
			'note' => $notetext, //need to do
			'dateline' => $_G['timestamp'],
			'iconid' => intval($_POST['iconid'])
		);
//		DB::insert('home_pokearchive', $setarr);
		C::t('home_pokearchive')->insert($setarr);

		//主表
		$setarr = array(
			'uid' => $uid,
			'fromuid' => $_G['uid'],
			'fromusername' => $_G['username'],
			'note' => $notetext,
			'dateline' => $_G['timestamp'],
			'iconid' => intval($_POST['iconid'])
		);

		//主表
//		DB::insert('home_poke', $setarr, 0, true);
		C::t('home_poke')->insert($setarr, false, true);

		//更新我的好友关系热度
		require_once libfile('function/friend');
		friend_addnum($tospace['uid']);

		if($op == 'reply') {
			//删除招呼
//			DB::query("DELETE FROM ".DB::table('home_poke')." WHERE uid='$_G[uid]' AND fromuid='$uid'");
			C::t('home_poke')->delete_by_uid_fromuid($_G['uid'], $uid);
			//DB::query("UPDATE ".DB::table('common_member')." SET newprompt=newprompt-'1' WHERE uid='$_G[uid]'");
			C::t('common_member')->increase($_G['uid'], array('newprompt' => -1));
		}
		//奖励
		updatecreditbyaction('poke', 0, array(), $uid);

		if($setarr['iconid']) {
			require_once libfile('function/spacecp');
			$pokemsg = makepokeaction($setarr['iconid']);
		} else {
			$pokemsg = lang('home/template', 'say_hi');
		}
		if(!empty($setarr['note'])) {
			$pokemsg .= ', '.lang('home/template', 'say').':'.$setarr['note'];
		}

		//发送提醒
		$note = array(
				'fromurl' => 'home.php?mod=space&uid='.$_G['uid'],
				'fromusername' => $_G['username'],
				'fromuid' => $_G['uid'],
				'from_id' => $_G['uid'],
				'from_idtype' => 'pokequery',
				'pokemsg' => $pokemsg
			);
		notification_add($uid, 'poke', 'poke_request', $note);

		//统计
		include_once libfile('function/stat');
		updatestat('poke');

		showmessage('poke_success', dreferer(), array('username' => $tospace['username'], 'uid' => $uid, 'from' => $_GET['from']), array('showdialog'=>1, 'showmsg' => true, 'closetime' => true));

	}

} elseif($op == 'ignore') {
	if(submitcheck('ignoresubmit')) {
		$where = empty($uid)?'':"AND fromuid='$uid'";
//		DB::query("DELETE FROM ".DB::table('home_poke')." WHERE uid='$_G[uid]' $where");
		C::t('home_poke')->delete_by_uid_fromuid($_G['uid'], $uid);

		C::t('home_notification')->delete_by_uid_type_authorid($_G['uid'], 'poke', $uid);

		showmessage('has_been_hailed_overlooked', '', array('uid' => $uid, 'from' => $_GET['from']), array('showdialog'=>1, 'showmsg' => true, 'closetime' => true, 'alert' => 'right'));
	}

} elseif($op == 'view') {

	$_GET['uid'] = intval($_GET['uid']);

	$list = array();
//	$query = DB::query("SELECT * FROM ".DB::table('home_poke')." WHERE uid='$space[uid]' AND fromuid='$_GET[uid]'");
//	if($value = DB::fetch($query)) {
	foreach(C::t('home_poke')->fetch_all_by_uid_fromuid($space['uid'], $_GET['uid']) as $value) {
		$pokeuid = $value['uid']+$value['fromuid'];

		$value['uid'] = $value['fromuid'];
		$value['username'] = $value['fromusername'];

		require_once libfile('function/friend');
		$value['isfriend'] = $value['uid']==$space['uid'] || friend_check($value['uid']) ? 1 : 0;

//		$subquery = DB::query("SELECT * FROM ".DB::table('home_pokearchive')." WHERE pokeuid='$pokeuid' ORDER BY dateline");
//		while ($subvalue = DB::fetch($subquery)) {
		foreach(C::t('home_pokearchive')->fetch_all_by_pokeuid($pokeuid) as $subvalue) {
			$list[$subvalue['pid']] = $subvalue;
		}

	}

} else {

	$perpage = 20;
	$perpage = mob_perpage($perpage);

	$page = empty($_GET['page'])?0:intval($_GET['page']);
	if($page<1) $page = 1;
	$start = ($page-1)*$perpage;
	//检查开始数
	ckstart($start, $perpage);

	//打招呼
	$fuids = $list = array();
//	$count = DB::result(DB::query("SELECT COUNT(*) FROM ".DB::table('home_poke')." WHERE uid='$space[uid]'"), 0);
	$count = C::t('home_poke')->count_by_uid($space['uid']);
	if($count) {
//		$query = DB::query("SELECT * FROM ".DB::table('home_poke')." WHERE uid='$space[uid]' ORDER BY dateline DESC LIMIT $start,$perpage");
//		while ($value = DB::fetch($query)) {
		foreach(C::t('home_poke')->fetch_all_by_uid($space['uid'], $start, $perpage) as $value) {
			$value['uid'] = $value['fromuid'];
			$value['username'] = $value['fromusername'];

			$fuids[$value['uid']] = $value['uid'];
			$list[$value['uid']] = $value;
		}
		if($fuids) {
			require_once libfile('function/friend');
			friend_check($fuids);

			$value = array();
			foreach($fuids as $key => $fuid) {
				$value['isfriend'] = $fuid==$space['uid'] || $_G["home_friend_".$space['uid'].'_'.$fuid] ? 1 : 0;
				$list[$fuid] = array_merge($list[$fuid], $value);
			}

		}
	}
	$multi = multi($count, $perpage, $page, "home.php?mod=spacecp&ac=poke");

}

$actives = array($op=='send'?'send':'poke' =>' class="a"');

include_once template('home/spacecp_poke');

?>

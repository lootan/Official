<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: topicadmin_stickreply.php 30872 2012-06-27 10:11:44Z liulanbo $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

if(!$_G['group']['allowstickreply'] && !$specialperm) {
	showmessage('no_privilege_stickreply');
}

$topiclist = $_GET['topiclist'];
$modpostsnum = count($topiclist);
if(empty($topiclist)) {
	showmessage('admin_stickreply_invalid');
} elseif(!$_G['tid']) {
	showmessage('admin_nopermission', NULL);
}
//$posttable = getposttablebytid($_G['tid']);
$sticktopiclist = $posts = array();
foreach($topiclist as $pid) {
	$post = C::t('forum_post')->fetch('tid:'.$_G['tid'], $pid, false);
	$sticktopiclist[$pid] = $post['position'];
	/* post表里有了position字段，下面的没用了
	//$position = DB::result_first("SELECT position FROM ".DB::table('forum_postposition')." WHERE pid='$pid'");
	$position = C::t('forum_postposition')->fetch_position_by_pid($pid);
	if($position) {
		$sticktopiclist[$pid] = $position;
	} else {
//		$post = DB::fetch_first("SELECT p.tid, p.authorid, p.dateline, p.first, t.special FROM ".DB::table($posttable)." p
//			LEFT JOIN ".DB::table('forum_thread')." t USING(tid) WHERE p.pid='$pid'");
		$post = C::t('forum_post')->fetch('tid:'.$_G['tid'], $pid, false);
		if($post) {
			$threadlist = C::t('forum_thread')->fetch($post['tid']);
			$post['special'] = $threadlist['special'];
			unset($threadlist);
		}
		$posts[]['authorid'] = $post['authorid'];
		//$posttable = getposttablebytid($post['tid']);
		//$curpostnum = DB::result_first("SELECT COUNT(*) FROM ".DB::table($posttable)." WHERE tid='$post[tid]' AND dateline<='$post[dateline]'");
		$curpostnum = C::t('forum_post')->count_by_tid_dateline('tid:'.$post['tid'], $post['tid'], $post['dateline']);
		if(empty($post['first'])) {
			$sticktopiclist[$pid] = $curpostnum;
		}
	}
	 * 
	 */
}

if(!submitcheck('modsubmit')) {

	$stickpid = '';
	foreach($sticktopiclist as $id => $postnum) {
		$stickpid .= '<input type="hidden" name="topiclist[]" value="'.$id.'" />';
	}

	include template('forum/topicadmin_action');

} else {

	if($_GET['stickreply']) {
		foreach($sticktopiclist as $pid => $postnum) {
			//DB::query("REPLACE INTO ".DB::table('forum_poststick')." SET tid='$_G[tid]', pid='$pid', position='$postnum', dateline='$_G[timestamp]'");
			C::t('forum_poststick')->insert(array(
				'tid' => $_G['tid'],
				'pid' => $pid,
				'position' => $postnum,
				'dateline' => $_G['timestamp'],
			), false, true);
		}
	} else {
		foreach($sticktopiclist as $pid => $postnum) {
			//DB::delete('forum_poststick', "tid='$_G[tid]' AND pid='$pid'");
			C::t('forum_poststick')->delete($_G['tid'], $pid);
		}
	}

	//$sticknum = DB::result_first("SELECT COUNT(*) FROM ".DB::table('forum_poststick')." WHERE tid='$_G[tid]'");
	$sticknum = C::t('forum_poststick')->count_by_tid($_G['tid']);
	$stickreply = intval($_GET['stickreply']);

	if($sticknum == 0 || $stickreply == 1) {
//		DB::query("UPDATE ".DB::table('forum_thread')." SET moderated='1', stickreply='$stickreply' WHERE tid='$_G[tid]'");
		C::t('forum_thread')->update($_G['tid'], array('moderated'=>1, 'stickreply'=>$stickreply));
	}

	$modaction = $_GET['stickreply'] ? 'SRE' : 'USR';
	$reason = checkreasonpm();

	$resultarray = array(
	'redirect'	=> "forum.php?mod=viewthread&tid=$_G[tid]&page=$page",
	'reasonpm'	=> ($sendreasonpm ? array('data' => array(array('authorid' => $post['authorid'])), 'var' => 'post', 'notictype' => 'post', 'item' => $_GET['stickreply'] ? 'reason_stickreply': 'reason_stickdeletereply') : array()),
	'reasonvar'	=> array('tid' => $thread['tid'], 'subject' => $thread['subject'], 'modaction' => $modaction, 'reason' => $reason),
	'modlog'	=> $thread
	);

}

?>
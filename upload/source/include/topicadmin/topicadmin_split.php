<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: topicadmin_split.php 30872 2012-06-27 10:11:44Z liulanbo $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

if(!$_G['group']['allowsplitthread']) {
	showmessage('no_privilege_splitthread');
}

//$posttable = getposttablebytid($_G['tid']);
//$posttableid = DB::result_first("SELECT posttableid FROM ".DB::table('forum_thread')." WHERE tid='{$_G['tid']}'");
$thread = C::t('forum_thread')->fetch($_G['tid']);
$posttableid = $thread['posttableid'];
if(!submitcheck('modsubmit')) {

	require_once libfile('function/discuzcode');

	$replies = $thread['replies'];
	if($replies <= 0) {
		showmessage('admin_split_invalid');
	}

	$postlist = array();
	//$query = DB::query("SELECT * FROM ".DB::table($posttable)." WHERE tid='$_G[tid]' ORDER BY dateline");
	foreach(C::t('forum_post')->fetch_all_by_tid('tid:'.$_G['tableid'], $_G['tid'], 'ASC') as $post) {
		$post['message'] = discuzcode($post['message'], $post['smileyoff'], $post['bbcodeoff'], sprintf('%00b', $post['htmlon']), $_G['forum']['allowsmilies'], $_G['forum']['allowbbcode'], $_G['forum']['allowimgcode'], $_G['forum']['allowhtml']);
		$postlist[] = $post;
	}
	include template('forum/topicadmin_action');

} else {

	if(!trim($_GET['subject'])) {
		showmessage('admin_split_subject_invalid');
	} elseif(!($nos = explode(',', $_GET['split']))) {
		showmessage('admin_split_new_invalid');
	}

	sort($nos);
	foreach(C::t('forum_post')->fetch_all_by_tid_position($thread['posttableid'], $_G['tid'], $nos) as $post) {
		$pids[] = $post['pid'];
	}
	/*
	$maxno = $nos[count($nos) - 1];
	$maxno = $maxno > $thread['replies'] + 1 ? $thread['replies'] + 1 : $maxno;
	$maxno = max(1, intval($maxno));
	//$query = DB::query("SELECT pid FROM ".DB::table($posttable)." WHERE tid='$_G[tid]' AND invisible='0' ORDER BY dateline LIMIT $maxno");
	$i = 1;
	$pids = array();
	foreach(C::t('forum_post')->fetch_all_by_tid('tid:'.$_G['tid'], $_G['tid'], false, 'ASC', 0, $maxno, null, 0) as $post) {
		if(in_array($i, $nos)) {
			$pids[] = $post['pid'];
		}
		$i++;
	}
	 */
	if(!($pids = implode(',',$pids))) {
		showmessage('admin_split_new_invalid');
	}

	$modaction = 'SPL';

	$reason = checkreasonpm();

	$subject = dhtmlspecialchars($_GET['subject']);
//	DB::query("INSERT INTO ".DB::table('forum_thread')." (fid, posttableid, subject) VALUES ('$_G[fid]', '$posttableid', '$subject')");
//	$newtid = DB::insert_id();

	$newtid = C::t('forum_thread')->insert(array('fid'=>$_G['fid'], 'posttableid'=>$posttableid, 'subject'=>$subject), true);

	//DB::query("UPDATE ".DB::table($posttable)." SET tid='$newtid' WHERE pid IN ($pids)");
	C::t('forum_post')->update('tid:'.$_G['tid'], explode(',', $pids), array('tid' => $newtid));
	updateattachtid('pid', (array)explode(',', $pids), $_G['tid'], $newtid);

	$splitauthors = array();
	//$query = DB::query("SELECT pid, tid, authorid, subject, dateline FROM ".DB::table($posttable)." WHERE tid='$newtid' AND invisible='0' GROUP BY authorid ORDER BY dateline");
	foreach(C::t('forum_post')->fetch_all_visiblepost_by_tid_groupby_authorid('tid:'.$_G['tid'], $newtid) as $splitauthor) {
		$splitauthor['subject'] = $subject;
		$splitauthors[] = $splitauthor;
	}

	//DB::query("UPDATE ".DB::table($posttable)." SET first='1', subject='$subject' WHERE pid='".$splitauthors[0]['pid']."'", 'UNBUFFERED');
	C::t('forum_post')->update('tid:'.$_G['tid'], $splitauthors[0]['pid'], array('first' => 1, 'subject' => $subject), true);

	//$fpost = DB::fetch_first("SELECT pid, author, authorid, dateline FROM ".DB::table($posttable)." WHERE tid='$_G[tid]' ORDER BY dateline LIMIT 1");
	$query = C::t('forum_post')->fetch_all_by_tid('tid:'.$_G['tid'], $_G['tid'], false, 'ASC', 0, 1);
	foreach($query as $row) {
		$fpost = $row;
	}
//	DB::query("UPDATE ".DB::table('forum_thread')." SET author='".addslashes($fpost['author'])."', authorid='$fpost[authorid]', dateline='$fpost[dateline]', moderated='1' WHERE tid='$_G[tid]'");
	C::t('forum_thread')->update($_G['tid'], array('author'=>$fpost['author'], 'authorid'=>$fpost['authorid'],'dateline'=>$fpost['dateline'], 'moderated'=>1));
	//DB::query("UPDATE ".DB::table($posttable)." SET subject='".addslashes($thread['subject'])."' WHERE pid='$fpost[pid]'");
	C::t('forum_post')->update('tid:'.$_G['post'], $fpost['pid'], array('subject' => $thread['subject']));

	//$fpost = DB::fetch_first("SELECT author, authorid, dateline, rate FROM ".DB::table($posttable)." WHERE tid='$newtid' ORDER BY dateline ASC LIMIT 1");
	$query = C::t('forum_post')->fetch_all_by_tid('tid:'.$_G['tid'], $newtid, false, 'ASC', 0, 1);
	foreach($query as $row) {
		$fpost = $row;
	}
	//����position
	//C::t('forum_post')->increase_position_by_tid($thread['posttableid'], array($_G['tid'], $newtid), count($pids) + $thread['maxposition']);
	$maxposition = 1;
	foreach(C::t('forum_post')->fetch_all_by_tid('tid:'.$_G['tid'], $_G['tid'], false, 'ASC') as $row) {
		if($row['position'] != $maxposition) {
			C::t('forum_post')->update('tid:'.$_G['tid'], $row['pid'], array('position' => $maxposition));
		}
		$maxposition ++;
	}
	C::t('forum_thread')->update($_G['tid'], array('maxposition' => $maxposition));
	$maxposition = 1;
	foreach(C::t('forum_post')->fetch_all_by_tid('tid:'.$_G['tid'], $newtid, false, 'ASC') as $row) {
		if($row['position'] != $maxposition) {
			C::t('forum_post')->update('tid:'.$_G['tid'], $row['pid'], array('position' => $maxposition));
		}
		$maxposition ++;
	}
//	DB::query("UPDATE ".DB::table('forum_thread')." SET author='".addslashes($fpost['author'])."', authorid='$fpost[authorid]', dateline='$fpost[dateline]', rate='".intval(@($fpost['rate'] / abs($fpost['rate'])))."', moderated='1' WHERE tid='$newtid'");
	C::t('forum_thread')->update($newtid, array('author'=>$fpost['author'], 'authorid'=>$fpost['authorid'], 'dateline'=>$fpost['dateline'], 'rate'=>intval(@($fpost['rate'] / abs($fpost['rate']))), 'maxposition' => $maxposition));
	updatethreadcount($_G['tid']);
	updatethreadcount($newtid);
	updateforumcount($_G['fid']);

	$_G['forum']['threadcaches'] && deletethreadcaches($thread['tid']);

	$modpostsnum++;
	$resultarray = array(
	'redirect'	=> "forum.php?mod=forumdisplay&fid=$_G[fid]",
	'reasonpm'	=> ($sendreasonpm ? array('data' => $splitauthors, 'var' => 'thread', 'item' => 'reason_moderate', 'notictype' => 'post') : array()),
	'reasonvar'	=> array('tid' => $thread['tid'], 'subject' => $thread['subject'], 'modaction' => $modaction, 'reason' => $reason),
	'modtids'	=> $thread['tid'].','.$newtid,
	'modlog'	=> array($thread, array('tid' => $newtid, 'subject' => $subject))
	);

}

?>
<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: group_attentiongroup.php 25889 2011-11-24 09:52:20Z monkey $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

$_GET['handlekey'] = 'attentiongroup';
//$usergroups = getuserprofile('groups');
require_once libfile('function/group');
$usergroups = update_usergroups($_G['uid']);
/*$query = DB::query("SELECT f.fid, f.name FROM ".DB::table('forum_groupuser')." g LEFT JOIN ".DB::table('forum_forum')." f ON f.fid=g.fid WHERE uid='$_G[uid]'");
while($row = DB::fetch($query)) {
	$usergroups[] = $row;
}
*/
/*
if(!empty($usergroups)) {
	$usergroups = unserialize($usergroups);
}
*/
$attentiongroup = !empty($_G['member']['attentiongroup']) ? explode(',', $_G['member']['attentiongroup']) : array();
$counttype = count($attentiongroup);
if(submitcheck('attentionsubmit')) {
	if(is_array($_GET['attentiongroupid'])) {
		$_GET['attentiontypeid'] = array_slice($_GET['attentiontypeid'], 0, 5);
		//DB::query("UPDATE ".DB::table('common_member_field_forum')." SET attentiongroup='".implode(',', daddslashes($_GET['attentiongroupid']))."' WHERE uid='$_G[uid]'");
		C::t('common_member_field_forum')->update($_G['uid'], array('attentiongroup' => implode(',', $_GET['attentiongroupid'])));
	} else {
		//DB::query("UPDATE ".DB::table('common_member_field_forum')." SET attentiongroup='' WHERE uid='$_G[uid]'");
		C::t('common_member_field_forum')->update($_G['uid'], array('attentiongroup' => ''));
	}
	showmessage('setup_finished', 'group.php?mod=my&view=groupthread');
}
include template('group/group_attentiongroup');

?>
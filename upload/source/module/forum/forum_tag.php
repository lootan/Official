<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

global $_G;
$op = in_array($_GET['op'], array('search', 'manage', 'set')) ? $_GET['op'] : '';
$taglist = array();
$thread = & $_G['thread'];
//$posttable = $thread['posttable'];

if($op == 'search') {
	//$wheresql = '';
	$searchkey = stripsearchkey($_GET['searchkey']);
	/*
	if($searchkey) {
		$wheresql = " AND tagname LIKE '%$searchkey%'";
	}
	 *
	 */
	//$query = DB::query("SELECT tagname FROM ".DB::table('common_tag')." WHERE status='0' $wheresql LIMIT 50");
	$query = C::t('common_tag')->fetch_all_by_status(0, $searchkey, 50, 0);
	//while($value = DB::fetch($query)) {
	foreach($query as $value) {
		$taglist[] = $value;
	}
	$searchkey = dhtmlspecialchars($searchkey);

} elseif($op == 'manage') {
	if($_G['tid']) {
		$tagarray_all = $array_temp = $threadtag_array = array();
		//$tags = DB::result_first("SELECT tags FROM ".DB::table($posttable)." WHERE tid='$_G[tid]' AND first=1");
		$tags = C::t('forum_post')->fetch_threadpost_by_tid_invisible($_G['tid']);
		$tags = $tags['tags'];
		$tagarray_all = explode("\t", $tags);
		if($tagarray_all) {
			foreach($tagarray_all as $var) {
				if($var) {
					$array_temp = explode(',', $var);
					$threadtag_array[] = $array_temp['1'];
				}
			}
		}
		$tags = implode(',', $threadtag_array);

		$recent_use_tag = array();
		$i = 0;
		//$query = DB::query("SELECT tagid FROM ".DB::table('common_tagitem')." WHERE idtype='tid' ORDER BY itemid DESC LIMIT 10");
		$query = C::t('common_tagitem')->select(0, 0, 'tid', 'itemid', 'DESC', 10);
		//while($result = DB::fetch($query)) {
		foreach($query as $result) {
			if($i > 4) {
				break;
			}
			if($recent_use_tag[$result['tagid']] == '') {
				$i++;
			}
			$recent_use_tag[$result['tagid']] = 1;
		}
		if($recent_use_tag) {
			//$query = DB::query("SELECT * FROM ".DB::table('common_tag')." WHERE tagid IN (".dimplode(array_keys($recent_use_tag)).")");
			$query = C::t('common_tag')->fetch_all(array_keys($recent_use_tag));
			//while($result = DB::fetch($query)) {
			foreach($query as $result) {
				$recent_use_tag[$result[tagid]] = $result['tagname'];
			}
		}
	}
} elseif($op == 'set' && $_GET['formhash'] == FORMHASH && $_G['group']['allowmanagetag']) {
	$class_tag = new tag();
	$tagstr = $class_tag->update_field($_GET['tags'], $_G['tid'], 'tid', $_G['thread']);
	//DB::query("UPDATE ".DB::table($posttable)." SET tags='$tagstr' WHERE tid='$_G[tid]' AND first=1");
	C::t('forum_post')->update_by_tid('tid:'.$_G['tid'], $_G['tid'], array('tags' => $tagstr), false, false, 1);
}

include_once template("forum/tag");
?>
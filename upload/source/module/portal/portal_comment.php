<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: portal_comment.php 33660 2013-07-29 07:51:05Z nemohou $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

$id = empty($_GET['id']) ? 0 : intval($_GET['id']);
$idtype = in_array($_GET['idtype'], array('aid', 'topicid')) ? $_GET['idtype'] : 'aid';
$url = '';
if(empty($id)) {
/*
	showmessage('comment_no_aid_id');
	showmessage('comment_no_topicid_id');
 */
	showmessage('comment_no_'.$idtype.'_id');
}
if($idtype == 'aid') {
	/*$csubject = DB::fetch_first("SELECT a.title, a.allowcomment, ac.commentnum
		FROM ".DB::table('portal_article_title')." a
		LEFT JOIN ".DB::table('portal_article_count')." ac
		ON ac.aid=a.aid
		WHERE a.aid='$id'");*/
	$csubject = C::t('portal_article_title')->fetch($id);
	if($csubject) {
		$csubject = array_merge($csubject, C::t('portal_article_count')->fetch($id));
	}
	$url = fetch_article_url($csubject);
} elseif($idtype == 'topicid') {
	//$csubject = DB::fetch_first("SELECT title, allowcomment, commentnum	FROM ".DB::table('portal_topic')." WHERE topicid='$id'");
	$csubject = C::t('portal_topic')->fetch($id);
	$url = fetch_topic_url($csubject);
}
if(empty($csubject)) {
/*
	showmessage('comment_aid_no_exist');
	showmessage('comment_topicid_no_exist');
 */
	showmessage('comment_'.$idtype.'_no_exist');
} elseif(empty($csubject['allowcomment'])) {
	showmessage($idtype.'_comment_is_forbidden');
}

$perpage = 25;
$page = intval($_GET['page']);
if($page<1) $page = 1;
$start = ($page-1)*$perpage;

//评论
$commentlist = array();
$multi = '';

if($csubject['commentnum']) {
	$pricount = 0;
	//$query = DB::query("SELECT * FROM ".DB::table('portal_comment')." WHERE id='$id' AND idtype='$idtype' ORDER BY dateline DESC LIMIT $start,$perpage");
	$query = C::t('portal_comment')->fetch_all_by_id_idtype($id, $idtype, 'dateline', 'DESC', $start, $perpage);
	//while ($value = DB::fetch($query)) {
	foreach($query as $value) {
		if($value['status'] == 0 || $value['uid'] == $_G['uid'] || $_G['adminid'] == 1) {
			$commentlist[] = $value;
		} else {
			$pricount ++;
		}
	}
}

$multi = multi($csubject['commentnum'], $perpage, $page, "portal.php?mod=comment&id=$id&idtype=$idtype");
//验证码开关
list($seccodecheck, $secqaacheck) = seccheck('publish');
include_once template("diy:portal/comment");

?>
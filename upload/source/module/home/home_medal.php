<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: home_medal.php 28887 2012-03-16 10:17:38Z monkey $
 */

if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

loadcache('medals');

if(!$_G['uid'] && $_GET['action']) {
	showmessage('not_loggedin', NULL, array(), array('login' => 1));
}

$_G['mnid'] = 'mn_common';
$medallist = $medallogs = array();
$tpp = 10;
$page = max(1, intval($_GET['page']));
$start_limit = ($page - 1) * $tpp;

if(empty($_GET['action'])) {
	include libfile('function/forum');
	$medalcredits = array();
//	$query = DB::query("SELECT * FROM ".DB::table('forum_medal')." WHERE available='1' ORDER BY displayorder LIMIT 0,100");
//	while($medal = DB::fetch($query)) {
	foreach(C::t('forum_medal')->fetch_all_data(1) as $medal) {
		$medal['permission'] = medalformulaperm(serialize(array('medal' => dunserialize($medal['permission']))), 1);
		if($medal['price']) {
			$medal['credit'] = $medal['credit'] ? $medal['credit'] : $_G['setting']['creditstransextra'][3];
			$medalcredits[$medal['credit']] = $medal['credit'];
		}
		$medallist[$medal['medalid']] = $medal;
	}

	$memberfieldforum = C::t('common_member_field_forum')->fetch($_G['uid']);
	$membermedal = $memberfieldforum['medals'] ? explode("\t", $memberfieldforum['medals']) : array();
	$membermedal = array_map('intval', $membermedal);

	$lastmedals = $uids = array();
//	$query = DB::query("SELECT m.uid, m.username, ml.medalid, ml.dateline FROM ".DB::table('forum_medallog')." ml
//		USE INDEX(dateline) LEFT JOIN ".DB::table('common_member')." m USING(uid)
//		WHERE ml.type<'2' ORDER BY ml.dateline DESC LIMIT 10");
//	while($lastmedal = DB::fetch($query)) {
	foreach(C::t('forum_medallog')->fetch_all_lastmedal(10) as $id => $lastmedal) {
		$lastmedal['dateline'] = dgmdate($lastmedal['dateline'], 'u');
		$lastmedals[$id] = $lastmedal;
		$uids[] = $lastmedal['uid'];
	}
	$lastmedalusers = C::t('common_member')->fetch_all($uids);
	/*$mymedals = array();
	$query = DB::query("SELECT medalid FROM ".DB::table('common_member_medal')." WHERE uid='$_G[uid]'");
	while($membermedal = DB::fetch($query)) {
		$mymedals[] = $membermedal['medalid'];
	}*/
	$mymedals = C::t('common_member_medal')->fetch_all_by_uid($_G['uid']);

} elseif($_GET['action'] == 'confirm') {

	include libfile('function/forum');
//	$medal = DB::fetch_first("SELECT * FROM ".DB::table('forum_medal')." WHERE medalid='$_GET[medalid]' AND available='1'");
	$medal = C::t('forum_medal')->fetch($_GET['medalid']);
	$medal['permission'] = medalformulaperm(serialize(array('medal' => dunserialize($medal['permission']))), 1);
	if($medal['price']) {
		$medal['credit'] = $medal['credit'] ? $medal['credit'] : $_G['setting']['creditstransextra'][3];
		$medalcredits[$medal['credit']] = $medal['credit'];
	}
	include template('home/space_medal_float');

} elseif($_GET['action'] == 'apply' && submitcheck('medalsubmit')) {

	$medalid = intval($_GET['medalid']);
	$_G['forum_formulamessage'] = $_G['forum_usermsg'] = $medalnew = '';
//	$medal = DB::fetch_first("SELECT * FROM ".DB::table('forum_medal')." WHERE medalid='$medalid'");
	$medal = C::t('forum_medal')->fetch($medalid);
	if(!$medal['type']) {
		showmessage('medal_apply_invalid');
	}

	//if(DB::result_first("SELECT COUNT(*) FROM ".DB::table('common_member_medal')." WHERE uid='$_G[uid]' AND medalid='$medalid'")) {
	if(C::t('common_member_medal')->count_by_uid_medalid($_G['uid'], $medalid)) {
		showmessage('medal_apply_existence', 'home.php?mod=medal');
	}

	$applysucceed = FALSE;
	$medalpermission = $medal['permission'] ? dunserialize($medal['permission']) : '';
	if($medalpermission[0] || $medalpermission['usergroupallow']) {
		include libfile('function/forum');
		medalformulaperm(serialize(array('medal' => $medalpermission)), 1);

		if($_G['forum_formulamessage']) {
			showmessage('medal_permforum_nopermission', 'home.php?mod=medal', array('formulamessage' => $_G['forum_formulamessage'], 'usermsg' => $_G['forum_usermsg']));
		} else {
			$applysucceed = TRUE;
		}
	} else {
		$applysucceed = TRUE;
	}

	if($applysucceed) {
		$expiration = empty($medal['expiration'])? 0 : TIMESTAMP + $medal['expiration'] * 86400;
		if($medal['type'] == 1) {
			if($medal['price']) {
				$medal['credit'] = $medal['credit'] ? $medal['credit'] : $_G['setting']['creditstransextra'][3];
				if($medal['price'] > getuserprofile('extcredits'.$medal['credit'])) {
					showmessage('medal_not_get_credit', '', array('credit' => $_G['setting']['extcredits'][$medal[credit]][title]));
				}
				updatemembercount($_G['uid'], array($medal['credit'] => -$medal['price']), true, 'BME', $medal['medalid']);
			}

			//$usermedal = DB::fetch_first("SELECT medals FROM ".DB::table('common_member_field_forum')." WHERE uid='$_G[uid]'");
			$memberfieldforum = C::t('common_member_field_forum')->fetch($_G['uid']);
			$usermedal = $memberfieldforum;
			unset($memberfieldforum);
			$medal['medalid'] = $medal['medalid'].(empty($expiration) ? '' : '|'.$expiration);
			$medalnew = $usermedal['medals'] ? $usermedal['medals']."\t".$medal['medalid'] : $medal['medalid'];
			//DB::query("UPDATE ".DB::table('common_member_field_forum')." SET medals='$medalnew' WHERE uid='$_G[uid]'");
			C::t('common_member_field_forum')->update($_G['uid'], array('medals' => $medalnew));
			//DB::insert('common_member_medal', array('uid' => $_G['uid'], 'medalid' => $medal['medalid']), 0, 1);
			C::t('common_member_medal')->insert(array('uid' => $_G['uid'], 'medalid' => $medal['medalid']), 0, 1);
			$medalmessage = 'medal_get_succeed';
			/*showmessage('medal_get_succeed');*/
		} else {
			if(C::t('forum_medallog')->count_by_verify_medalid($_G['uid'], $medal['medalid'])) {
				showmessage('medal_apply_existence', 'home.php?mod=medal');
			}
			$medalmessage = 'medal_apply_succeed';
			/*showmessage('medal_apply_succeed');*/
			manage_addnotify('verifymedal');
		}

//		DB::query("INSERT INTO ".DB::table('forum_medallog')." (uid, medalid, type, dateline, expiration, status) VALUES ('$_G[uid]', '$medalid', '$medal[type]', '$_G[timestamp]', '$expiration', '".($expiration ? 1 : 0)."')");
		C::t('forum_medallog')->insert(array(
		    'uid' => $_G['uid'],
		    'medalid' => $medalid,
		    'type' => $medal['type'],
		    'dateline' => TIMESTAMP,
		    'expiration' => $expiration,
		    'status' => ($expiration ? 1 : 0),
		));
		showmessage($medalmessage, 'home.php?mod=medal', array('medalname' => $medal['name']));
	}

} elseif($_GET['action'] == 'log') {

	include libfile('function/forum');
//	$query = DB::query("SELECT * FROM ".DB::table('forum_medal')." WHERE available='1' ORDER BY displayorder LIMIT 0,100");
//	while($medal = DB::fetch($query)) {
	foreach(C::t('forum_medal')->fetch_all_data(1) as $medal) {
		$medallist[$medal['medalid']] = $medal;
	}

	//$medaldata = DB::result_first("SELECT medals FROM ".DB::tale('common_member_field_forum')." WHERE uid='$_G[uid]'");
	$memberfieldforum = C::t('common_member_field_forum')->fetch($_G['uid']);
	$membermedal = $memberfieldforum['medals'] ? explode("\t", $memberfieldforum['medals']) : array();
	foreach($membermedal as $k => $medal) {
		if(!in_array($medal, array_keys($medallist))) {
			unset($membermedal[$k]);
		}
	}
	$medalcount = count($membermedal);

	if(!empty($membermedal)) {
		$mymedal = array();
		foreach($membermedal as $medalid) {
			if($medalpos = strpos($medalid, '|')) {
				$medalid = substr($medalid, 0, $medalpos);
			}
			$mymedal['name'] = $_G['cache']['medals'][$medalid]['name'];
			$mymedal['image'] = $medallist[$medalid]['image'];
			$mymedals[] = $mymedal;
		}
	}

//	$medallognum = DB::result_first("SELECT COUNT(*) FROM ".DB::table('forum_medallog')." WHERE uid='$_G[uid]' AND type<'2'");
	$medallognum = C::t('forum_medallog')->count_by_uid($_G['uid']);
	$multipage = multi($medallognum, $tpp, $page, "home.php?mod=medal&action=log");

//	$query = DB::query("SELECT me.*, m.image FROM ".DB::table('forum_medallog')." me
//			LEFT JOIN ".DB::table('forum_medal')." m USING (medalid)
//			WHERE me.uid='$_G[uid]' ORDER BY me.dateline DESC LIMIT $start_limit,$tpp");
//	while($medallog = DB::fetch($query)) {
	foreach(C::t('forum_medallog')->fetch_all_by_uid($_G['uid'], $start_limit, $tpp) as $medallog) {
		$medallog['name'] = $_G['cache']['medals'][$medallog['medalid']]['name'];
		$medallog['dateline'] = dgmdate($medallog['dateline']);
		$medallog['expiration'] = !empty($medallog['expiration']) ? dgmdate($medallog['expiration']) : '';
		$medallogs[] = $medallog;
	}

} else {
	showmessage('undefined_action');
}

$navtitle = lang('core', 'title_medals_list');

include template('home/space_medal');

?>
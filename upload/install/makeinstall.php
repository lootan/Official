<?php

/**
 *      [Discuz!] (C)2001-2099 Comsenz Inc.
 *      This is NOT a freeware, use is subject to license terms
 *
 *      $Id: admin.php 6623 2010-03-24 08:55:05Z cnteacher $
 */

include_once('../source/class/class_core.php');
include_once('../source/function/function_core.php');

$cachelist = array();
$discuz = C::app();

$discuz->cachelist = $cachelist;
$discuz->init_cron = false;
$discuz->init_setting = false;
$discuz->init_user = false;
$discuz->init_session = false;
$discuz->init_misc = false;

$discuz->init();

$version = str_replace(array('.', ' '), '', $version);
$file = './data/install_new.sql';
$datafile = './data/install_data_new.sql';

$db = & DB::object();
$tablepre = $db->tablepre;

$db->query('SET SQL_QUOTE_SHOW_CREATE=0', 'SILENT');

$time = gmdate("$dateformat $timeformat", $timestamp + $timeoffset * 3600);
$tables = arraykeys2(fetchtablelist($tablepre), 'Name');

$table_dumpdata = array(
	'common_setting',
	'forum_forum',
	'forum_forumfield',
	'forum_bbcode',
	'common_cron',
	'forum_faq',
	'common_friendlink',
	'forum_imagetype',
	'forum_medal',
	'common_nav',
	'forum_onlinelist',
	'common_smiley',
	'common_style',
	'common_stylevar',
	'common_template',
	//'forum_typeoption',
	'common_usergroup',
	'common_usergroup_field',
	'common_admingroup',
	'common_admincp_group',
	'common_admincp_perm',
	'common_task',
	'forum_grouplevel',
	'common_credit_rule',
	'home_click',
	'common_member_profile_setting',
	'common_block_style',
	'pre_portal_category',
);
foreach ($table_dumpdata as $key => $value) {
	$table_dumpdata[$key] = $tablepre.$value;
}

$sqlcompat == 'MYSQL40';
if($db->version() > '4.1') {
	$db->query("SET SQL_MODE='MYSQL40'");
}

$now = gmdate('Y-m-d H:i:s', time() + 3600 * 8);
$sqldump = <<<EOT
--
-- DiscuzX INSTALL MAKE SQL DUMP V1.0
-- DO NOT modify this file
--
-- Create: $now
--

EOT;
echo '正在生成文件 <a href="'.$file.'">'.$file.'</a> 请稍后<hr>';
foreach ($tables as $id => $table) {
	echo ($id + 1). ": $table <br>\n";
	$sqldump .= sqldumptable($table);
}
file_put_contents($file, $sqldump);
echo '</textarea><hr> 生成完毕!';

echo '<hr>导出数据 请稍后<hr>';
$data = ''; $serialize = '';
foreach ($table_dumpdata as $id => $table) {
	echo ($id + 1). ": $table <br>\n";
	$data .= tabledata($table)."\n\n";
}
file_put_contents($datafile, $data);
echo '</textarea><hr> 生成完毕!';

function arraykeys2($array, $key2) {
	$return = array();
	foreach($array as $val) {
		$return[] = $val[$key2];
	}
	return $return;
}

function fetchtablelist($tablepre = '') {
	global $db;
	$arr = explode('.', $tablepre);
	$dbname = $arr[1] ? $arr[0] : '';
	$sqladd = $dbname ? " FROM $dbname LIKE '$arr[1]%'" : "LIKE '$tablepre%'";
	!$tablepre && $tablepre = '*';
	$tables = $table = array();
	$query = $db->query("SHOW TABLE STATUS $sqladd");
	while($table = $db->fetch_array($query)) {
		if(!strstr($table['Name'], $tablepre.'ucenter_')) {
			$table['Name'] = ($dbname ? "$dbname." : '').$table['Name'];
			$tables[] = $table;
		}
	}
	return $tables;
}


function syntablestruct($sql, $version, $dbcharset) {

	//note 是否为建表语句
	if(strpos(trim(substr($sql, 0, 18)), 'CREATE TABLE') === FALSE) {
		return $sql;
	}

	//note 自动判断当前建表语句的版本
	$sqlversion = strpos($sql, 'ENGINE=') === FALSE ? FALSE : TRUE;

	//note 如果都为同一版本，则不做处理
	if($sqlversion === $version) {

		//note 如果为高版本，并且设置了转换的字符集，则进行替换。
		return $sqlversion && $dbcharset ? preg_replace(array('/ character set \w+/i', '/ collate \w+/i', "/DEFAULT CHARSET=\w+/is"), array('', '', "DEFAULT CHARSET=$dbcharset"), $sql) : $sql;
	}

	//note 如果低转高
	if($version) {
		return preg_replace(array('/TYPE=HEAP/i', '/TYPE=(\w+)/is'), array("ENGINE=MEMORY DEFAULT CHARSET=$dbcharset", "ENGINE=\\1 DEFAULT CHARSET=$dbcharset"), $sql);

		//note 如果高转低
	} else {
		return preg_replace(array('/character set \w+/i', '/collate \w+/i', '/ENGINE=MEMORY/i', '/\s*DEFAULT CHARSET=\w+/is', '/\s*COLLATE=\w+/is', '/ENGINE=(\w+)(.*)/is'), array('', '', 'ENGINE=HEAP', '', '', 'TYPE=\\1\\2'), $sql);
	}
}
function sqldumptable($table, $startfrom = 0, $currsize = 0) {
	global $db;
	$sqlcompat = 'MYSQL40';
	$sqlcharset = '';

	$offset = 300;
	$tabledump = '';

	$createtable = $db->query("SHOW CREATE TABLE $table", 'SILENT');

	if(!$db->error()) {
		$tabledump = "DROP TABLE IF EXISTS $table;\n";
	} else {
		return '';
	}

	$create = $db->fetch_row($createtable);

	//debug 判断是否在不同数据库
	if(strpos($table, '.') !== FALSE) {
		$tablename = substr($table, strpos($table, '.') + 1);
		$create[1] = str_replace("CREATE TABLE $tablename", 'CREATE TABLE '.$table, $create[1]);
	}

	$data = explode("\n", $create[1]);
	foreach ($data as $key => $line) {
		$line  = rtrim($line);
		$tail = substr($line, -1) == ',' || substr($line, -1) == ':' ? substr($line, -1) : '';
		if($pos = strpos($line, 'COMMENT \'')) {
			$line = substr($line, 0, $pos - 1).$tail;
		}
		$data[$key] = rtrim($line);
	}

	$create[1] = implode("\n", $data);

	$tabledump .= $create[1];

	if($sqlcompat == 'MYSQL41' && $db->version() < '4.1') {
		$tabledump = preg_replace("/TYPE\=(.+)/", "ENGINE=\\1 DEFAULT CHARSET=".$dumpcharset, $tabledump);
	}
	if($db->version() > '4.1' && $sqlcharset) {
		$tabledump = preg_replace("/(DEFAULT)*\s*CHARSET=.+/", "DEFAULT CHARSET=".$sqlcharset, $tabledump);
	}

	$tablestatus = $db->fetch_first("SHOW TABLE STATUS LIKE '$table'");

	if($sqlcompat == 'MYSQL40' && $tablestatus['Engine'] == 'MEMORY') {
		$tabledump = str_replace('TYPE=MEMORY', 'TYPE=HEAP', $tabledump);
	}

	$tabledump = preg_replace('/ AUTO_INCREMENT=\w+/i', '', $tabledump);
	$tabledump .= ";\n\n";
	return $tabledump;
}

function tabledata($table, $startfrom = 0, $currsize = 0) {
	global $db;
	$sqlcompat = 'MYSQL40';
	$usehex = FALSE;
	$sqlcharset = '';

	$offset = 300;
	$tabledump = '';
	$tablefields = array();

	$query = $db->query("SHOW FULL COLUMNS FROM $table", 'SILENT');
	if($query) {
		while($fieldrow = $db->fetch_array($query)) {
			$tablefields[] = $fieldrow;
		}
	} else {
		return ;
	}

	if(strstr($table, 'common_setting')) {
		$selectsql = "SELECT * FROM $table order by `skey` LIMIT 9999 ";
	} else {
		$selectsql = "SELECT * FROM $table LIMIT 9999";
	}
	echo $selectsql.'<br>';
	if($rows = $db->query($selectsql)) {
		$numfields = $db->num_fields($rows);
		$numrows = $db->num_rows($rows);
		while($row = $db->fetch_row($rows)) {
			$comma = $t = '';
			for($i = 0; $i < $numfields; $i++) {
				$t .= $comma.('`'.$tablefields[$i]['Field'].'`=\''.$db->escape_string($row[$i]).'\'');
				$comma = ',';
			}
			$tabledump .= "INSERT INTO $table SET $t;\n";
		}
	}

	$tabledump .= "\n";

	return $tabledump;
}
?>
